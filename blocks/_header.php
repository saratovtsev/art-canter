<div id="header_up" class="header__navpanel">
    <div id="header_up_main_menu" class="header__menu">
      <button type="button" class="primary-menu-toggle _primary-toggle">
        <span class="primary-menu-toggle__single"></span>
        <span class="primary-menu-toggle__single"></span>
        <span class="primary-menu-toggle__single"></span>
      </button>
      <!-- /{SITE}{MENU} -->
        <ul class="primary-menu">
          <li>
            <a href="/about/" isset="true">О компании</a>
            <ul class="sub-menu">
              <li>
                <a href="/about/37/" isset="true">Жюри</a>
              </li>
              <li>
                <a href="/about/reference/" isset="true">Отзывы</a>
              </li>
              <li>
                <a href="/about/part/" isset="true">Партнеры</a>
              </li>
              <li>
                <a href="/about/articles/" isset="true">Наши Проекты в СМИ</a>
              </li>
              <li>
                <a href="/about/reklamodatel/" isset="true">Рекламодателям</a>
              </li>
              <li>
                <a href="/about/our_people/" isset="true">Наши сотрудники</a>
              </li>
              <li>
                <a href="/about/51/" isset="true">Дипломы и награды</a>
              </li>
              <li>
                <a href="/about/charity/" isset="true">Благотворительность</a>
              </li>
              <li>
                <a href="/about/muzklondike/" isset="true">Музыкальный Клондайк</a>
              </li>
              <li>
                <a href="/about/pomoshchi-po-saity/" isset="true">Помощь по сайту</a>
              </li>
              <li>
                <a href="/about/privacy-policy/" isset="true">Политика конфиденциальности</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/faq/" isset="true">Вопросы Ответы</a>
            <ul class="sub-menu">
              <li>
                <a href="/faq/useful/" isset="true">Полезная Информация</a>
              </li>
              <li>
                <a href="/faq/modul_artcenter/" isset="true">Модуль</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/40/" isset="true">Услуги</a>
            <ul class="sub-menu">
              <li>
                <a href="/40/41/" isset="true">Визы</a>
              </li>
              <li>
                <a href="/40/partners/" isset="true">Партнерам</a>
              </li>
              <li>
                <a href="/40/45/" isset="true">Страхование</a>
              </li>
              <li>
                <a href="/40/43/" isset="true">Консультация</a>
              </li>
              <li>
                <a href="/40/49/" isset="true">Бронирование Отелей</a>
              </li>
              <li>
                <a href="/40/44/" isset="true">Доставка Документов</a>
              </li>
              <li>
                <a href="/40/zhyuri/" isset="true">Жюри</a>
              </li>
              <li>
                <a href="/40/hotels/" isset="true">Отели в Вашем городе</a>
              </li>
            </ul>
          </li>
          <li>
            <a target="_blank" href="#" isset="true">Наше СМИ</a>
          </li>
          <li>
            <a href="/contacts/" isset="true">Контакты</a>
          </li>
        </ul>
      <!-- {SITE}{MENU}/ -->
    </div>
    <a id="ac_eng" href="/eng/" isset="true" class="language">
      <img alt="" src="/d/i/en.png" class="language__img">
    </a>
    <a id="header_up_for_partners" href="/40/partners/" style="margin-left: 10px" isset="true" class="header__cpa">Партнёрам</a>
    <div id="header_up_login_fav" class="header__login">
        <div class="header__mobilebtn">
          <button class="header-sprite header-sprite__find" onclick="scrollToFormAndShow();"></button><button class="header-sprite header-sprite__mail"></button><button class="header-sprite header-sprite__tel"></button>
        </div>
        <!-- /{SITE}{LOGIN_LOGOUT} -->
        <a href="#" id="header_up_login_fav_link_login" class="header__signup display_block linkplace" onclick="start_login();" isset="true">Войти / Зарегистрироваться</a>
        <a href="#" id="header_up_login_fav_link_exit" class="display_none linkplace" onclick="start_logout();" isset="true"><img alt="" src="/d/i/exit_head.png"></a>
        <a href="/profile/" class="display_none linkplace" id="header_up_login_fav_link_profile" isset="true"></a>
        <!-- {SITE}{LOGIN_LOGOUT} -->
        <a href="/favorites/" class="display_block linkplace favorite" id="header_up_login_fav_link_fav" isset="true">Избранное</a>
    </div>

</div>

<div class="clear"></div>

<div id="logo" class="logo">
    <a href="/" isset="true" class="logo__link">
        <img alt="" src="/d/i/logo-gb.gif" class="logo__img">
        <div class="">
          <span class="logo__title">АРТ-ЦЕНТР</span>
          <span class="logo__description">Все фестивали на одном портале</span>
        </div>
    </a>
</div>
<div id="header_right" class="header__right">
  <div id="header_right_contacts" class="header__contact">
      <span class="header__phonenumber">+7 (926) 777-32-47  &nbsp;&nbsp;&nbsp;&nbsp; +7 (495) 672-18-62 </span>
      <a onclick="start_mail();" style="cursor: pointer;" class="header__link">Написать нам</a>
  </div>

    <div id="main_menu" class="menu-fest header__menu-fest">
        <a href="/events/festivals/" class="menu-fest__item menu-fest__item_fest" isset="true">Фестивали</a>
        <a href="/events/masterklassy/" class="menu-fest__item menu-fest__item_mklass" isset="true">Мастер-классы</a>
        <a href="/events/konkyrsi/" class="menu-fest__item menu-fest__item_konk" isset="true">Конкурсы</a>
        <a href="/events/detskie-lagerya/" class="menu-fest__item menu-fest__item_kan" isset="true">Детские лагеря</a>
        <a href="/events/popular/" class="menu-fest__item menu-fest__item _super" isset="true">Популярные</a>
        <a href="/events/closing/" class="menu-fest__item menu-fest__item _super" isset="true">Горящие</a>
        <a href="#" id="countries_slider_link" class="menu-fest__item menu-fest__item_world _super" isset="true">По странам</a>
    </div>
</div>
