<?php
  $zhanr_descr_arr = array(
    'Для участия приглашаются хоры всех категорий и вокальные ансамбли. В рамках данного жанра проводятся международные и всероссийские хоровые фестивали и конкурсы, конкурсы вокальных ансамблей, конкурсы хормейстеров и конкурсы для дирижеров хоров.',
    'Для участия приглашаются вокалисты и вокальные ансамбли эстрадного, академического и народного вокала. В рамках данной категории проводятся конкурсы эстрадного и академического вокала, конкурсы вокально-инструментальных ансамблей, конкурсы эстрадной песни, конкурсы народного пения и другие.',
    'Для участия приглашаются танцоры и хореографические коллективы различных направлений хореографии. В данной категории проводятся хореографические фестивали и конкурсы, конкурсы балета, конкурсы современной и народной хореографии, мастер-классы для танцоров и хореографов, хореографические лагеря и семинары для педагогов.',
    'Для участия приглашаются отдельные исполнители, ансамбли и оркестры. В рамках данной категории проводятся конкурсы для юных музыкантов, фестивали и конкурсы для духовых, симфонических и оркестров народных инструментов, конкурсы для профессиональных исполнителей и любителей.',
    'Для участия приглашаются солисты, ансамбли и коллективы, выступающие в народном, фольклорном и этно направлении, включая стилизации и обрядовые постановки. В рамках данной категории проводятся фестивали и конкурсы народного вокала и фестивали народной хореографии, конкурсы театральных и обрядовых постановок, фестивали декоративно-прикладного искусства, конкурсы народного костюма.',
    'Для участия приглашаются солисты и коллективы всех направлений творчества. В рамках данной категории проводятся хореографические и вокальные фестивали, фестивали для театров, конкурс театрального творчества, конкурс театров мод, конкурс чтецов и исполнителей авторской песни, конкурсы художников и дизайнеров, конкурсы красоты.',
  )
 ?>

<form action="/search/" method="post" id="form_search_mini_id">
  <input type="hidden" name="text" id="form_search_field_name" value="">
  <div id="form_search_mini" class="form_search_mini">
    <div id="show_hide" class="show_hide_nocollapse" onclick="show_hide_form_search_mini()"></div>
    <div class="form_search_mini_title">
        Подобрать фестиваль
    </div>

    <div class="form_search_mini_calendar">
      с <input id="date1_input" type="text" name="date1" size="12" value="<?php echo date('Y.m.d'); ?>" />
        <input id="date1_mini_hover" type="text" name="date1_mini_hover" size="12" value="<?php echo date('Y.m.d'); ?>" />
      по <input id="date2_input" type="text" name="date2" size="12"  value="<?php echo date('Y.m.d');?>" />
         <input id="date2_mini_hover" type="text" name="date2_mini_hover" size="12"  value="<?php echo  date('Y.m.d');?>" />

      <input type="hidden" id="min_date_real" value="02.01.2017">
      <input type="hidden" id="max_date_real" value="02.01.2019">
    </div>

    <div style="clear: both"></div>

    <div class="form_search_mini_country_city">
      <div class="form_search_mini_country">
        <div class="form_search_mini_country_title">Страна</div>
          <div class="form_search_mini_country_content">
            <select id="select-country" class="demo-default" placeholder="Начните вводить текст.." name="country[]">
              <?php for ($i=0; $i < 46; $i++) {
                  echo "<option value=\"".$i."\">Страна-".$i."</option>";
                } ?>
            </select>
            </div>
            <div class="form_search_mini_country_quick">
                <input type="hidden" name="quick_place" id="quick_place" value="<?php echo (isset($_POST['quick_place'])&&intval($_POST['quick_place'])>-2)?$_POST['quick_place']:'undefined'; ?>">
                <a onclick="quickPlace(-7, '_mini')">Россия</a>
                <a onclick="quickPlace(-2, '_mini')">Европа</a>
                <a onclick="quickPlace(-3, '_mini')">Азия</a>
                <a onclick="quickPlace(-4, '_mini')">Америка</a>
            </div>
        </div>
        <br>
        <div class="form_search_mini_city">
          <div class="form_search_mini_city_title">Город</div>
          <div class="form_search_mini_city_content">
            <select id="select-city" class="demo-default" placeholder="Начните вводить текст.." name="town[]">
              <?php
                echo '<option value selected="selected"></option>';

                echo '<option value="1">Москва</option>';
                echo '<option value="17">Санкт-Петербург</option>';

                for ($i=2; $i < 47; $i++) {
                    if ($i !== 17) {
                    echo "<option value=\"".$i."\">Город-".$i."</option>";
                  }
                }
              ?>
            </select>
          </div>
        </div>

    </div>

    <div class="form_search_mini_zaoch">
      <input type="checkbox" id="chbox_zaoch"
        name="zaoch" class="search_zaoch_check"
        <?php if(isset($_POST['zaoch'])) echo 'checked' ?> />
      <label for="chbox_zaoch"><span></span>Заочный</label>
    </div>

    <div class="form_search_mini_zhanr">
      <div class="form_search_mini_zhanr_title">Жанр мероприятия</div>
        <div class="form_search_mini_content_checkbox_block">
            <input type="checkbox" id="chbox_1_mini" name="cat[]" class="search_mini_category_check" value="1" <?php if(isset($_POST['cat']) && in_array(1, $_POST['cat'])) echo 'checked' ?>>
            <label for="chbox_1_mini">
              <span></span>
              <a data-title='<?php echo $zhanr_descr_arr[0]; ?>' data-location="bottom">Хоровые</a>
            </label>
            <input type="checkbox" id="chbox_2_mini" name="cat[]" class="search_mini_category_check" value="2" <?php if(isset($_POST['cat']) && in_array(2, $_POST['cat'])) echo 'checked' ?>>
            <label for="chbox_2_mini">
              <span></span>
              <a data-title='<?php echo $zhanr_descr_arr[1]; ?>' data-location="bottom">Вокальные</a>
            </label>
            <input type="checkbox" id="chbox_3_mini" name="cat[]" class="search_mini_category_check" value="3" <?php if(isset($_POST['cat']) && in_array(3, $_POST['cat'])) echo 'checked' ?>>
            <label for="chbox_3_mini">
              <span></span>
              <a data-title='<?php echo $zhanr_descr_arr[2]; ?>' data-location="bottom">Хореографические</a>
            </label>
            <input type="checkbox" id="chbox_4_mini" name="cat[]" class="search_mini_category_check" value="4" <?php if(isset($_POST['cat']) && in_array(4, $_POST['cat'])) echo 'checked' ?>><label for="chbox_4_mini">
              <span></span>
              <a data-title='<?php echo $zhanr_descr_arr[3]; ?>' data-location="bottom">Инструментальные</a>
            </label>
            <input type="checkbox" id="chbox_5_mini" name="cat[]" class="search_mini_category_check" value="5" <?php if(isset($_POST['cat']) && in_array(5, $_POST['cat'])) echo 'checked' ?>>
            <label for="chbox_5_mini">
              <span></span>
              <a data-title='<?php echo $zhanr_descr_arr[4]; ?>' data-location="bottom">Фольклорные</a>
            </label>
            <input type="checkbox" id="chbox_6_mini" name="cat[]" class="search_mini_category_check" value="6" <?php if(isset($_POST['cat']) && in_array(6, $_POST['cat'])) echo 'checked' ?>>
            <label for="chbox_6_mini">
              <span></span><a data-title='<?php echo $zhanr_descr_arr[5]; ?>' data-location="bottom">Другие</a>
            </label>
        </div>
    </div>
    <div class="form_search_mini_price">
        <div class="form_search_mini_price_title">Стоимость мероприятия</div>

        <input type="hidden" id="min_price_real" value="1000">
        <input type="hidden" id="max_price_real" value="50000">

        <input id="minCost" type="text" name="price1" value="1000" size="5" style="float: left;"/>
        <input id="maxCost" type="text" name="price2" value="50000" size="5" style="float: right;"/>

        <div id="slider_price_mini"></div>
    </div>
    <div class="form_search_mini_age">
        <div class="form_search_mini_age_title">Возраст участников</div>

        <input id="minAge" type="text" name="age1" value="<?php echo ((isset($_POST['age1']))?$_POST['age1']:'0'); ?>" size="3" style="float: left;" />
        <input id="maxAge" type="text" name="age2" value="<?php echo ((isset($_POST['age2']))?$_POST['age2']:'100'); ?>" size="3" style="float: right;" />


        <div id="slider_age_mini"></div>
    </div>

    <a href="#" id="mini_form_search_submit" onclick="search_submit_mini(); yaCounter17972227.reachGoal('search'); return false;"><div>Подобрать&nbsp;мероприятие</div></a>

</div>
<div style="clear: both"></div>
<div class="attention">
    <p>Обратите внимание</p>
    <a target="_blank" href="/events/closing" style="position: relative; top: 20px; background: #fff; border: 1px solid #2D910E; border-radius: 2px; width: 200px; margin: 10px auto 0; padding: 10px 15px; text-align: center; cursor: pointer; color: #000; text-decoration: none">Заканчивается прием заявок</a>
</div>

<div style="clear: both"></div>


<!--{ADS}-->



</form>
