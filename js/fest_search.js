var text;
var country;
var quick_place;
var town=[];
var date1;
var date2;
var min_date_real;
var max_date_real;
var cat;
var age1;
var age2;
var price1;
var price2;

var zaoch;

var view = 'list';

var curs_eur;
var curs_usd;


var order_by_date;
var filter_show;        // all, go, close
var count_all = -1;
var count_showed;
var array_events=[];
var the_end;


var slider_price;
var slider_age;



$(document).ready(function(){

    Date.prototype.format = function (mask, utc) {
        return dateFormat2(this, mask, utc);
    };

    min_date_real = $("#min_date_real").val();
    var min_date_real_date = new Date(min_date_real.substr(3,2)+'/'+min_date_real.substr(0,2)+'/'+min_date_real.substr(6,4));
    var dayOfMonth = min_date_real_date.getDate();
    min_date_real_date.setDate(dayOfMonth + 1);
    min_date_real_date2 = min_date_real_date.format("dd.mm.yyyy")


    max_date_real = $("#max_date_real").val();
    var max_date_real_date = new Date(max_date_real.substr(3,2)+'/'+max_date_real.substr(0,2)+'/'+max_date_real.substr(6,4));
    var dayOfMonth = max_date_real_date.getDate();
    max_date_real_date.setDate(dayOfMonth - 1);
    max_date_real_date2 = max_date_real_date.format("dd.mm.yyyy")

    now_date = new Date();
    now_date = now_date.format("dd.mm.yyyy")

    min_price_real = $("#min_price_real").val()
    max_price_real = $("#max_price_real").val()
    
    curs_eur = $("#curs_eur").val();
    curs_usd = $("#curs_usd").val();

    if($("#is_search_results").length>0){
        
        $('.list_map_switch').click(function(){
            switcherListMap();
        });
        
        setTimeout("$('.kalendae .k-calendar').css({'margin':'3px 15px 0 15px'})", 3000);
        
        
        $('#content').css('min-height', '1100px');
        
        // убрать большую форму
        $('#form_search_id').hide();
        
        var myHash = location.hash;
        if(myHash.length>0){
            //взять переменные из location.hash
            myHash=myHash.substr(1,myHash.length-1);
            if(myHash.length>0){
                myHash = myHash.split('&');
                view = 'list'
                for(var i=0; i<myHash.length; i++){
                    hash_item = myHash[i].split('=');
                    if(hash_item[0]=='text'){
                        
                        text = decodeURIComponent(hash_item[1])
                        //text = hash_item[1]
                        $("#form_search_field_name").val(text);
                        $(".main_search_input").val(text);
                        // console.log($(".main_search_input").length)
                        // console.log(text)
                    }
                    if(hash_item[0]=='country'){
                        country = parseInt(hash_item[1]);
                        if(country < 0){
                            switch (country) {
                              case -2:
                                $("#select-country").prepend($('<option value="-2">Европа</option>'));
                                if($("#select-country [value='"+country+"']").attr("selected", "selected")){
                                    $('#select-country').selectize()
                                }
                                break;
                              case -3:
                                $("#select-country").prepend($('<option value="-3">Азия</option>'));
                                if($("#select-country [value='"+country+"']").attr("selected", "selected")){
                                    $('#select-country').selectize()
                                }
                                break;
                              case -4:
                                $("#select-country").prepend($('<option value="-4">Америка</option>'));
                                if($("#select-country [value='"+country+"']").attr("selected", "selected")){
                                    $('#select-country').selectize()
                                }
                                break;
                              default:
                                $('#select-country').selectize()
                                break;
                            }
                        }else{
                            if($("#select-country [value='"+country+"']").attr("selected", "selected")){
                                $('#select-country').selectize()
                            }
                        }
                        
                    }
                    if(hash_item[0]=='quick_place'){
                        quick_place = hash_item[1];
                        $("#quick_place").val(quick_place);
                    } 
                    if(hash_item[0]=='town'){
                        town = hash_item[1]
                        if($("#select-city [value='"+town+"']").attr("selected", "selected")){
                            $('#select-city').selectize()
                        }
                    }
                    if(hash_item[0]=='date1'){
                        date1 = hash_item[1];
                        //console.log(date1);
                        $("#date1_input").val(date1);
                        date_tmp = date1.split('.');
                        $("#date1_mini_hover").val(date_tmp[1]+'.'+date_tmp[0]+'.'+date_tmp[2]);
                    }
                    if(hash_item[0]=='date2'){
                        date2 = hash_item[1];
                        //console.log(date2);
                        $("#date2_input").val(date2);
                        date_tmp = date2.split('.');
                        $("#date2_mini_hover").val(date_tmp[1]+'.'+date_tmp[0]+'.'+date_tmp[2]);
                    }
                    if(hash_item[0]=='age1'){

    //$("#slider_age").slider("values",1,value2);
                        age1 = hash_item[1]
                        $("#minAge").val(age1)
                        $("#slider_age").slider("values",0,age1);
                    }
                    if(hash_item[0]=='age2'){
                        age2 = hash_item[1]
                        $("#maxAge").val(age2)
                    }

                    if(hash_item[0]=='price1'){
                        price1 = hash_item[1]
                        $("#minCost").val(price1)
                    }
                    if(hash_item[0]=='price2'){
                        price2 = hash_item[1]
                        $("#maxCost").val(price2)
                    }

                    if(hash_item[0]=='cat' && hash_item[1]!='undefined'){
                        cat = hash_item[1];
                        cat_arr = cat.split(',');
                        for(var k=0; k<cat_arr.length; k++){
                            $("#chbox_"+cat_arr[k]+'_mini').prop("checked","checked")
                        }
                    }
                    else{
                        cat = '';
                    }

                    if(hash_item[0]=='order_by_date') order_by_date = hash_item[1]
                    if(hash_item[0]=='filter_show'){
                        $(".form_filter_search_time #"+hash_item[1]).attr("checked", true);
                        filter_show = hash_item[1];
                    } 
                    if(hash_item[0]=='view') view = hash_item[1]

                    if(hash_item[0]=='zaoch'){
                        zaoch = hash_item[1]
                        $("#zaoch").val(zaoch)
                    }
                }
                
                if(view=='map'){
                    $('#form_search_mini').css('top', '91px');
                    $('#form_search_mini_id').css('height', '950px');
                    $('#list_fest_header').removeClass('list_fest_header_mini').addClass('list_fest_header_normal');
                }

                
                if(typeof(order_by_date)=='undefined')order_by_date='ASC';
                if(typeof(filter_show)=='undefined')filter_show='go';



                reviewSearchNewParams('hash', view, true, false)

            }
        }
        else{
            // console.log(myHash);
            text = $("#text").val();

            $(".main_search_input").val(text);
            
            country = $("#country").val();
            if(country=='')country=-1
            quick_place = $("#quick_place").val();
            
            town = $("#town").val();
            if(town=='')town=-1
            
            date1 = $("#date1").val();
            date2 = $("#date2").val();
            age1 = $("#age1").val();
            age2 = $("#age2").val();
            price1 = $("#price1").val();
            price2 = $("#price2").val();

            

            zaoch = $('#chbox_zaoch').val() == 'on' ? 1 : 0;

            view = "list"

            cat_array = []
            $(".search_category_check").each(function(){
                if($("#"+this.id).prop("checked")){
                    cat_array.push($("#"+this.id).val());
                }
            })
            cat = cat_array.join(',')

            if($("#select-country [value='"+country+"']").attr("selected", "selected")){
                $('#select-country').selectize()
            }

            // if($("#select-city [value='"+town+"']").attr("selected", "selected")){
            //     $('#select-city').selectize()
            // }

            order_by_date = 'ASC';
            //filter_show = text==''?'go':'all';
            
            filter_show = 'all';
            
           
            if(filter_show=='go'){
                //console.log('select GO');
                $("#go").prop("checked", "checked");
            }
            if(filter_show=='close'){
                $("#close").prop("checked", "checked");
            }
            if(filter_show=='all'){
                $("#all").prop("checked", "checked");
            }
                
            
            location.hash = 'text='+text+'&country='+country+'&quick_place='+quick_place+'&town='+town+'&date1='+date1+'&date2='+date2+'&age1='+age1+'&age2='+age2+'&price1='+price1+'&price2='+price2+'&cat='+cat+'&order_by_date='+order_by_date+'&filter_show='+filter_show+'&view='+view+'&zaoch='+zaoch;
            reviewSearchNewParams('first', view, false, false)
        }



        // слайдер цен
        var max_price = parseInt($('#max_price_real').val());
        slider_price = document.getElementById('slider_price_mini');
        noUiSlider.create(slider_price, {
            start: [price1, price2],
            step: 10,
            margin: 2,
            connect: true,
            direction: 'ltr',
            orientation: 'horizontal',
            range: {
                'min': 0,
                'max': max_price
            },
            format: wNumb({
                decimals: 0
            })
        });
        
        var price1_mini = document.getElementById('minCost'),
            price2_mini = document.getElementById('maxCost');
            

        slider_price.noUiSlider.on('update', function(values, handle){
            if (handle){
                price2_mini.value = values[handle];
            }else{
                price1_mini.value = values[handle];
            }
        });

        // When the input changes, set the slider value
        price1_mini.addEventListener('change', function(){
            slider_price.noUiSlider.set([this.value, null]);
        });
        price2_mini.addEventListener('change', function(){
            slider_price.noUiSlider.set([null, this.value]);
        });
        
        // слапйдер возраста
        slider_age = document.getElementById('slider_age_mini');
        noUiSlider.create(slider_age, {
            start: [age1, age2],
            step: 1,
            margin: 2,
            connect: true,
            direction: 'ltr',
            orientation: 'horizontal',
            range: {
                'min': 0,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });
        
        var age1_mini = document.getElementById('minAge'),
            age2_mini = document.getElementById('maxAge');
            

        slider_age.noUiSlider.on('update', function(values, handle){
            if (handle){
                age2_mini.value = values[handle];
            }else{
                age1_mini.value = values[handle];
            }
        });

        // When the input changes, set the slider value
        age1_mini.addEventListener('change', function(){
            slider_age.noUiSlider.set([this.value, null]);
        });
        age2_mini.addEventListener('change', function(){
            slider_age.noUiSlider.set([null, this.value]);
        });
        
        

        
        $(".form_filter_search_time").on("click", ".time_filter_search", function (event){
            filter_show = event.target.id
            
            if(filter_show=='go'){
                //console.log('select GO');
                $(".go").prop("checked", "checked");
            }
            if(filter_show=='close'){
                //console.log('select CLOSE');
                $(".close").prop("checked", "checked");
            }
            if(filter_show=='all'){
                //console.log('select ALL');
                $(".all").prop("checked", "checked");
            }
            
            reviewSearchNewParams('filter', view, false, false);
            
        });

        //renderParamsToDelete();
    }
});


var resultOnMap;
var showedResultOnMap = false;
var reloadMap = false;
var showedAllMarkers = false;
var nowSearching = false;
var xy_for_search;
var array_events=[];
var count_showed = 0;

var globalResultsArray;

var globalNotFound = false;

var return_town_find = false;

function reviewSearchNewParams(mode, view, reloadMap, from_switch){

    if(from_switch){
        //console.log('из переключателя');
        //console.log('result_has_been_showed_map = ',result_has_been_showed_map);
        //console.log('result_has_been_showed_list = ',result_has_been_showed_list);
        
        if(view == 'map'){
            if(result_has_been_showed_map){
                return true;
            }
            else{
                result_has_been_showed_map = true;
            }
        }
        else if(view == 'list'){
            if(result_has_been_showed_list){
                return true;
            }
            else{
                result_has_been_showed_list = true;
            }
        }
    }
    else{
        if(view == 'map'){
            result_has_been_showed_map = true;
            result_has_been_showed_list = false;
        }
        else{
            result_has_been_showed_map = false;
            result_has_been_showed_list = true;
        }
    }
    
    //console.log('result_has_been_showed_map = ',result_has_been_showed_map);
    //console.log('result_has_been_showed_list = ',result_has_been_showed_list);
    
    
    nowSearching = true;
    
    first = 0;
    //console.log('reviewListNewParams');
    if(mode=='date'){
        if(order_by_date=='ASC'){
            order_by_date = 'DESC';
            $(".order_filter_inner_date").removeClass("disable").removeClass("asc").addClass("desc");
            setHashValue('order_by_date', 'DESC');
        }
        else{
            order_by_date = 'ASC';
            $(".order_filter_inner_date").removeClass("disable").removeClass("desc").addClass("asc");
            setHashValue('order_by_date', 'ASC')
        }
    }
    else if(mode=='filter'){
        setHashValue('filter_show', filter_show);
    }
    else if(mode=='hash' || mode=='first'){
        if(order_by_date=='ASC'){
            $(".order_filter_inner_date").removeClass("disable").removeClass("asc").addClass("asc");
        }
        else{
            $(".order_filter_inner_date").removeClass("disable").removeClass("desc").addClass("desc");
        }

        if(mode=='first'){
            first = 1;
        }
    }

    text = $(".main_search_input").val();
    window.document.title='Результаты поиска'+(text!=''?(' по фразе "'+text+'"'):'');
    country = $("#select-country").val();
    if(country=='')country=-1;

    quick_place = $("#quick_place").val();


    if(first == 1) {
        if(town != -1) {
            $("#select-city option").each(function(i, e){
                if($(e).val() == town){town_name = $(e).text()}
            })

            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;

            control.setValue([town, town_name]);
        }
        else{
            town=-1;
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
            setHashValue('town','-1');
        }
        
    }
    else {
        town = $("#select-city").val();
        if(textintext('_', town)) {
            town = town.split('_')[1]
        }
        
        if(town==''){
            town=-1;
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
            setHashValue('town','-1');
        }
        else {
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
            setHashValue('town',town);
        }
    }

    date1 = $("#date1_input").val();
    date2 = $("#date2_input").val();
    age1 = $("#minAge").val();
    age2 = $("#maxAge").val();
    price1 = $("#minCost").val();
    price2 = $("#maxCost").val();
    zaoch = $("#chbox_zaoch").attr('checked') == 'checked' ? 1 : 0;
    
    

    cat_array = []
    $(".search_mini_category_check").each(function(){
        if($("#"+this.id).prop("checked")){
            cat_array.push($("#"+this.id).val());
        }
    })
    cat = cat_array.join(',')
    location.hash = 'text='+text+'&country='+country+'&quick_place='+quick_place+'&town='+town+'&date1='+date1+'&date2='+date2+'&age1='+age1+'&age2='+age2+'&price1='+price1+'&price2='+price2+'&cat='+cat+"&order_by_date="+order_by_date+"&filter_show="+filter_show+'&view='+view+'&zaoch='+zaoch;
    
    renderParamsToDelete();

    //console.log("Показано "+count_showed)
    $("#show_more_results").css("display", "none");
    $("#block_not_found").remove();
    var jsondata_g;
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: 'mode=get_search_new_10&text='+text+'&country='+country+'&quick_place='+quick_place+'&town='+town+'&date1='+date1+'&date2='+date2+'&age1='+age1+'&age2='+age2+'&price1='+price1+'&price2='+price2+'&curs_eur='+curs_eur+'&curs_usd='+curs_usd+'&cat='+cat+'&order_by_date='+order_by_date+'&filter_show='+filter_show+"&first="+first+'&zaoch='+zaoch,
        before: startLoader(),
        success: function(jsondata){
            jsondata_g = jsondata;
            if(jsondata.status==1){
                globalResultsArray = jsondata;
                
                if(jsondata.return_arr.year!=-1 && (typeof jsondata.return_arr.year != "undefined")){
                    // Установить даты каледнаря если определился год в запросе
                    date1_calendar = jsondata.return_arr.year+'-01-01';
                    date2_calendar = jsondata.return_arr.year+'-12-31';
                    // k.setSelected(date1_calendar+','+date2_calendar)

                    setHashValue('date1', '01.01.'+jsondata.return_arr.year);
                    setHashValue('date2', '31.12.'+jsondata.return_arr.year);

                    date1 = '01.01.'+jsondata.return_arr.year;
                    date2 = '31.12.'+jsondata.return_arr.year;

                    renderParamsToDelete();
                }
                //console.log(jsondata.return_arr.categories);
                if(typeof jsondata.return_arr.categories != 'undefined' && jsondata.return_arr.categories.length>0){
                    for(var tmp = 0; tmp < jsondata.return_arr.categories.length; tmp++){
                        //console.log(jsondata.return_arr.categories[tmp])
                        $('#chbox_'+jsondata.return_arr.categories[tmp]).prop("checked", true);
                    }
                    cat = jsondata.return_arr.categories.join(',')
                    setHashValue('cat',cat);

                    renderParamsToDelete();
                }
                
                if(typeof jsondata.return_arr.country != 'undefined' && jsondata.return_arr.country.length>0){
                    $('#select-country').selectize();
                    var $select = $('#select-country').selectize();
                    var control = $select[0].selectize;
                    control.setValue([jsondata.return_arr.country, jsondata.return_arr.country_name]);
                    renderParamsToDelete();
                }
                
                if(typeof jsondata.return_arr.town != 'undefined' && jsondata.return_arr.town.length>0){
                    $('#select-city').selectize();
                    var $select = $('#select-city').selectize();
                    var control = $select[0].selectize;
                    console.log(jsondata.return_arr.town, jsondata.return_arr.town_name);
                    control.setValue([jsondata.return_arr.town, jsondata.return_arr.town_name]);
                    return_town_find = true;
                    // console.log('Пришел город с сервера');
                    renderParamsToDelete();
                }
                else{
                    return_town_find = false;
                    //console.log('НЕ пришел город с сервера');
                }
                
                // показать шапку
                $('#list_fest_header').show();
                $('.list_fest_header_count').css("display", "block").html('Найдено '+jsondata.count + ' ' + numberOf(jsondata.count, 'результат', ['','а','ов']));
                if(view=='list'){
                    
                    // изменить ссылку
                    //...
                    
                    putResultsToList(globalResultsArray);
                }
                else if(view=='map'){
                    // изменить ссылку
                    //...
                    
                    // проверить найдено ли что-то по городам и странам (искалось ли) если да то перезагрузка карты, если нет то по умолчанию
                    if(jsondata.return_arr.reloadMap == 1){
                        reloadMap = true;
                    }
                    if(form_mini_show) setTimeout("show_hide_form_search_mini()", 2000);
                    putResultsToMap(globalResultsArray, reloadMap);
                }
                globalNotFound = false;
            }
            else{
                $('#list_fest_header').hide();
                globalNotFound = true;
                $('#list_fest').html('<br><div id="block_not_found">Результатов по данному запросу не найдено. <br>Попробуйте задать более широкие параметры поиска: увеличить интервал дат, стоимости, <br>расширить географический диапазон и т.д.<br><br>Возможно, фестиваля, который Вы ищете, нет среди активных. Попробуйте поискать среди прошедших фестивалей (вкладка "все" над результатами поиска)</div>').show();
                
                $("#map_search").css("display", "none");
                $("#show_more_results").css("display", "none");
            }
            stopLoader();
            
            //console.log('before scroll');
            order_filter_inner_date = $("#before_order_filter").offset();
            $('body,html').animate({scrollTop:order_filter_inner_date.top-75},800);
            
            
            
        },
        complete: function(){
            nowSearching = false;
            $("#show_more_results span").html('Показать еще 10 <img class="treug" src="/d/i/show_more_treug.png">');
            reCallTooltip();
            //$("a").LiteTooltip({ backcolor: "#ffffff", textcolor: "#000000", opacity: 1, padding: 20});
            makeDotDotDotAndFav();
            
            top_fixed_warning = $("#before_order_filter").offset();
            if(view=='list'){
                reviewMainSearchGlobal();
            }

            // убрать из списка городов лишние
            var town_i;
            var town_name = ''
            var finded = 0;

            if(country < 0){
                $('#select-city').html('');
                var $select = $('#select-city').selectize();
                var control = $select[0].selectize;
                control.clearOptions();
                $.ajax({
                    url: '/admin/modules/ajax/country.php',
                    type: "POST",
                    data: "mode=get_towns_by_part_of_world&part="+country,
                    success: function(jsondata){
                        finded = jsondata.id_dog_res.length;
                        for(var i=0; i<jsondata.id_dog_res.length; i++){
                            if(town == jsondata.id_dog_res[i]['id']){
                                town_name = jsondata.id_dog_res[i]['name']
                                town_i = i
                            }
                            control.addOption({
                                value: i+'_'+jsondata.id_dog_res[i]['id'],
                                text: jsondata.id_dog_res[i]['name']
                            });
                            
                        }
                    },
                    complete: function(){
                        if(finded > 0) {
                            control.setValue([town_i+'_'+town, town_name]);
                        }
                    },
                    async: false,
                    dataType: 'json'
                });
            }
            if(country > 0){
                $('#select-city').html('');
                var $select = $('#select-city').selectize();
                var control = $select[0].selectize;
                control.clearOptions();
                $.ajax({

                    


                    url: '/admin/modules/ajax/country.php',
                    type: "POST",
                    data: "mode=get_towns_by_country&country="+country,
                    success: function(jsondata){
                        finded = jsondata.id_dog_res.length;
                        for(var i=0; i<jsondata.id_dog_res.length; i++){
                            if(town == jsondata.id_dog_res[i]['id']){
                                town_name = jsondata.id_dog_res[i]['name']
                                town_i = i
                            }
                            control.addOption({
                                value: i+'_'+jsondata.id_dog_res[i]['id'],
                                text: jsondata.id_dog_res[i]['name']
                            });
                            
                        }
                    },
                    complete: function(){
                        if(finded > 0) {
                            control.setValue([town_i+'_'+town, town_name]);
                        }
                    },
                    async: false,
                    dataType: 'json'
                });
            }


        },
        async: true,
        dataType: 'json'
    });
}

function testtt(){
    var $select = $('#select-city').selectize();
    var control = $select[0].selectize;
    control.addOption({
        value: 9999,
        text: 'qqqqq'
    });
    control.setValue([9999, 'qqqqq']);
}

function putResultsToList(jsondata){
    if(jsondata.count>0){
        $("#block_not_found").remove();
        $('#list_fest').html(jsondata.res);
        
        
        count_showed = jsondata.events_id_first_10;
        //console.log(count_showed)
        array_events = jsondata.all_events_id;
      
        //console.log(array_events);

        if(parseInt(jsondata.count)<11){
            $("#show_more_results").css("display", "none");
            the_end=true;
        }
        else{
            $("#show_more_results").css("display", "block");
            the_end=false;
        }
        
        $('#list_fest, #list_results').css({"display": "block"});
        $('#map_search').css({"display": "none"});
        showedResultOnMap = false;
    }
    else{
        $('.list_fest_header_count').css({"display": "none"});
        $('#map_search').css({"display": "none"});
        $('#list_fest').css({"display": "block"}).html('<br><div id="block_not_found">Результатов по данному запросу не найдено. <br>Попробуйте задать более широкие параметры поиска: увеличить интервал дат, стоимости, <br>расширить географический диапазон и т.д.<br><br>Возможно, фестиваля, который Вы ищете, нет среди активных. Попробуйте поискать среди прошедших фестивалей (вкладка "все" над результатами поиска)</div>');
    }
}
var mapHasBeenInitialized = false;

var showedMarkersOnMapSearch = false;

function putResultsToMap(jsondata, reloadMap){
    //console.log(reloadMap);
    //console.log(jsondata);
    if(jsondata.count>0){
        //console.log(resultOnMap.length)
        $('#list_fest, #list_results, #show_more_results').css({"display": "none"});

        $('#map_search').css({"display": "block"});
        if(!mapHasBeenInitialized){mapInitialize();mapHasBeenInitialized = true;}
        showedResultOnMap = true;
        
        // Очистить карту
        if(markers_result_search.length > 0 && showedMarkersOnMapSearch){
            clearMarkersSearch();
            $("#map_search_list").html('');
        }
        
        
        
        // создать массив маркеров id, x, y, html (может включать более 1 фестиваля)
        
        // ОПРЕДЕЛЕНИЕ ГРАНИЦ КАРТЫ
        if(reloadMap){
            
            // выбрать первые 10 фестивалей и сделать массив для показа на карте id, x, y
            var arr_from_first_10 = [];
            for(var i = 0; i < (jsondata['all_events_id'].length>9?10:jsondata['all_events_id'].length); i++){
                //console.log(i, jsondata['all_events_id'][i][1]);
                // если координат несколько то разбить

                if(textintext(';',jsondata['all_events_id'][i][1])){
                    var tmp_coords = jsondata['all_events_id'][i][1].split(';');
                    for(var y = 0; y < tmp_coords.length; y++){
                        arr_from_first_10.push(new Array(jsondata['all_events_id'][i][0], tmp_coords[y]));
                    }
                }
                else{
                    arr_from_first_10.push(new Array(jsondata['all_events_id'][i][0], jsondata['all_events_id'][i][1]));
                }
            }
            //console.log(arr_from_first_10);
            
            var min_x = 180;
            var max_x = 0;
            var min_y = 90;
            var max_y = 0;
            
            // Найти координаты верхнего правого и нижнего левого угла
            for(var u = 0; u < arr_from_first_10.length; u++){
                if(arr_from_first_10[u][1]!=null){
                    var cur_xy = arr_from_first_10[u][1].split(',');
                    var cur_x = parseFloat($.trim(cur_xy[1]));
                    var cur_y = parseFloat($.trim(cur_xy[0]));
                    
                    if(cur_x<min_x){min_x = cur_x;}
                    if(cur_y<min_y){min_y = cur_y;}
                    if(cur_x>max_x){max_x = cur_x;}
                    if(cur_y>max_y){max_y = cur_y;}
                }
            }
            
            //console.log("Границы области: lb: ", min_y, min_x, " tr: ", max_y, max_x);
            
            center_x = (max_x-min_x)/2;
            center_y = (max_y-min_y)/2;
            
            cur_ratio = (max_y-min_y)/(max_x-min_x);
            if(cur_ratio>0.59){ // нужно увеличить ширину
                new_width = (max_y - min_y)/0.59;
                max_x += (new_width - (max_x-min_x))/2;
                min_x -= (new_width - (max_x-min_x))/2;
            }
            else if(cur_ratio<0.59){ // увеличить по высоте
                new_height = (max_x - min_x)*0.59;
                max_y += (new_height - (max_y-min_y))/2;
                min_y -= (new_height - (max_y-min_y))/2;
            }
            
            //console.log("Новые границы области: lb: ", min_y, min_x, " tb: ", max_y, max_x);
        }
        else{
            // ГРАНИЦА КАРТЫ - КАК ЕСТЬ
                        
            var mapBounds = getBound(map_search);
            
            min_x = mapBounds.minX;
            min_y = mapBounds.minY;
            
            max_x = mapBounds.maxX;
            max_y = mapBounds.maxY;
            
            
            //console.log("Текущие координаты границ: ", mapBounds, mapBounds.O.O, mapBounds.j.j, mapBounds.O.j, mapBounds.j.O);
            //console.log("Текущие координаты границ: ", mapBounds.getNorthEast(), mapBounds.getSouthWest());
            //console.log("Новые границы области: ", min_y, min_x, " ", max_y, max_x);
            
        }
        
        
        
        
        
        // Выбрать ВСЕ фестивали, подходящие под эту область
        arr_markers_to_map = [];
        arr_markers_to_map.length = 0;
        
        if(jsondata['all_events_id'].length > 0){
            for(var i = 0; i < jsondata['all_events_id'].length; i++){
                if(textintext(';',jsondata['all_events_id'][i][1])){
                    var tmp_coords = jsondata['all_events_id'][i][1].split(';');
                    for(var y = 0; y < tmp_coords.length; y++){
                        cur_xy = tmp_coords[y].split(',');
                        cur_y = $.trim(cur_xy[0])
                        cur_x = $.trim(cur_xy[1])
                        
                        // входит ли точка в прямоугольник
                        if(dotInArea(cur_y, cur_x, min_y, min_x, max_y, max_x)){
                            arr_markers_to_map.push(new Array(jsondata['all_events_id'][i][0], cur_y, cur_x));
                        }
                    }
                }
                else{
                    if(jsondata['all_events_id'][i][1] != null){
                        var tmp_coords = jsondata['all_events_id'][i][1];
                        cur_xy = tmp_coords.split(',');
                        cur_y = $.trim(cur_xy[0])
                        cur_x = $.trim(cur_xy[1])
                        // входит ли точка в прямоугольник
                        if(dotInArea(cur_y, cur_x, min_y, min_x, max_y, max_x)){
                            arr_markers_to_map.push(new Array(jsondata['all_events_id'][i][0], cur_y, cur_x));
                        }
                    }
                    
                }
            }
        }
        
        if(arr_markers_to_map.length > 0){
            // Маркеры для данного участка карты есть
            //console.log("Точки для показа:", arr_markers_to_map.length,  arr_markers_to_map);
            // Для каждой точки сделать html
            var ids_to_map = [];
            for (var t = 0; t < arr_markers_to_map.length; t++){
                ids_to_map.push(arr_markers_to_map[t][0])
            }
            //console.log("id фестивалей для показа:", ids_to_map);
            
            getHtmlForMapByIDS(ids_to_map);
            //console.log(allEventsToMapHTML);
            
            var finishArrMarkers = [];
            
            for(var i in arr_markers_to_map){
                // Проверить, нет ли маркера с этими коодинатами
                var already_exist = false;
                for(var e = 0; e < finishArrMarkers.length; e++){
                    if(finishArrMarkers[e]['y'] == arr_markers_to_map[i][1] && finishArrMarkers[e]['x'] == arr_markers_to_map[i][2]){
                        
                        finishArrMarkers[e]['id'] += '_'+allEventsToMapHTML['result'][i]['id'];
                        
                        
                        
                        
                        finishArrMarkers[e]['count_events_by_marker'] = parseInt(finishArrMarkers[e]['count_events_by_marker'])+1;
                        finishArrMarkers[e]['html'] =  finishArrMarkers[e]['html']+
                            '<div class="event_marker_html_item item_'+finishArrMarkers[e]['count_events_by_marker']+'" style="display: none" id="'+e+'_'+finishArrMarkers[e]['count_events_by_marker']+'">'+
                            '<a href="/events/'+allEventsToMapHTML['result'][i]['id']+'">'+allEventsToMapHTML['result'][i]['name']+'</a><br>'+
                                allEventsToMapHTML['result'][i]['html_locations'] + 
                                '<img src="/d/i/dates3.png">' + allEventsToMapHTML['result'][i]['date_start'] + ' - ' + allEventsToMapHTML['result'][i]['date_end']+
                                '</div>',
                        already_exist = true;
                    }
                }
                
                if(!already_exist){
                    finishArrMarkers.push({
                        'id': allEventsToMapHTML['result'][i]['id'],
                        'html': 
                            '<div class="event_marker_html_item item_1" id="'+i+'_1">'+
                            '<a href="/events/'+allEventsToMapHTML['result'][i]['id']+'">'+allEventsToMapHTML['result'][i]['name']+'</a><br>'+
                                allEventsToMapHTML['result'][i]['html_locations'] + 
                                '<img src="/d/i/dates3.png">' + allEventsToMapHTML['result'][i]['date_start'] + ' - ' + allEventsToMapHTML['result'][i]['date_end']+
                                '</div>',
                        'y': arr_markers_to_map[i][1],
                        'x': arr_markers_to_map[i][2],
                        'count_events_by_marker': 1
                    });
                }
            }
            
            //console.log(finishArrMarkers);
            
            
            // СПИСОК
            
            // неповторяющиеся фестивали для списка
            
            //console.log("Все фестивали ", allEventsToMapHTML);
            
            
            var ids_to_list = [];
            var list_items = [];
            for(var i = 0; i < allEventsToMapHTML['result'].length; i++){
                if(!in_array(allEventsToMapHTML['result'][i]['id'],ids_to_list)){
                    ids_to_list.push(allEventsToMapHTML['result'][i]['id']);
                    list_items.push(allEventsToMapHTML['result'][i]);
                }
            }
            
            var list_items_html = '';
            for(var y in list_items){
                //console.log(list_items[y]);
                list_items_html += '<div class="map_search_list_item" id="map_search_list_item_'+list_items[y]['id']+'">'+
                '<a href="/events/'+list_items[y]['field']+'" class="name_event">'+list_items[y]['name']+'</a>'+
                '<br>'+list_items[y]['html_locations']+
                '<img style="margin: 0 8px 0 3px" src="/d/i/dates3.png"><span class="map_search_list_item_dates">' + twoDatesToString(list_items[y]['date_start'], list_items[y]['date_end']) + '</span></div>';
            }
            
            $("#map_search_list").html(list_items_html);
            setListenersForMapListItems();
            
            
            
            // Показ маркеров
            if(finishArrMarkers.length > 0){
                for(var i in finishArrMarkers){
                    var counter = '';
                    if(finishArrMarkers[i]['count_events_by_marker']>1){
                        counter = 'Показано: <div id="current_show_'+i+'">1</div> из '+ finishArrMarkers[i]['count_events_by_marker'] 
                            + '<a onclick = "prev_event_map('+i+')"><<</a>'
                            + '<a onclick = "next_event_map('+i+')">>></a>'
                    }
                    
                    showNewMarker(map_search, 
                        //'01', 
                        'm1_'+finishArrMarkers[i]['count_events_by_marker'],
                        finishArrMarkers[i]['y'], 
                        finishArrMarkers[i]['x'], 
                        'qweqwe', 
                        '<div class="marker_popup_block" id="marker_popup_block_'+i+'">'+ finishArrMarkers[i]['html']+'</div>'+counter,
                        finishArrMarkers[i]['id'] 
                    );
                }
                showedMarkersOnMapSearch = true;
            }
            
            
            
            setTimeout("setListenerForChangeMapPosition()", 2000);
            setTimeout("mapBoundsGlobal = getBound(map_search);", 1000);
            setListenerClickMarkers();
            
            if(reloadMap){
                var bound = new google.maps.LatLngBounds();
                for(var i in markers_result_search){
                    bound.extend(markers_result_search[i].getPosition());
                }
                map_search.fitBounds(bound);
            }
            
            markerCluster(map_search, markers_result_search);
        }
        /*
        else {
            console.log("передвижение карты, маркеров не нашлось. спросить о расширении");
            if(confirm("Для данного участка карты результатов не найдено. Расширить зону поиска?")) {
                reloadMapForShowExistMarkers();
            }
        }
        */
    }
    else{
        $.goMap.clearMarkers(); 
        $('#list_fest').css({"display": "block"});
        $('#list_fest').html('<br><div id="block_not_found">Результатов по данному запросу не найдено. <br>Попробуйте задать более широкие параметры поиска: увеличить интервал дат, стоимости, <br>расширить географический диапазон и т.д.<br><br>Возможно, фестиваля, который Вы ищете, нет среди активных. Попробуйте поискать среди прошедших фестивалей (вкладка "все" над результатами поиска)</div>');
    }
}

function clearMarkersSearch() {
    for (i in markers_result_search) {
        markers_result_search[i].setMap(null);
    }
    markerClustererSearch.clearMarkers();
    markers_result_search = [];
}

var maxZIndex = 1000;
function setListenersForMapListItems(){
    $(".map_search_list_item").mouseover(function(){
        
        ids = this.id.split('_');
        id_target = ids[ids.length-1]
        //alert(id);
        // Подсветить маркеры этого фестиваля (их может быть сколько угодно)
        //console.log(markers_result);
        // Перебрать все маркеры
        for (i in markers_result_search) {
            // проверить есть нужный id фестиваля в этом маркере
            //console.log(markers_result[i])
            var ids_evnt_from_marker = markers_result_search[i].event_id.split('_');
            if(in_array(id_target, ids_evnt_from_marker)){
                markers_result_search[i].setIcon('/d/i/maps/m2.png');
                markers_result_search[i].setOptions({zIndex:maxZIndex++});
            }
        }
    });
    $(".map_search_list_item").mouseleave(function(){
        for (i in markers_result_search) {
            var ids_evnt_from_marker = markers_result_search[i].event_id.split('_');
            markers_result_search[i].setIcon('/d/i/maps/m1_'+ids_evnt_from_marker.length+'.png');
        }
    });
}

























function prev_event_map(id_marker){
    // сколько всего подблоков
    count_sub_block = $('#marker_popup_block_'+id_marker+' .event_marker_html_item').length;
    // какой показан
    id_showed = $('#marker_popup_block_'+id_marker+' .event_marker_html_item:visible').attr('id')
    id_showed = id_showed.split('_')
    id_showed = id_showed[1]
    
    console.log("Показан: ", id_showed);

    // скрыть текущий
    $('#marker_popup_block_'+id_marker+' .item_'+id_showed).css('display', 'none')
    // показать предыдущий
    id_prev = id_showed==1?count_sub_block:(parseInt(id_showed)-1)
    $('#marker_popup_block_'+id_marker+' .item_'+id_prev).css('display', 'block')
    // счетчик
    $('#current_show_'+id_marker).html(id_prev)
}

function next_event_map(id_marker){
    // сколько всего подблоков
    count_sub_block = $('#marker_popup_block_'+id_marker+' .event_marker_html_item').length;
    // какой показан
    id_showed = $('#marker_popup_block_'+id_marker+' .event_marker_html_item:visible').attr('id')
    id_showed = id_showed.split('_')
    id_showed = id_showed[1]
    
    // скрыть текущий
    $('#marker_popup_block_'+id_marker+' .item_'+id_showed).css('display', 'none')
    // показать следующий
    id_next = id_showed==count_sub_block?1:(parseInt(id_showed)+1)
    $('#marker_popup_block_'+id_marker+' .item_'+id_next).css('display', 'block')
    // счетчик
    $('#current_show_'+id_marker).html(id_next)
}







function search_submit(){
    if($("#is_search_results").length>0){
        reviewSearchNewParams('hash', view, false, false);
    }
    else{
        $('#form_search_id').submit();
    }
}

function search_submit_mini(){
    if($("#is_search_results").length>0){
        reviewSearchNewParams('hash', view, false, false);
    }
    else{
        $('#form_search_mini_id').submit();
    }
}


function showMoreEventsSearch(){
    console.log('showMoreEventsList');
    
    console.log("Показано "+count_showed)
    console.log(array_events)
    var tmp_arr=[];
    for(var i=count_showed; i<count_showed+10; i++){
        console.log(array_events[i]);
        if(typeof array_events[i] != "undefined"){
            tmp_arr.push(array_events[i][0])
        }
        else{
            the_end = true;
        }
    }
    console.log(tmp_arr)
    
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: "mode=get_arr_events&arr="+tmp_arr,
        beforeSend: function(){
            $("#show_more_results span").html('<img class="loader" src="/d/i/294.GIF">');
        },
        success: function(jsondata){
            if(jsondata.status==1){
                //console.log(jsondata.res);
                $('#list_fest').append(jsondata.res);
                count_showed += parseInt(jsondata.count);

                //console.log(showed_key);
                if(parseInt(jsondata.count)<10){
                    $("#show_more_results").css("display", "none");
                }
            }
            else{
                $('#list_fest').append('<br><div id="block_not_found">Результатов по данному запросу больше не найдено. <br>Попробуйте задать более широкие параметры поиска: увеличить интервал дат, стоимости, <br>расширить географический диапазон и т.д.<br><br>Возможно, фестиваля, который Вы ищете, нет среди активных. Попробуйте поискать среди прошедших фестивалей (вкладка "все" над результатами поиска)</div>');
                $("#show_more_results").css("display", "none");
            }
        },
        complete: function(){
            //alert(jsondata.status);
            $("#show_more_results span").html('Показать еще 10 <img class="treug" src="/d/i/show_more_treug.png">');
            reCallTooltip();
            //$("a").LiteTooltip({ backcolor: "#ffffff", textcolor: "#000000", opacity: 1, padding: 20});
            makeDotDotDotAndFav();

        },
        async: true,
        dataType: 'json'
    });
}

function renderParamsToDelete(){
    //console.log("renderParamsToDelete");
    var params_to_delete_exist = false;
    $(".param_search_to_delete_block, .param_search_to_delete_block_ext").html('');
    
    if(cat!=''&&cat!='undefined'){
        params_to_delete_exist = true;
        cat_arr=cat.split(',')
        
        var zhanr = '<div class="param_search_to_delete"><b>Жанры: </b>';
        
        for(var i=0; i<cat_arr.length; i++){
            if(cat_arr[i]==1){zhanr += 'Хоровые <a onclick="clearItemHash(\'cat_1\')"><img src="/d/i/delete.png"></a>'}
            if(cat_arr[i]==2){zhanr += 'Вокальные <a onclick="clearItemHash(\'cat_2\')"><img src="/d/i/delete.png"></a>'}
            if(cat_arr[i]==3){zhanr += 'Хореографические <a onclick="clearItemHash(\'cat_3\')"><img src="/d/i/delete.png"></a>'}
            if(cat_arr[i]==4){zhanr += 'Инструментальные <a onclick="clearItemHash(\'cat_4\')"><img src="/d/i/delete.png"></a>'}
            if(cat_arr[i]==5){zhanr += 'Фольклорные <a onclick="clearItemHash(\'cat_5\')"><img src="/d/i/delete.png"></a>'}
            if(cat_arr[i]==6){zhanr += 'Другие <a onclick="clearItemHash(\'cat_6\')"><img src="/d/i/delete.png"></a>'}
            if(i==(cat_arr.length-1)){
                zhanr += '</div>'
            }
        }
        $(".param_search_to_delete_block").append(zhanr)
        
        
        /*
        $(".param_search_to_delete_block").append('<div class="param_search_to_delete">Жанры: ')
        
        for(var i=0; i<cat_arr.length; i++){
            if(cat_arr[i]==1){$(".param_search_to_delete_block").append('Хоровые <a onclick="clearItemHash(\'cat_1\')"><img src="/d/i/delete.png"></a>')}
            if(cat_arr[i]==2){$(".param_search_to_delete_block").append('Вокальные <a onclick="clearItemHash(\'cat_2\')"><img src="/d/i/delete.png"></a>')}
            if(cat_arr[i]==3){$(".param_search_to_delete_block").append('Хореографические <a onclick="clearItemHash(\'cat_3\')"><img src="/d/i/delete.png"></a>')}
            if(cat_arr[i]==4){$(".param_search_to_delete_block").append('Инструментальные <a onclick="clearItemHash(\'cat_4\')"><img src="/d/i/delete.png"></a>')}
            if(cat_arr[i]==5){$(".param_search_to_delete_block").append('Фольклорные <a onclick="clearItemHash(\'cat_5\')"><img src="/d/i/delete.png"></a>')}
            if(cat_arr[i]==6){$(".param_search_to_delete_block").append('Другие <a onclick="clearItemHash(\'cat_6\')"><img src="/d/i/delete.png"></a>')}
            if(i==(cat_arr.length-1)){
                $(".param_search_to_delete_block").append('</div>')
            }            
        }
        */
    }
    
    
    
    if(text!=''){
        params_to_delete_exist = true;
        $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Текст:</b> '+text+' <a onclick="clearItemHash(\'text\')"><img src="/d/i/delete.png"></a></div>')
    } 
    if(quick_place!='undefined'){
        params_to_delete_exist = true;
        if(quick_place==-2) $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Место: </b>Европа <a onclick="clearItemHash(\'quick_place\')"><img src="/d/i/delete.png"></a></div>')
        if(quick_place==-3) $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Место: </b>Азия <a onclick="clearItemHash(\'quick_place\')"><img src="/d/i/delete.png"></a></div>')
        if(quick_place==-4) $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Место: </b>Америка <a onclick="clearItemHash(\'quick_place\')"><img src="/d/i/delete.png"></a></div>')
        if(quick_place==-5) $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Место: </b>Африка <a onclick="clearItemHash(\'quick_place\')"><img src="/d/i/delete.png"></a></div>')
        if(quick_place==-6) $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Место: </b>Австралия <a onclick="clearItemHash(\'quick_place\')"><img src="/d/i/delete.png"></a></div>')
    }
    if(country>-1){
        params_to_delete_exist = true;
        $.ajax({
            url: '/modules/ajax/fest_list.php',
            type: "POST",
            data: 'mode=get_country_name&id='+country,
            success: function(jsondata){
                if(jsondata.status==1){
                    $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Страна:</b> '+jsondata.country_name+' <a onclick="clearItemHash(\'country\')"><img src="/d/i/delete.png"></a></div>')
                }
            },
            async: false,
            dataType: 'json'
        });
    }

    if(town>-1 && town!='' ){
        params_to_delete_exist = true;
        $.ajax({
            url: '/modules/ajax/fest_list.php',
            type: "POST",
            data: 'mode=get_town_name&id='+town,
            success: function(jsondata){
                if(jsondata.status==1){
                    $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Город:</b> '+jsondata.town_name+' <a onclick="clearItemHash(\'town\')"><img src="/d/i/delete.png"></a></div>')
                }
            },
            async: false,
            dataType: 'json'
        });
    }
    
    
    
    if(date1!=''&&date2!=''){
        
        // 4 комбинации дат
        if(date1==min_date_real_date2&&date2==max_date_real_date2){}
        if(date1!=min_date_real_date2&&date2==max_date_real_date2){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Дата проведения: </b> с '+date1+' <a onclick="clearItemHash(\'date1\')"><img src="/d/i/delete.png"></a></div>')
        }
        if(date1==min_date_real_date2&&date2!=max_date_real_date2){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Дата проведения: </b> по '+date2+' <a onclick="clearItemHash(\'date2\')"><img src="/d/i/delete.png"></a></div>')
        }
        if(date1!=min_date_real_date2&&date2!=max_date_real_date2){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Дата проведения: </b> с '+date1+' <a onclick="clearItemHash(\'date1\')"><img src="/d/i/delete.png"></a> по '+date2+' <a onclick="clearItemHash(\'date2\')"><img src="/d/i/delete.png"></a></div>')
        }
    }

    

    if(age1!=''&&age2!=''){
        
        if(age1==0&&age2==100){}
        if(age1!=0&&age2==100){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Возраст: </b> от '+age1+' <a onclick="clearItemHash(\'age1\')"><img src="/d/i/delete.png"></a></div>')
        }
        if(age1==0&&age2!=100){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Возраст: </b> до '+age2+' <a onclick="clearItemHash(\'age2\')"><img src="/d/i/delete.png"></a></div>')
        }
        if(age1!=0&&age2!=100){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Возраст: </b> от '+age1+' <a onclick="clearItemHash(\'age1\')"><img src="/d/i/delete.png"></a> до '+age2+' <a onclick="clearItemHash(\'age2\')"><img src="/d/i/delete.png"></a></div>')
        }
    }



    if(price1!=''&&price2!=''){
        
        if(price1==min_price_real&&price2==max_price_real){}
        if(price1!=min_price_real&&price1!=0&&price2==max_price_real){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Цена: </b> от '+price1+' <a onclick="clearItemHash(\'price1\')"><img src="/d/i/delete.png"></a></div>')
        }
        if(price1==min_price_real&&price2!=max_price_real){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Цена: </b> до '+price2+' <a onclick="clearItemHash(\'price2\')"><img src="/d/i/delete.png"></a></div>')
        }
        if(price1!=min_price_real&&price1!=0&&price2!=max_price_real){
            params_to_delete_exist = true;
            $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Цена: </b> от '+price1+' <a onclick="clearItemHash(\'price1\')"><img src="/d/i/delete.png"></a> до '+price2+' <a onclick="clearItemHash(\'price2\')"><img src="/d/i/delete.png"></a></div>')
        }
    }

    if(zaoch == 1){
        params_to_delete_exist = true;
        $(".param_search_to_delete_block").append('<div class="param_search_to_delete"><b>Заочный</b> <a onclick="clearItemHash(\'zaoch\')"><img src="/d/i/delete.png"></a></div>')
    }
    
    if(params_to_delete_exist){
        $(".param_search_to_delete_block_ext").append('<div class="param_search_to_delete_all"><a onclick="clearItemHash(\'all\')">Сбросить фильтры</a></div>')
    }
    
    $(".param_search_to_delete_block_ext").append('<div id="back_to_form" onclick="scrollToFormAndShow();"><a>Изменить параметры поиска</a></div>')
    
    
    
    
    $(".param_search_to_delete_block").append('<div style="clear:both"></div>')


}

function clearItemHash(item){
    if(item=='all'){
        text='';
        $("#form_search_field_name").val('');
        setHashValue('text','');
        $("#quick_place").val('undefined');
        quick_place='undefined';
        setHashValue('quick_place','undefined')
        $('#select-country').selectize();
        var $select = $('#select-country').selectize();
        var control = $select[0].selectize;
        control.setValue([-1, '(Не выбрано)']);
        country = -1
        if($("#select-country [value='-1']").attr("selected", "selected")){
            $('#select-country').selectize();
            var $select = $('#select-country').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
        }
        setHashValue('country','-1');
        town = -1
        if($("#select-city [value='-1']").attr("selected", "selected")){
            $('#select-city').selectize();
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
        }
        setHashValue('city','-1');
        date1 = min_date_real_date2;
        $("#date1").val(date1);
        setHashValue('date1',date1);
        date2 = max_date_real_date2;
        $("#date2").val(date2);
        setHashValue('date2',date2);
        
        item_arr=item.split('_');
        cat_arr = cat.split(',');
        $("input.search_category_check:checkbox").removeAttr("checked");
        setHashValue('cat','');

        age1 = 0;
        $("#minAge").val(age1)
        $("#slider_age").slider("values",0,age1);
        setHashValue('age1',age1);
        age2 = 100;
        $("#maxAge").val(age2)
        $("#slider_age").slider("values",1,age2);
        setHashValue('age2',age2);
        price1 = 0;
        $("#minCost").val(price1)
        $("#slider_cost").slider("values",0,price1);
        setHashValue('price1',price1);
        price2 = max_price_real;
        $("#maxCost").val(price2)
        $("#slider_cost").slider("values",1,price2);
        setHashValue('price2',price2);
        
        reviewSearchNewParams('hash', view, false, false);
    }
    
    
    
    
    
    if(item=='text'){
        text='';
        $("#form_search_field_name").val('');
        $(".main_search_input").val('');
        setHashValue('text','');
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='quick_place'){
        $("#quick_place").val('undefined');
        quick_place='undefined';
        setHashValue('quick_place','undefined')

            $('#select-country').selectize();
            var $select = $('#select-country').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);

        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='country'){
        country = -1
        if($("#select-country [value='-1']").attr("selected", "selected")){
            $('#select-country').selectize();
            var $select = $('#select-country').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
        }
        setHashValue('country','-1');
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='town'){
        town = -1
        if($("#select-city [value='-1']").attr("selected", "selected")){
            $('#select-city').selectize();
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
            control.setValue([-1, '(Не выбрано)']);
        }
        setHashValue('city','-1');
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='date1'){
        date1 = min_date_real_date2;
        $("#date1").val(date1);
        setHashValue('date1',date1);
        $("#date1_input").val(date1);
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='date2'){
        date2 = max_date_real_date2;
        $("#date2").val(date2);
        setHashValue('date2',date2);
        $("#date2_input").val(date2);
        reviewSearchNewParams('hash', view, false, false);
    }
    item_arr=item.split('_');
    if(item_arr.length==2 && item_arr[0]=='cat'){
        cat_arr = cat.split(',');
        cat_new = []
        for(o=0; o<cat_arr.length; o++){
            if(cat_arr[o]!=item_arr[1]){
                cat_new.push(cat_arr[o])
            }
        }
        $("input.search_category_check:checkbox").removeAttr("checked");
        for(var k=0; k<cat_new.length; k++){
            $("#chbox_"+cat_new[k]).prop("checked","checked")
        }
        cat = cat_new.join(',')
        setHashValue('cat',cat);
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='age1'){
        age1 = 0;
        $("#minAge").val(age1)
        slider_age.noUiSlider.set([age1, null]);
        setHashValue('age1',age1);
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='age2'){
        age2 = 100;
        $("#maxAge").val(age2)
        slider_age.noUiSlider.set([null, age2]);
        setHashValue('age2',age2);
        reviewSearchNewParams('hash', view, false, false);
    }

    if(item=='price1'){
        price1 = 0;
        $("#minCost").val(price1)
        slider_price.noUiSlider.set([price1, null]);
        setHashValue('price1',price1);
        reviewSearchNewParams('hash', view, false, false);
    }
    if(item=='price2'){
        price2 = max_price_real;
        $("#maxCost").val(price2)
        slider_price.noUiSlider.set([null, price2]);
        setHashValue('price2',price2);
        reviewSearchNewParams('hash', view, false, false);
    }

    if(item=='zaoch'){
        zaoch = 0;
        $("#chbox_zaoch").removeAttr('checked')
        $("#zaoch").val(0);
        setHashValue('zaoch',zaoch);
        reviewSearchNewParams('hash', view, false, false);
    }
}

function startLoader(){
    $('#popup_bg, #loader').css({display: "block"});
    $("#popup_bg, #loader").animate({opacity: "0.2"}, 100);
}

function stopLoader(){
    $('#popup_bg, #loader').css({display: "none"});
}