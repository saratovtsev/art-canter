$("#popup_bg").click(function(){
    stop_question();
    stop_login();
    stop_mail();
    stop_help();
    stop_reg();
    stop_rec();
    stop_order_error();
    stop_recall();
    stop_sendme();
    hideMapFest();
    stop_is_parent_event();
});

$("#popup_vopros_close").click(function(){stop_question();});
$("#popup_login_close").click(function(){stop_login();});
$("#popup_reg_close").click(function(){stop_reg();});
$("#popup_reс_close").click(function(){stop_rec();});
$("#popup_reсall_close").click(function(){stop_recall();});
$("#popup_sendme_close").click(function(){stop_sendme();});

function hidePopupBg() {
  $('#popup_bg').css('display', 'none');
}
