if (matchMedia) {
  var mq = window.matchMedia("(max-width: 768px)");
  mq.addListener(WidthChange);
  var mob = window.matchMedia("(max-width: 560px)");
  mob.addListener(WidthChange);
  WidthChange(mq, mob);
}

function mobileCss() {
    $('#sticky-wrapper').insertBefore('#header_right');
    $('<div class="header__help"></div>').insertAfter('#header_right');
    $('.i_need_help').appendTo('.header__help');
    $('.menu-fest__item').each(function () {
      parentW = +($('.menu-fest__item').parents('.menu-fest').css('width')).replace('px', '')
        - +($('.menu-fest__item').parents('.menu-fest').css('padding-left')).replace('px', '')
        - +($('.menu-fest__item').parents('.menu-fest').css('padding-right')).replace('px', '');
      if (!$(this).hasClass('_super')) {
        $(this).css('width', ((parentW-15)/2)+'px');
      }
      else {
        $(this).css('width', ((parentW-25)/3)+'px');
      }
    })
    $('.main_page_big_renab_info_name').each(function () {
      if($(this).text().length > 35) {
        $(this).text($(this).text().substring(0, 32) + '...')
      }
    })
    $('.main_page_big_renab_info_place').each(function () {
      if($(this).text().length > 115) {
        $(this).text($(this).text().substring(0, 112) + '...')
      }
    })
    $('.checkbox-block label').each(function () {
      parentW = +($(this).parents('.checkbox-block').css('width')).replace('px', '');
      $(this).css('width', parentW/3 -10 + 'px');
    })
    $('#countries_slider_link').on('click', function () {
      if ($('#countries_block').is(':visible')) $('#countries_block').hide(400);
    })
    $('._primary-toggle').on('click', function () {
      $(this).parent().toggleClass('open');
    })
}

function phoneCss() {
  mobileCss();
  $('.checkbox-block label').each(function () {
    parentW = +($(this).parents('.checkbox-block').css('width')).replace('px', '');
    $(this).css('width', parentW/2 -10 + 'px');
  })
}

function WidthChange() {
  if (mq.matches) {
    window.onload = mobileCss;
  }
  if (mob.matches) {
    window.onload = phoneCss;
  }
  else {

  }
}
