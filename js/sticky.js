$(document).ready(function(){
    if($('.header_search').length){
        $('.header_search')
            .sticky({topSpacing:0})
            .on('sticky-start', function() {
                if($('#is_search_results').length){
                    $('.main_menu_search_content_ext').css({'display': 'none'});
                }
                $('.header_search').addClass('fixed_header_search_fixed');
                $('.header_search').addClass('with_shadow_bottom');

            })
            .on('sticky-end', function() {
                $('.main_menu_search_content_ext').css({'display': 'block'});
                $('.header_search').removeClass('fixed_header_search_fixed');
                $('.header_search').removeClass('with_shadow_bottom');
            });
    }


        $('.order_filter')
            .sticky({topSpacing:$('#is_search_results').length?65:90})
            .on('sticky-start', function() {
                $('.order_filter').addClass('order_filter_fixed');
                $('.order_filter').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $('.header_search').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
            })
            .on('sticky-end', function() {
                $('.order_filter').removeClass('order_filter_fixed')
                $('.header_search').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $('.order_filter').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
            });




    if($('.fest_content_fixed_menu').length){
        $('.fest_content_fixed_menu')
            .sticky({topSpacing:90})
            .on('sticky-start', function() {
                $('.header_search').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
                $(".link_to_order_on_fixed").css({"visibility": "visible"});
                $('.fest_content_fixed_menu').addClass('fixed_menu_fixed').css({"width": "1212"});
                $('.fest_content_fixed_menu').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $(window).resize(function(){
                    $('.fest_content_fixed_menu').css({"width": "1212"});
                });

            })
            .on('sticky-end', function() {
                $('.header_search').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $(".link_to_order_on_fixed").css({"visibility": "hidden"});
                $('.fest_content_fixed_menu').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
            });
    }

});

function scrollToFormAndShow(){
    showed_form_search = true;
    // если форма скрыта - показать
    if($("input").is("#is_page_event_list") || $("input").is("#is_search_results")){
        if(!form_mini_show){
            show_hide_form_search_mini();
        }
    }
    else{
        if($("#form_search").is(":visible")){
        }
        else{
            $("#form_search").css({
                'display' : 'block'
            });
            showed_form_search = false;
        }

        if(!showed_form_search){
            showHideFormSearch();
        }
    }


    if($("#page_main").length>0 || $("#page_fest").length>0){
        top_form_search= $("#form_search").offset();
        $('body,html').animate({scrollTop:top_form_search.top-90},400);
    }
    else{

        $('body,html').animate({scrollTop:$("#form_search_mini_id").offset().top-190},400);
        //$('body,html').animate({scrollTop:top_fixed_header_search.top+50},400);
    }






    $('#form_search').queue(function (next){
        /*
        top_fixed = $("#fest_content .fest_content_fixed_menu").offset();
        top_fixed_warning = $("#before_order_filter").offset();
        */
        next();
    });
}
