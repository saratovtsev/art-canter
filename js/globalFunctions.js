function setHashValue(field, val){
    var myHash = location.hash;
    if(myHash.length>0){
        myHash=myHash.substr(1,(myHash.length)-1);
    }
    myHash = myHash.split('&');
    for(var i=0; i<myHash.length; i++){
        myHash_item = myHash[i].split('=');
        if(myHash_item[0]==field){
            myHash[i]=field+'='+val
        }
    }
    myHash = myHash.join('&')
    location.hash=myHash;
    return true;
}
