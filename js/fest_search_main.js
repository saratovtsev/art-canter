var text_search;

var order_by = 'relev';
var order_mode = 'ASC';

var filter_show;        // all, go, close
var count_all = -1;
var count_showed;
var array_events=[];
var the_end;

$(document).ready(function(){
    if($("#is_main_search").length>0){
        text_search = $("#text_search_main").val();
        var myHash = location.hash;
        if(myHash.length>0){
            //взять переменные из location.hash
            myHash=myHash.substr(1,myHash.length-1);
            if(myHash.length>0){
                myHash = myHash.split('&');
                // сортировка по полученный параметрам
                for(var i=0; i<myHash.length; i++){
                    hash_item = myHash[i].split('=');
                    if(hash_item[0]=='text'){
                        //text_search = hash_item[1]
                        text_search = decodeURIComponent(hash_item[1])
                        $("#main_search_input").val(text_search);
                        document.title = 'Результаты поиска по фразе "'+text_search+'"';

                    }
                    if(hash_item[0]=='order_by') order_by = hash_item[1]
                    if(hash_item[0]=='order_mode') order_mode = hash_item[1]
                    if(hash_item[0]=='filter_show') filter_show = hash_item[1]

                }

                if(typeof(order_by)=='undefined')order_by='relev';
                if(typeof(order_mode)=='undefined')order_mode='ASC';
                if(typeof(filter_show)=='undefined')filter_show='go';
                
                if(filter_show=='go'){
                    console.log('select GO');
                    $(".go").prop("checked", "checked");
                }
                if(filter_show=='close'){
                    $(".close").prop("checked", "checked");
                }
                if(filter_show=='all'){
                    $(".all").prop("checked", "checked");
                }
                
                
                
                
                
                if(order_by=='relev'){
                    $(".order_filter_inner_date").removeClass("asc").removeClass("desc").addClass("disable");
                }
                else if(order_by=='date'){
                    $("#order_filter_inner_relev").removeClass("asc").removeClass("desc").addClass("disable");
                }
                
                reviewMainSearchNewParams(order_by, false)
            }

            /*
            console.log(text)
            console.log(order_by_date)
            console.log(filter_show)
            console.log(count_showed)
            */

        }
        else{
            //console.log(myHash);
            text_search = $("#text_search_main").val();
            
            order_by = 'relev';
            order_mode = 'ASC';
            filter_show = 'go';
            
            if(filter_show=='go'){
                $("#go").prop("checked", "checked");
            }
            
            location.hash = 'text='+text_search+'&order_by='+order_by+'&filter_show='+filter_show;
            reviewMainSearchNewParams('default_search', false);
        }

        $("#form_filter_main_search_time").on("click", ".time_filter_main_search", function (event) {
            filter_show = event.target.id
            reviewMainSearchNewParams('filter', false);
        });
    }
});

function reviewMainSearchNewParams(mode, change_order){
    
    startLoader();
    
    //console.log('reviewListNewParams');
    first = 0;
    if(mode=='date'){
        if(order_by=='date'){
            if(change_order){
                if(order_mode=='ASC'){
                    order_mode = 'DESC';
                    $(".order_filter_inner_date").removeClass("disable").removeClass("asc").addClass("desc");
                    setHashValue('order_by', 'date');
                    setHashValue('order_mode', 'DESC');
                }
                else{
                    order_mode = 'ASC';
                    $(".order_filter_inner_date").removeClass("disable").removeClass("desc").addClass("asc");
                    setHashValue('order_by', 'date');
                    setHashValue('order_mode', 'ASC');
                }    
            }
        }
        else{
            order_by = 'date';
            order_mode = 'ASC';
            
            $("#order_filter_inner_relev").removeClass("desc").removeClass("asc").addClass("disable");
            $(".order_filter_inner_date").removeClass("disable").removeClass("desc").addClass("asc");
            
            setHashValue('order_by', 'date');
            setHashValue('order_mode', 'ASC');
        }
    }
    else if(mode=='filter'){
        setHashValue('filter_show', filter_show);
        
        
    }
    else if(mode=='relev'){
        if(order_by=='relev'){
            if(change_order){
                if(order_mode=='ASC'){
                    order_mode = 'DESC';
                    $("#order_filter_inner_relev").removeClass("disable").removeClass("asc").addClass("desc");
                    setHashValue('order_by', 'relev');
                    setHashValue('order_mode', 'DESC');
                }
                else{
                    order_mode = 'ASC';
                    $("#order_filter_inner_relev").removeClass("disable").removeClass("desc").addClass("asc");
                    setHashValue('order_by', 'relev');
                    setHashValue('order_mode', 'ASC');
                }
            }
        }
        else{
            order_by = 'relev';
            order_mode = 'ASC';
            
            
            $("#order_filter_inner_relev").removeClass("disable").removeClass("desc").addClass("asc");
            $(".order_filter_inner_date").removeClass("desc").removeClass("asc").addClass("disable");
            
            setHashValue('order_by', 'relev');
            setHashValue('order_mode', 'ASC');
        }
    }
    else if(mode=='default_search'){
        $("#order_filter_inner_relev").removeClass("disable").addClass("asc");
        $(".order_filter_inner_date").removeClass("asc").removeClass("desc").addClass("disable");
        
        if(filter_show=='all'){
            $("input#all[name=time_filter]:radio").attr('checked', 'checked');
        }
        else if(filter_show=='close'){
            $("input#close[name=time_filter]:radio").attr('checked', 'checked');
        }
        
        first = 1;
        
    }

    count_showed = 0;

    location.hash = 'text='+text_search+'&order_by='+order_by+'&order_mode='+order_mode+'&filter_show='+filter_show;
    
    $("#fraza").html('по фразе <span style="text-decoration: underline"> '+text_search+'</span>');
    
    array_events=[];
    $('#list_fest').html('');
    //console.log("Показано "+count_showed)
    $("#show_more_results").css("display", "none");
    $("#block_not_found").remove();
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: 'mode=get_main_search_new_10&text='+text_search+'&order_by='+order_by+'&order_mode='+order_mode+'&filter_show='+filter_show+"&count_showed="+count_showed+"&first="+first,
        success: function(jsondata){
            if(jsondata.status==1){
                //console.log(jsondata.res);
                if(jsondata.count>0){
                    
                    if(jsondata.research=='1'){
                        $("#all").prop("checked", "checked");
                        setHashValue('filter_show','all');
                    }
                    
                    $('#list_fest').html(jsondata.res);
                    count_showed += parseInt(jsondata.count);
                    if(parseInt(jsondata.count)<10){
                        $("#show_more_results").css("display", "none");
                        the_end=true;
                    }
                    else{
                        $("#show_more_results").css("display", "block");
                        the_end=false;
                        getMainSearch50(0)
                    }
                }
                else{
                    $('#list_fest').html('<br><div id="block_not_found">Результатов по данному запросу не найдено. <br>Попробуйте задать другие ключевые слова</div>');
                }
            }
            else{
                $('#list_fest').html('<br><div id="block_not_found">Результатов по данному запросу не найдено. <br>Попробуйте задать другие ключевые слова</div>');
                $("#show_more_results").css("display", "none");
            }
            order_filter_inner_date = $(".order_filter_inner_date").offset();
            
            //$('body,html').animate({scrollTop:order_filter_inner_date.top},800);
            stopLoader();
        },
        complete: function(){
            //alert(jsondata.status);
            $("#show_more_results").html('Показать еще 10');
            reCallTooltip();
            //$("a").LiteTooltip({ backcolor: "#ffffff", textcolor: "#000000", opacity: 1, padding: 20});
            $(".fest_list_item_content_name").dotdotdot();
        },
        async: true,
        dataType: 'json'
    });
    reviewMainSearchGlobal();
}


function reviewMainSearchGlobal(){
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: 'mode=get_main_search_global&text='+text,
        success: function(jsondata){
            if(jsondata.status==1){
                //console.log(jsondata.res);
                $('#list_results').html(jsondata.res);
            }
        },
        async: true,
        dataType: 'json'
    });
    
}

function getMainSearch50(count_showed){
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: 'mode=get_main_search_50&text='+text_search+'&order_by='+order_by+'&order_mode='+order_mode+'&filter_show='+filter_show+'&count_showed='+count_showed,
        success: function(jsondata){
            if(jsondata.status==1){
                for(var i=0; i<jsondata.res.length; i++){
                    array_events.push(jsondata.res[i])
                }
            }
        },
        complete: function(){
            //alert(jsondata.status);
        },
        async: false,
        dataType: 'json'
    });
}

function showMoreEventsMainSearch(){
    //console.log('showMoreEventsList');
    
    //console.log("Показано "+count_showed)
    var tmp_arr=[];
    for(var i=count_showed; i<count_showed+10; i++){
        if(typeof array_events[i] != "undefined"){
            tmp_arr.push(array_events[i])
        }
        else{
            the_end = true;
        }
        
    }
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: "mode=get_arr_events&arr="+tmp_arr,
        beforeSend: function(){
            $("#show_more_main_results").html('');
        },
        success: function(jsondata){
            if(jsondata.status==1){
                //console.log(jsondata.res);
                $('#list_fest').append(jsondata.res);
                count_showed += parseInt(jsondata.count);
                if(count_showed%50==0 && !the_end){
                    getMainSearch50(count_showed)
                }

                //console.log(showed_key);
                if(parseInt(jsondata.count)<10){
                    $("#show_more_results").css("display", "none");
                }
            }
            else{
                $('#list_fest').append('<br><div id="block_not_found">Результатов по данному запросу больше не найдено. <br>Попробуйте задать другие ключевые слова</div>');
                $("#show_more_results").css("display", "none");
            }
        },
        complete: function(){
            //alert(jsondata.status);
            $("#show_more_results").html('Показать еще 10');
            reCallTooltip();
            //$("a").LiteTooltip({ backcolor: "#ffffff", textcolor: "#000000", opacity: 1, padding: 20});
            $(".fest_list_item_content_name").dotdotdot();
        },
        async: true,
        dataType: 'json'
    });
}
