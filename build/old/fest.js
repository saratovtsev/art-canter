// блок похожих мероприятий
var can_similar_slide_next = true;
var can_similar_slide_prev = false;
var count_similar;
var focus_similar;

var showed_form_search = true;
var showed_why_we_are = true;

// позиция переключателя форма/карта
var position_left = true;
// клик по переключателю форма/карта
var switch_click = false;
// Начато движение переключателя форма/карта
var switch_move = false;


$(document).ready(function(){
    // если есть "Похожие.."
    if($("div").is(".similar_events_slider_item")){

        count_similar = $(".similar_events_slider_item").length;

        $(".similar_events_slider_item:first").css("display", "block");
        focus_similar = 0;

        // пронумеровать
        var rrr=0;
        $(".similar_events_slider_item").each(function(){
            $(this).attr("id", "similar_events_slider_item_"+rrr);
            //alert(this.id);
            rrr++;
        })




        $("#similar_btn_prev").removeClass("slider_button_left");
        $("#similar_btn_prev").addClass("slider_button_left_forbidden");
    }

    // если есть "Популярные.."
    if($("div").is(".main_page_popular_events_content_slide")){
        count_populars = $("#count_populars").val();
        $("#main_page_popular_events_content_slide_0").css("display", "block");
        focus_popular = 0;
        $("#popular_btn_prev").removeClass("popular_prev");
        $("#popular_btn_prev").addClass("popular_prev_forbidden");

        if(count_populars==1){
            $("#popular_btn_next").removeClass("popular_next");
            $("#popular_btn_next").addClass("popular_next_forbidden");
        }


    }
    is_search = false;
    // Это страница с результатами поиска
    if($("input").is("#is_search_results")){
        is_search = true;

        // форма поиска НЕвидимая
        $("#form_search").css({'display' : 'block'});
        // ее можно скрыть
        $("#form_search_close").css("display", "block");
        result_search_head_top = $("#result_search_head").offset();
    }

    if($("div").is(".form_search_content_item_city")){
        if(!is_search){
            $('#select-country').selectize();
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;

            $('#select-city').selectize();
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
        }
    }

    $('[name="cat2[]"]').change(function(){
        startSearchByCountryTown()
    });
})

var form_height;

function showHideFormSearch(){
    if(page_main || is_search || page_event_list){
        if(showed_form_search){
            form_height = $("#form_search").height();
            if(showedForm){mini_height = 60}

            $("#form_search").css("overflow", "hidden");
            $("#form_search").animate({
                height: mini_height
            });

            $('#form_search').queue(function (next){
                top_fixed_warning = $("#order_filter").offset();
                next();
            });

            /*
            $(".form_search_content_type_content").animate({
                top: "-100px"
            });
            */
            showed_form_search = false;
            $("#form_search_header_title_bul").removeClass("form_search_header_title_bul");
            $("#form_search_header_title_bul").addClass("form_search_header_title_bul_hidden");


        }
        else{
            $("#form_search").css("overflow", "visible");
            $("#form_search").animate({
                height: 731
            });

            $('#form_search').queue(function (next){
                top_fixed_warning = $("#order_filter").offset();
                next();
            });

            //alert(height)
            /*
            $(".form_search_content_type_content").animate({
                top: "0px"
            });
            */
            showed_form_search = true;
            $("#form_search_header_title_bul").removeClass("form_search_header_title_bul_hidden");
            $("#form_search_header_title_bul").addClass("form_search_header_title_bul");


            // Если главная страница то поднять форму
            if(page_main){
                start_form_y = $("#form_search").offset();
                start_form_y = start_form_y.top
                $('body,html').animate({scrollTop:start_form_y},800);
            }
        }
    }
}

var showedForm = true;
var showedMap = false;

var result_has_been_showed_list = result_has_been_showed_map = false;

var form_mini_show = true;
function show_hide_form_search_mini(){
    if(form_mini_show){
        form_mini_show = !form_mini_show;
        // сворачивание

        $('#form_search_mini').css('left',  '-311px');
        $('#form_search_mini_id').css('width', '25px');

        $('#show_hide').removeClass('show_hide_nocollapse').addClass('show_hide_collapse');

        $('#list_fest').removeClass('list_fest_mini').addClass('list_fest_normal');
        $('.fest_list_item_content_name').hide();
        $('.fest_list_item_content_name_normal').show();
        if(view!='map'){
            $('#list_fest_header').removeClass('list_fest_header_mini').addClass('list_fest_header_normal');
            $('#show_more_results').removeClass('show_more_results_mini').addClass('show_more_results_normal');

        }

    }
    else{
        form_mini_show = !form_mini_show;
        // разворачивание

        $('#form_search_mini_id').css('width', '365px');

        $('#show_hide').removeClass('show_hide_collapse').addClass('show_hide_nocollapse');

        $('#list_fest').removeClass('list_fest_normal').addClass('list_fest_mini');
        $('.fest_list_item_content_name').show();
        $('.fest_list_item_content_name_normal').hide();

        if(view!='map'){
            $('#form_search_mini').css('left', '27px');
            $('#list_fest_header').removeClass('list_fest_header_normal').addClass('list_fest_header_mini');
            $('#show_more_results').removeClass('show_more_results_normal').addClass('show_more_results_mini');
            $(".fest_list_item_content_name").dotdotdot();
            $('.fest_list_item_content_name .dotdotdot').each(function(){
                var tmp_fav = $(this).children("img");
                var tmp_link = $(this).children("a");
                $(this).html(tmp_link[0]['outerHTML'] + tmp_fav[0]['outerHTML']);
            });
        }
        else{
            $('#form_search_mini').css('left', '0px');
        }
    }
}

function switcherListMap(){
    console.log("switch! now: ", view);
    if(view=='map'){
        view='list';
        $('.list_map_switch #list').hide();
        $('.list_map_switch #map').show();
        //show_hide_form_search_mini();

        $('#form_search_mini').css('top', '0px');

        $('#map_search').css({"display": "none"});
        $('#list_fest, #list_results').css({"display": "block"});
        if(!the_end && !globalNotFound){
            $("#show_more_results").css({"display": "block"}).removeClass('show_more_results_mini').addClass('show_more_results_normal');
        }
        setHashValue('view', view);

        if(!result_has_been_showed_list){
            reviewSearchNewParams('hash', view, false, true);
            result_has_been_showed_list = true;
        }
        else{
            $(".fest_list_item_content_name").dotdotdot();
        }

        $('#form_search_mini_id').css({
            'position': 'relative',
            'float': 'left'
        });

    }
    else{
        view='map';
        $('.list_map_switch #list').show();
        $('.list_map_switch #map').hide();
        $("#show_more_results").hide();

        $('#form_search_mini').css('top', '91px');
        $('#form_search_mini_id').css('height', '950px');

        $('#list_fest_header').removeClass('list_fest_header_mini').addClass('list_fest_header_normal');

        if(!form_mini_show) show_hide_form_search_mini();

        if(!globalNotFound){
            $('#map_search').css({"display": "block"});
            $('#list_fest, #list_results').css({"display": "none"});

            setHashValue('view', view);

            if(!result_has_been_showed_map){
                reviewSearchNewParams('hash', view, true, true)
                result_has_been_showed_map = true;
            }
            else{
                if(form_mini_show) setTimeout("show_hide_form_search_mini()", 1000);
            }
        }

        $('#form_search_mini_id').css({
            'position': 'absolute'
        });

    }
    return true;
}


function quickCalend(num){
    //alert(num)
    now = new Date();
    now_month = now.getMonth()

    switch(num){
        case -4:
            //$("#date1").val('1 декабря');
            //$("#date2").val('28 февраля');
            if(now_month<=11 && now_month>1){
                date1_calendar = now.getFullYear()+'-12-01'
                date2_calendar = (now.getFullYear()+1)+'-03-01'
            }
            else if(now_month<2){
                date1_calendar = (now.getFullYear()-1)+'-12-01'
                date2_calendar = now.getFullYear()+'-03-01'
            }
            break;
        case -3:
            //$("#date1").val('1 марта');
            //$("#date2").val('31 мая');

            if(now_month<=4){
                date1_calendar = now.getFullYear()+'-03-01'
                date2_calendar = now.getFullYear()+'-06-01'
            }
            else{
                date1_calendar = (now.getFullYear()+1)+'-03-01'
                date2_calendar = (now.getFullYear()+1)+'-06-01'
            }
            break;
        case -2:
            //$("#date1").val('1 июня');
            //$("#date2").val('31 августа');

            if(now_month<=7){
                date1_calendar = now.getFullYear()+'-06-01'
                date2_calendar = now.getFullYear()+'-09-01'
            }
            else{
                date1_calendar = (now.getFullYear()+1)+'-06-01'
                date2_calendar = (now.getFullYear()+1)+'-09-01'
            }
            break;
        case -1:
            //$("#date1").val('1 сентября');
            //$("#date2").val('30 ноября');

            if(now_month<=10){
                date1_calendar = now.getFullYear()+'-09-01'
                date2_calendar = now.getFullYear()+'-12-01'
            }
            else{
                date1_calendar = (now.getFullYear()+1)+'-09-01'
                date2_calendar = (now.getFullYear()+1)+'-12-01'
            }
            break;
    }

    /*
    var k = new Kalendae(document.getElementById('kalendae_3th'),{
        months:3,
        mode:'range',
        selected:[date1_calendar, date2_calendar],
        weekStart: 1
    });
    */
    //console.log(date1_calendar+','+date2_calendar)

    k.setSelected(date1_calendar+','+date2_calendar)

}

function quickPlace(num, form){
    console.log('quickPlace');
    if(num==-7){
      console.log('RUS');
        $('#select-country').selectize();
        var $select = $('#select-country').selectize();
        var control = $select[0].selectize;
        console.log(control);
        control.setValue([46, "Россия"]);
    }
    else{
        var text_place;
        switch(num){
            case -2:
                text_place = 'Европа';
                break;
            case -3:
                text_place = 'Азия';
                break;
            case -4:
                text_place = 'Америка';
                break;
            case -5:
                text_place = 'Африка';
                break;
            case -6:
                text_place = 'Австралия';
            case -7:
                text_place = 'Россия';
                break;
        }
        $("#quick_place").val(num);
        $('#select-country').selectize();
        var $select = $('#select-country').selectize();
        var control = $select[0].selectize;

        control.addOption({
            value: num,
            text: text_place
        });
        control.setValue([num, text_place]);


        // подгрузка городов по части света
        $('#select-country').selectize();
        var $select = $('#select-city').selectize();
        var control = $select[0].selectize;
        control.clearOptions();

        $.ajax({
            url: '/admin/modules/ajax/country.php',
            type: "POST",
            data: "mode=get_towns_by_part_of_world&part="+num,
            success: function(jsondata){
                /*
                $('select[name="town[]"] option').remove();
                $('select[name="town[]"]').append('<option value="-1">(Не выбран)</option>');
                */
                for(var i=0; i<jsondata.id_dog_res.length; i++){
                    control.addOption({
                        value: i+'_'+jsondata.id_dog_res[i]['id'],
                        text: jsondata.id_dog_res[i]['name']
                    });
                    //alert(jsondata.id_dog_res[i]['name']);
                    //$('select[name="town[]"]').append('<option value="'+jsondata.id_dog_res[i]['id']+'">'+jsondata.id_dog_res[i]['name']+'</option>');
                }
            },
            complete: function(){
                //alert(jsondata.status);
            },
            async: false,
            dataType: 'json'
        });



    }


    //$("#my_select [value='2']").attr("selected", "selected");
}



function AddCity(){
    var control = $select[0].selectize;
    control.addOption({
        value: 4,
        text: 'Something New'
    });
    alert(1)
}

// Выбор интервалов

$(document).ready(function(){
    // Слайдер возраста

    $("#slider_age").slider({
        min: 0,
        max: 100,
        values: [0,100],
        range: true,
        stop: function(event, ui) {
            $("#minAge").val($("#slider_age").slider("values",0));
            $("#maxAge").val($("#slider_age").slider("values",1));
        },
        slide: function(event, ui){
            $("#minAge").val($("#slider_age").slider("values",0));
            $("#maxAge").val($("#slider_age").slider("values",1));
        }
    });

    $("input#minAge").change(function(){
        var value1=$("input#minAge").val();
        var value2=$("input#maxAge").val();
        if(parseInt(value1) > parseInt(value2)){
            value1 = value2;
            $("input#minAge").val(value1);
        }
        $("#slider_age").slider("values",0,value1);
    });


    $("input#maxAge").change(function(){
        var value1=$("input#minAge").val();
        var value2=$("input#maxAge").val();
        if (value2 > 100) {value2 = 1000; $("input#maxAge").val(100)}
        if(parseInt(value1) > parseInt(value2)){
            value2 = value1;
            $("input#maxAge").val(value2);
        }
        $("#slider_age").slider("values",1,value2);
    });

    var value1=$("input#minAge").val();
    var value2=$("input#maxAge").val();

    $("#slider_age").slider("values",0,value1);
    $("#slider_age").slider("values",1,value2);




    // Слайдер цен

    var min_price_real = parseInt($("#min_price_real").val());
    var max_price_real = parseInt($("#max_price_real").val());

    $("#slider_cost").slider({
        min: min_price_real,
        max: max_price_real,
        values: [0,100000],
        range: true,
        stop: function(event, ui) {
            $("input#minCost").val($("#slider_cost").slider("values",0));
            $("#min_price_div").html($("#slider_cost").slider("values",0));
            $("input#maxCost").val($("#slider_cost").slider("values",1));
            $("#max_price_div").html($("#slider_cost").slider("values",1));

        },
        slide: function(event, ui){
            $("input#minCost").val($("#slider_cost").slider("values",0));
            $("#min_price_div").html($("#slider_cost").slider("values",0));
            $("input#maxCost").val($("#slider_cost").slider("values",1));
            $("#max_price_div").html($("#slider_cost").slider("values",1));
        }
    });

    $("input#minCost").change(function(){
        var value1=$("input#minCost").val();
        var value2=$("input#maxCost").val();

        if(parseInt(value1) > parseInt(value2)){
            value1 = value2;
            $("input#minCost").val(value1);
        }
        $("#slider_cost").slider("values",0,value1);
    });

    $("input#maxCost").change(function(){

        var value1=$("input#minCost").val();
        var value2=$("input#maxCost").val();

        if(parseInt(value1) > parseInt(value2)){
            value2 = value1;
            $("input#maxCost").val(value2);
            $("#max_price_div").html(value1);
        }
        $("#slider_cost").slider("values",1,value2);
    });

    var value1=$("input#minCost").val();
    var value2=$("input#maxCost").val();

    $("#slider_cost").slider("values",0,value1);
    $("#slider_cost").slider("values",1,value2);

    $("#min_price_div").html($("#slider_cost").slider("values",0));
    $("#max_price_div").html($("#slider_cost").slider("values",1));


    // $('.ui-slider-handle::odd').css('backgroundImage', 'url(/d/i/slider_right.png)')















    // фильтрация ввода в поля
    /*
        jQuery('input').keypress(function(event){
            var key, keyChar;
            if(!event) var event = window.event;

            if (event.keyCode) key = event.keyCode;
            else if(event.which) key = event.which;

            if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
            keyChar=String.fromCharCode(key);

            if(!/\d/.test(keyChar))    return false;

        });
    */


    // Автоматическое удаление заявок (вкл/выкл)
    $("#auto_delete").change(function(){
        if($(this).attr("checked")){
            autoDelOrder(1);
        }else{
            autoDelOrder(0);
        }
    });
});

function autoDelOrder(autodel){
    $.ajax({
        url: '/modules/ajax/order-backend.php',
        type: "POST",
        data: "mode=auto_del_order&autodel="+autodel,
        success: function(jsondata){
            //alert(jsondata.status);
            if(jsondata.status == 1){
                $('#auto_del_msg').html('Успешно сохранено').css("color", "#07B807").show().fadeOut(3000);
            }
            else{
                $('#auto_del_msg').html('Ошибка сервера. Повторите попытку позднее').css("color", "#CC0000").show().fadeOut(3000);
            }
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}



function similar_slide_next(){
    if(can_similar_slide_next){
        $("#similar_events_slider_item_"+focus_similar).fadeOut();
        $("#similar_events_slider_item_"+(focus_similar+1)).fadeIn();
        focus_similar++;
        can_similar_slide_prev = true;
        $("#similar_btn_prev").removeClass("slider_button_left_forbidden");
        $("#similar_btn_prev").addClass("slider_button_left");
        if(focus_similar == (count_similar-1)){
            can_similar_slide_next = false;
            $("#similar_btn_next").removeClass("slider_button_right");
            $("#similar_btn_next").addClass("slider_button_right_forbidden");
        }
        else{
            can_similar_slide_next = true;
        }

    }
}

function similar_slide_prev(){
    if(can_similar_slide_prev){
        $("#similar_events_slider_item_"+focus_similar).fadeOut();
        $("#similar_events_slider_item_"+(focus_similar-1)).fadeIn();
        focus_similar--;
        can_similar_slide_next = true;
        $("#similar_btn_next").removeClass("slider_button_right_forbidden");
        $("#similar_btn_next").addClass("slider_button_right");
        if(focus_similar == 0){
            can_similar_slide_prev = false;
            $("#similar_btn_prev").removeClass("slider_button_left");
            $("#similar_btn_prev").addClass("slider_button_left_forbidden");
        }
        else{
            can_similar_slide_prev = true;
        }
    }
}

function showHidewhyWeAre(){
    if(showed_why_we_are){

        $(".why_we_are").css({"height":"0px", "padding":"0", "border-bottom":"0px solid #0ab82b"});
        $(".why_we_are_block").css({"height":"0px"});

        showed_why_we_are = false;
        $("#why_we_are_title_bul").removeClass("why_we_are_title_bul");
        $("#why_we_are_title_bul").addClass("why_we_are_title_bul_hidden");
    }
    else{

        $(".why_we_are").css({"height":"110px", "padding":"20px 0 60px 0", "border-bottom":"2px solid #0ab82b"});
        $(".why_we_are_block").css({"height":"200px"});


        showed_why_we_are = true;
        $("#why_we_are_title_bul").removeClass("why_we_are_title_bul_hidden");
        $("#why_we_are_title_bul").addClass("why_we_are_title_bul");
    }
}



function showMoreEvents(){
    var type_list_order_by = $('#type_list_order_by').val();
    var sql_select_price = $('#sql_select_price').val();


    var type_list = $('#type_list').val();
    var country_name = -1;
    if(type_list == 'country'){
        country_name = $('#type_list_country').val();
    }
    category_id = -1;
    if(type_list == 'category'){
        category_id = $('#type_list_category').val();
    }

    $.ajax({
        url: '/modules/ajax/search-backend.php',
        type: "POST",
        data: "mode=show_more_events&type_list_order_by="+type_list_order_by+"&count_showed="+showed_key+"&type_list="+type_list+"&country_name="+country_name+"&sql_select_price="+sql_select_price+"&category_id="+category_id+"&time_filter="+time_filter,
        success: function(jsondata){
            //alert(jsondata.status);
            if(jsondata.status == 1){
                $('#list_fest').append(jsondata.res);
                //console.log(jsondata.z_index_last);
                showed_key = parseInt(showed_key)+parseInt(jsondata.count);
                //console.log(showed_key);
                if(parseInt(jsondata.count)<10){
                    $("#show_more_results").css("display", "none");
                }
            }
            else{
                $('#list_fest').append('<br><div style="width: 100%; text-align: center; font-size: 16px;">Результатов по данному запросу больше не найдено</div>');
                $("#show_more_results").css("display", "none");

            }
            //alert(jsondata.status);
        },
        complete: function(){
            reCallTooltip();
            //$("a").LiteTooltip({ backcolor: "#ffffff", textcolor: "#000000", opacity: 1, padding: 20});
        },
        dataType: 'json'
    });
}

function addfav(id){
    $.ajax({
        url: '/modules/ajax/fest-backend.php',
        type: "POST",
        data: "mode=changefav&id="+id,
        success: function(jsondata){
            //alert(jsondata.status);
            if(jsondata.status == 1){
                if(jsondata.change == 'added'){
                    console.log(id)
                    $('.fav_btn_'+id).attr("src", "/d/i/fav2.png")
                }
                else{
                    $('.fav_btn_'+id).attr("src", "/d/i/fav.png")
                }
            }
            else{

            }
            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}

showed = false;
function showReviewAdding(n){
    if(n==1){
        $("#tab_review").click();
        showed = false;
    }
    if(showed){
        $("#btn_adding_review").removeClass("button_green_selected");
        $("#btn_adding_review").addClass("button_green");
        $("#review_form").css("display", "none");
        showed = false;
    }
    else{
        $("#btn_adding_review").removeClass("button_green");
        $("#btn_adding_review").addClass("button_green_selected");
        $("#review_form").css("display", "block");
        showed = true;
    }
}

function addReview(id){
    var error_message = '';
    var name = $('#review_new_name_input').val();
    if(name == ''){
        error_message += "Не заполнено имя<br>"
        var error = true;
    }

    var email = $('#review_new_email_input').val();
    if(email == ''){
        error_message += "Не заполнен email<br>";
        var error = true;
    }
    else{
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if(!pattern.test(email)){
            error_message += "Неверно заполнен email<br>";
            var error = true;
        }
    }

    var collect = $('#review_new_collect_input').val();
    if(collect == ''){
        error_message += "Не заполнено название коллектива<br>";
        var error = true;
    }

    var event_name = $('#review_new_event_input').val();

    var text = $('#review_new_text_input').val();
    if(text == ''){
        error_message += "Не заполнен текст отзыва<br>";
        var error = true;
    }

    if(!error){
        console.log('Скрыть кнопку');
        $("#review_form_errors").hide();
        $("#btn_adding_review_submit").hide();
        startLoader();
        $.ajax({
            url: '/modules/ajax/fest-backend.php',
            type: "POST",
            data: "mode=add_review&id="+id+"&name="+name+"&email="+email+"&collect="+collect+"&text="+text+"&event_name="+event_name,
            success: function(jsondata){
                //alert(jsondata.status);

                if(jsondata.status == 1){
                    $('#review_form').html('Отзыв успешно добавлен и будет опубликован после проверки администратором');
                    $("#btn_adding_review_submit").show();
                    stopLoader();
                }
                else{
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'json'
        });

    }
    else{
        $("#review_form_errors").show();
        $("#review_form_errors").html(error_message);
    }
}

function showChReviewAdding(id){
    if($("#ch_review_form_"+id).is(":visible")){
        $("#ch_review_form_"+id).css("display", "none");
    }
    else{
        $("#ch_review_form_"+id).css("display", "block");
    }
}

function addChReview(event_id, review_id){
    var name = $('#ch_review_form_'+review_id+' form .review_new_name .review_new_name_input').val();
    if(name == ''){
        alert("Не заполнено имя");
        var error = true;
    }
    var text = $('#ch_review_form_'+review_id+' form .review_new_text .review_new_text_input').val();
    if(text == ''){
        alert("Не заполнен текст комментария");
        var error = true;
    }
    if(!error){
        $("#btn_adding_review_child").hide();
        startLoader();
        $.ajax({
            url: '/modules/ajax/fest-backend.php',
            type: "POST",
            data: "mode=add_ch_review&event_id="+event_id+"&review_id="+review_id+"&name="+name+"&text="+text,
            success: function(jsondata){
                //alert(jsondata.status);

                if(jsondata.status == 1){
                    $('#ch_review_form_'+review_id).html('Комментарий к отзыву успешно добавлен и будет опубликован после проверки администратором');
                    stopLoader();
                }
                else{
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'json'
        });
    }
}

function hidePopupBg(){
    $("#popup_bg").animate({opacity: "0"}, 100);
    $('#popup_bg').queue(function (next){
        $('#popup_bg').css({display: "none"});
        next();
    });
}

var true_answer_captcha;
var id_fest_question;
function start_question(id,festp){
    id_fest_question = id;
    $("#num_fest_question").val(id);
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);

    $(".popup_window_inner_field").css("display", "block");
    $("#message_new_question").css("display", "none");
    $("#popup_vopros_submit").show();
    //$("#message_new_question").html('');
    height = $('#popup_vopros').height();
    width = $('#popup_vopros').width();
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    yaCounter17972227.reachGoal('question');
	if (typeof festp !== 'undefined') {
		yaCounter17972227.reachGoal('openquestionform');
	}

    $("#popup_vopros").animate({opacity: "1"}, 300);
    $('#popup_vopros').css({display: "block"});
    $('#popup_vopros').css("margin", "-"+height+"px 0 0 -"+width+"px");

    // получить новую капчу
    //reCaptcha();

    return false;
}

function reCaptcha(){
    $.ajax({
        url: '/modules/ajax/fest-backend.php',
        type: "POST",
        data: "mode=get_captcha",
        success: function(jsondata){
            true_answer_captcha = jsondata.new_captcha;
            $('.popup_window_inner_field_inputtext_captcha').attr("src","/write/tmp/captcha/"+true_answer_captcha+".jpg");
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}



function stop_question(){
    //$("#message_new_question").html('');
    $("#popup_vopros").animate({opacity: "0"}, 300);
    $('#popup_vopros').css({display: "none"});
    hidePopupBg();
    return false;
}

function getPrice(){
    var event_id = $("#fest_id").val();
    data_for_calc = '';
    var i=0;
    var tot_q=0;

    $(".members_count").each(function(){
        type_lodging = this.id.split('_');
        type_lodging = type_lodging[1];
        //console.log(type_lodging, this.value);
        count = (this.value=='')?'0':this.value

        data_for_calc += type_lodging+'_'+count;
        if(i != ($(".members_count").length-1)){
            data_for_calc += '-';
        }
        tot_q +=parseInt(count);
        //console.log(count);
        i++;
    });

    if (tot_q>0){
        $(".count_members_total").show();
    }
    else{
        $(".count_members_total").hide();
    }
    $("#tot_q").html(tot_q);



    $.ajax({
        url: '/modules/ajax/fest-backend.php',
        type: "POST",
        data: "mode=get_price&event="+event_id+"&lodging_and_count="+data_for_calc,
        success: function(jsondata){
            //alert(jsondata.status);

            if(jsondata.status == 1){
                //console.log("Общая стоимость: ", jsondata.price_total);
                $('#total_price').html('<b><span style="color: #0ab82b">ИТОГО:</span> '+jsondata.price_total+' руб</b><br><br>'+jsondata.price_html);
            }
            else{

            }

            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}


function recount(){
    if($("input").is(".members_count")){
        getPrice();
    }
}

$(document).ready(function(){

    // страница заявки
    if($('#page_order').length){
        $('.dir_spon').change(function(event) {
            if(this.value == 'spon'){
                $('.blagodarnost2').html('Спонсор');
                $('#template_blagodarnost').attr('href', '/d/thanks_sponsor.jpg');
            }
            else{
                $('.blagodarnost2').html('Директор');
                $('#template_blagodarnost').attr('href', '/d/thanks_director.jpg');
            }
        });

    }



    recount();
    // пересчет кол-ва участников

    $('.members_count').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            alert("Допустимы только цифры!")
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });

    // $(".members_count").keyup(function(e){
    //     console.log(e.which);
    //     var access_keys = [8, 9, 13, 27,  46,   48, 49, 50, 51, 52, 53, 54, 55, 56, 57,   96, 97, 98, 99, 100, 101, 102, 103, 104, 105 ]


    //     if(in_array(e.which, access_keys)){
    //         recount();
    //     }
    //     else{
    //         $(this).val($(this).val().substr(0, $(this).val().length-1))
    //         alert("Допустимы только цифры!")
    //     }

    // });
});

function retry_order(){
    $('#fest_order_content').removeClass('hidden').addClass('nohidden');
    $('#link_to_retry_order').hide();

    top_start_form_order= $("#fest_order_content_name").offset();
    $('body,html').animate({scrollTop:top_start_form_order.top},400);

}

$(document).ready(function(){
    if($('#order_error').length>0 && $('#order_error').val()==1){
        start_order_error();
        $('#popup_order_error_text_block').html($('#order_error_text').html());
    }
});





$(document).ready(function(){
    if($('#order_error').length>0 && $('#order_error').val()==1){
        start_order_error();
        $('#popup_order_error_text_block').html($('#order_error_text').html());
    }
});

function start_order_error(){
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);

    $(".popup_window_inner_field").css("display", "block");
    $("#message_mail").css("display", "none");

    var height = $('#popup_order_error').height();
    var width = $('#popup_order_error').width();
    var width = 420;

    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;


    $("#popup_order_error").animate({opacity: "1"}, 300);
    $('#popup_order_error').css({display: "block"});
    //$('#popup_order_error').css("margin", "-"+height+"px 0 0 -"+width+"px");
    $('#popup_order_error').css("margin", "-300px 0 0 -"+width+"px");
    return false;
}

function stop_order_error(){
    $("#popup_order_error").animate({opacity: "0"}, 300);
    $('#popup_order_error').css({display: "none"});
    hidePopupBg();
    return false;
}






$(document).ready(function(){
    if($('#is_parent_event').length>0){
        start_is_parent_event();
        //$('#popup_order_error_text_block').html($('#order_error_text').html());
    }
});

function start_is_parent_event(){
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);

    $(".popup_window_inner_field").css("display", "block");
    new_event = $('#is_parent_event').val().split('{}');
    $('#link_to_actual_event').html('<a href="/events/'+new_event[0]+'">'+new_event[1]+'</a>');


    var height = $('#popup_is_parent_event').height();
    var width = $('#popup_is_parent_event').width();
    var width = 420;

    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;


    $("#popup_is_parent_event").animate({opacity: "1"}, 300);
    $('#popup_is_parent_event').css({display: "block"});
    $('#popup_is_parent_event').css("margin", "-150px 0 0 -"+width+"px");
    return false;
}

function stop_is_parent_event(){
    $("#popup_is_parent_event").animate({opacity: "0"}, 300);
    $('#popup_is_parent_event').css({display: "none"});
    hidePopupBg();
    return false;
}






var hidden_parents_reviews = true;

function show_hide_parents_reviews(){
    if(hidden_parents_reviews){
        $('.parent_hidden').css("display", "block");
        $('#link_parents_reviews').text("Свернуть отзывы прошлых лет");
        hidden_parents_reviews = false;
    }
    else{
        hidden_parents_reviews = true;
        $('.parent_hidden').css("display", "none");
        $('#link_parents_reviews').text("Посмотреть отзывы прошлых лет об этом фестивале");
    }
}

$(document).ready(function() {
    if($('#no_reviews').length){
        show_hide_parents_reviews();
    }
})


var hidden_parents_photos = true;

function show_hide_parents_photos(){
    if(hidden_parents_photos){
        $('#parent_photos').css("display", "block");
        $('#link_parents_photos').text("Свернуть фото прошлых лет этого фестиваля");
        hidden_parents_photos = false;
    }
    else{
        hidden_parents_photos = true;
        $('#parent_photos').css("display", "none");
        $('#link_parents_photos').text("Посмотреть фото прошлых лет этого фестиваля");
    }
}



function getTrueDate(str){
    day = str.substr(3, 2);
    month = str.substr(0, 2);
    year = str.substr(6, 4);

    switch(month){
      case '01': month2 = ' января '; break;
      case '02': month2 = ' февраля '; break;
      case '03': month2 = ' марта '; break;
      case '04': month2 = ' апреля '; break;
      case '05': month2 = ' мая '; break;
      case '06': month2 = ' июня '; break;
      case '07': month2 = ' июля '; break;
      case '08': month2 = ' августа '; break;
      case '09': month2 = ' сентября '; break;
      case '10': month2 = ' октября '; break;
      case '11': month2 = ' ноября '; break;
      case '12': month2 = ' декабря '; break;
    }
    return day + month2 + year;
}

function getTrueDateToSearch(str){
    day = str.substr(3, 2);
    month = str.substr(0, 2);
    year = str.substr(6, 4);

    return year +'-'+ month +'-'+ day;
}

function show_vk_block(){
    $('.fb-page').hide();
    $('#ok_group_widget').hide();
    $('#vk_groups').show();
    $('#show_vk').addClass('active');
    $('#show_fb').removeClass('active');
    $('#show_ok').removeClass('active');
}

function show_fb_block(){
    $('#vk_groups').hide();
    $('#ok_group_widget').hide();
    $('.fb-page').show();
    $('#show_fb').addClass('active');
    $('#show_vk').removeClass('active');
    $('#show_ok').removeClass('active');
}

function show_ok_block(){
    $('#vk_groups').hide();
    $('.fb-page').hide();
    $('#ok_group_widget').show();
    $('#show_ok').addClass('active');
    $('#show_vk').removeClass('active');
    $('#show_fb').removeClass('active');
}
