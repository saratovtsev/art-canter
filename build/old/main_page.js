var current_bb = 0;
var count_bb = 0;
var next_bb = 1;

var timerIdBB;
var timerIdPopular;
var autoRotationPopular = true;

var colors = {0: "ff0238", 1: "ff7802", 2: "0fca21", 3: "08a2d6"}

$(document).ready(function(){
    if($("#page_main").length>0){
        // найти все дивы .main_page_big_renab и посчитать их
        var div_big_renab = $(".main_page_big_renab");
        count_bb = div_big_renab.length;
        //console.log(count_bb, " банеров")
        if(count_bb>1){
            //$("#form_search").css("top", "448px");
            autoRotationBB(-1);

            // нарисовать прямоугольники снизу слайдера и выравнять кнопки по центру
            c=0
            for(var num_slide = 0; num_slide<count_bb; num_slide++){
                new_block = '<div id="main_page_big_renab_pages_item_'+num_slide+'" class="main_page_big_renab_pages_item" style="background: #'+colors[c]+'" onclick="rotationOnThis('+num_slide+')"></div>'
                $("#main_page_big_renab_pages").append(new_block)

                if(++c==4) c=0;
            }
            mainWidth = +($("#main_page_big_renab_pages").css('width')).replace('px', '');
            new_width = (mainWidth/2)/count_bb
            if(count_bb>10 || window.matchMedia("(max-width: 768px)").matches){
                new_width = Math.floor(mainWidth/count_bb)
                $(".main_page_big_renab_pages_item").css({"width": new_width+"px"});
                ostatok = mainWidth-new_width*count_bb;
                $("#main_page_big_renab_pages_item_0").css({"width": new_width+ostatok+"px"});
                margin_left = 0
            }
            else{
                margin_left = Math.floor((mainWidth-new_width*count_bb)/2)
            }
            $("#main_page_big_renab_pages_item_0").css({"top": "-55px", "margin-left": margin_left+"px"});
        }

        $("#popular_btn_prev").click(function(){
            autoRotationPopular = false;
            popular_slide_prev();
            setTimeout("autoRotationPopularAgain()", 6000);
        });

        $("#popular_btn_next").click(function(){
            autoRotationPopular = false;
            popular_slide_next();
            setTimeout("autoRotationPopularAgain()", 6000);
        });

        if(count_populars>0){
            timerIdPopular = setInterval("autoRotationPopularSlide()", 4000);
        }
    }
});

/***************** БОЛЬШОЙ БАНЕР СВЕРХУ - РОТАЦИЯ *******************/

function autoRotationBB(n){
    timerIdBB = setInterval("rotationBB(-1)", 8000)
}

function rotationBB(n){
    //console.log("start rotation", current_bb);
    $("#main_page_big_renab_"+current_bb).fadeOut();
    //$("#main_page_big_renab_pages_item_"+current_bb).css("top", "0px");
    $("#main_page_big_renab_pages_item_"+current_bb).animate({top:'0px'});
    new_current_bb = (n==-1)?((current_bb==count_bb-1)?0:current_bb+1):n;
    //alert(next_bb);
    $("#main_page_big_renab_"+new_current_bb).fadeIn();
    //$("#main_page_big_renab_pages_item_"+new_current_bb).css("top", "-55px");
    $("#main_page_big_renab_pages_item_"+new_current_bb).animate({top:'-55px'});
    current_bb = new_current_bb;
}

function rotationBBleft(){
    new_current_bb = current_bb==0?count_bb-1:current_bb-1;
    console.log(new_current_bb);
    clearInterval(timerIdBB);
    rotationBB(new_current_bb);
    current_bb = new_current_bb;
    autoRotationBB(current_bb);
}
function rotationBBright(){
    new_current_bb = current_bb==count_bb-1?0:current_bb+1;
    console.log(new_current_bb);
    clearInterval(timerIdBB);
    rotationBB(new_current_bb)
    current_bb = new_current_bb;
    autoRotationBB(current_bb);
}

function rotationOnThis(m){
    new_current_bb = m;
    console.log(new_current_bb);
    clearInterval(timerIdBB);
    rotationBB(new_current_bb)
    current_bb = new_current_bb;
    autoRotationBB(current_bb);
}
//************************************************




// Популярные мероприятия

var focus_popular;
var count_populars;

function autoRotationPopularSlide(){
    if(autoRotationPopular){
        popular_slide_next();
    }
}

function popular_slide_next(){
    //console.log(focus_popular);
        $("#main_page_popular_events_content_slide_"+focus_popular).fadeOut();
        focus_popular = (focus_popular==(count_populars-1))?0:focus_popular+1
        $("#main_page_popular_events_content_slide_"+(focus_popular)).fadeIn();

        $("#popular_btn_prev").removeClass("popular_prev_forbidden");
        $("#popular_btn_prev").addClass("popular_prev");
}

function popular_slide_prev(){

        $("#main_page_popular_events_content_slide_"+focus_popular).fadeOut();
        focus_popular = (focus_popular==0)?count_populars-1:focus_popular-1
        $("#main_page_popular_events_content_slide_"+(focus_popular)).fadeIn();

        $("#popular_btn_next").removeClass("popular_next_forbidden");
        $("#popular_btn_next").addClass("popular_next");

}

function autoRotationPopularAgain(){
    clearInterval(timerIdPopular)
    autoRotationPopular = true;
    timerIdPopular = setInterval("autoRotationPopularSlide()", 4000);
}
