jQuery.fn.exists = function(){return this.length > 0;}

var id_fest_question; // id феста для отправки вопроса по нему
var global_search = false; // курсор в инпуте основного поиска
var event_search = false; // курсор в инпуте основного поиска

var arr_letters = []; // массив с буквами стран (А - Д, Е - К ...)
var current_countries_item; // текущий элемент массива в списке стран (листалка в главном меню СТРАНЫ)

var showed_countries_block = false; // показан ли блок со странами

var page_main;
var page_event_list;



function hideMessageTehRab(){
    $("#teh_rabot").remove();
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=hide_meswsage_teh_rab",
        success: function(jsondata){
        },
        complete: function(){
        },
        async: true,
        dataType: 'json'
    });
}

function messageTehRab(){
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=message_teh_rab",
        success: function(jsondata){
            if(jsondata.status!=1){
                $('body').append("<div id='teh_rabot' style='position: fixed; width: 300px; z-index: 10000; border: 4px solid #8597B6; top: 0; right: 0; padding: 20px; background-color: #E4E8EF'><div style='position: absolute; top: 3px; right: 3px; cursor: pointer' onclick='hideMessageTehRab()'><u>Закрыть</u></div><br>В данный момент на сайте проводятся технические работы.<br>Если Вы не смогли найти интересующий Вас фестиваль, наши сотрудники с радостью Вам помогут. Пишите нам: <a href='mailto:info@art-center.ru'>info@art-center.ru</a></div>")
            }
        },
        complete: function(){
        },
        async: true,
        dataType: 'json'
    });
}

function form_search_submit(){
    val = $(".main_search_input").val();
    if(val.length>3 || val == 'уфа' || val == 'Уфа' || val == 'УФА' || val == 'ОАЭ' || val == 'оаэ' || val == 'сша' || val == 'США' || val == 'бах' || val == 'Бах' || val == 'СНГ' || val == 'снг' || val == 'Рим' || val == 'РИМ' || val == 'рим' || val == 'КИТ'  || val == 'Кит'  || val == 'кит'){
        if($('#form_search_mini').length > 0){
            setTimeout("$('#mini_form_search_submit').click()", 300);

        }
        else{
            setTimeout("$('#main_form_search_submit').click()", 300);
        }

    }
    else{
        alert('Поисковый запрос слишком мал. (минимум 4 символа)');
    }
}

function makeDotDotDotAndFav(){
    $(".fest_list_item_content_name").dotdotdot();
    $('.fest_list_item_content_name .dotdotdot').each(function(){
        var tmp_fav = $(this).children(".fav_btn");
        var tmp_cup = $(this).children(".cup_img").exists()?$(this).children(".cup_img")[0]['outerHTML']:'';
        var tmp_link = $(this).children("a");
        if (tmp_fav.length > 0 && tmp_link.length > 0){
          $(this).html(tmp_link[0]['outerHTML'] + tmp_fav[0]['outerHTML'] + tmp_cup);
        }
    });
}

$(document).ready(function(){
    if($("input").is("#is_page_event_list")){
        if($("div").is(".fest_list_item_content_name")){
            makeDotDotDotAndFav();
        }
    }



    $("#name_client_order_page").bind('focus', function(){
        if($("#agree_order").prop("checked")){
            $("#agree_order").prop("checked", "checked")
        }
    })

    $("a[rel=example_group], a[rel=group_old]").fancybox({
        'transitionIn'    : 'elastic',
        'transitionOut'   : 'elastic',
        'titlePosition'   : 'over',
    });

    var client1 = new ZeroClipboard($(".link_copy_to_clipboard_baner"), {
      moviePath: "/d/js/ZeroClipboard.swf"
    });

    client1.on("load", function(client1) {
      client1.on("complete", function(client1, args) {
        //$('.link_copy_to_clipboard_baner').hide(); // скрываем для наглядности кнопку
      });
    });

    // календари для формы поиска

    startDate = $("#min_date_real").val();
    endDate = $("#max_date_real").val();

    startDateUnTrue = dateToUnTrue(startDate)
    endDateUnTrue = dateToUnTrue(endDate)



    /*
    new Kalendae.Input('date1', {
        blackout: function (date) {
            //console.log(Kalendae.moment(date).year(), ' ', Kalendae.moment(date).month(), ' ', Kalendae.moment(date).date());
             startDate = startDateUnTrue;
             min_date = new Date(startDate);
                y = Kalendae.moment(date).year()
                m = Kalendae.moment(date).month()
                m++
                d = Kalendae.moment(date).date()
                if(m<10) m='0'+m
                if(d<10) d='0'+d
                cur_date = new Date(y + '-' + m + '-' + d);
                if((cur_date - min_date) > 0){
                    return false
                }
                else{
                    return true;
                }
            //return Kalendae.moment(date).date() % 2; //blackout every other day
        },
        format: 'DD.MM.YYYY',
        weekStart: 1
    });

    new Kalendae.Input('date2', {
        blackout: function (date) {
            //console.log(Kalendae.moment(date).year(), ' ', Kalendae.moment(date).month(), ' ', Kalendae.moment(date).date());
             endDate = endDateUnTrue;
             max_date = new Date(endDate);
                y = Kalendae.moment(date).year()
                m = Kalendae.moment(date).month()
                m++
                d = Kalendae.moment(date).date()
                if(m<10) m='0'+m
                if(d<10) d='0'+d
                cur_date = new Date(y + '-' + m + '-' + d);
                if((cur_date - max_date) < 0){
                    return false
                }
                else{
                    return true;
                }
            //return Kalendae.moment(date).date() % 2; //blackout every other day
        },
        format: 'DD.MM.YYYY',
        weekStart: 1
    });
*/
    $("#date1").change(function(){
        if($("#date3").val()!='0'){
            $("#date2").val('');
        }
        $("#date3").val('0');
    });

    $("#date2").change(function(){
        if($("#date3").val()!='0'){
            $("#date1").val('');
        }
        $("#date3").val('0');
    });


    // Переключатель список/карта
    var myHash = location.hash;
    switcher_map = false;
    if(myHash.length>1){
        myHash = myHash.split('&');
        for(var i=0; i<myHash.length; i++){
            hash_item = myHash[i].split('=');
            if(hash_item[0]=='view' && hash_item[1]=='map'){
                switcher_map = true;
                $('.list_map_switch #list').show();
                $('.list_map_switch #map').hide();
            }
        }
    }


    $('.toggle').toggles({
        on: switcher_map,
        clickable: !$(this).hasClass('noclick'),
        dragable: !$(this).hasClass('nodrag'),
        click: ($(this).attr('rel')) ? $('.'+$(this).attr('rel')) : undefined,
        //on: !$(this).hasClass('off'),
        //checkbox: ($(this).data('checkbox')) ? $('.'+$(this).data('checkbox')) : undefined,
        checkbox:$('#checkbox_form_map'),
        ontext: $(this).data('ontext') || '',
        offtext: $(this).data('offtext') || ''
    });





    $("#checkbox_form_map").change(function(){
        switcherListMap();
    });



    $("#popup_bg").click(function(){
        stop_question();
        stop_login();
        stop_mail();
        stop_help();
        stop_reg();
        stop_rec();
        stop_order_error();
        stop_recall();
        stop_sendme();
        hideMapFest();
        stop_is_parent_event();
    });

    $("#popup_vopros_close").click(function(){stop_question();});
    $("#popup_login_close").click(function(){stop_login();});
    $("#popup_reg_close").click(function(){stop_reg();});
    $("#popup_reс_close").click(function(){stop_rec();});
    $("#popup_reсall_close").click(function(){stop_recall();});
    $("#popup_sendme_close").click(function(){stop_sendme();});



    // если это главная страница
    if($("input").is("#page_main")){
        page_main = true;
        // форма поиска видимая
        $("#form_search").css({
            'display' : 'block'
        });
        // ее нельзя скрыть
        $("#form_search_close").css("display", "none");
        // но можно свернуть. курсор над заголовком, стрелка справа от него
        $(".form_search_header_title_bul, .form_search_header_title_bul_hidden").css('visibility', 'visible');
        $("#form_search_header_title").css('cursor', 'pointer');

        // размер карты больше на 43px
        //$("#map_search").css('height', '613px');
    }

    // если это страница со списком фестивалей или результаты
    if($("input").is("#is_page_event_list") || $("input").is("#is_search_results")){
        page_event_list = true;
        // Большой формы поиска НЕТ
        $("#form_search_id").html('');

    }





    // если это страница заявки
    if($("input").is("#page_order")){
        var page_order = true;
    }


    if(device.mobile() || device.tablet()){
        // Показать слайдер со странами
        $("#countries_slider_link").click(function(){
            $("#countries_block").css({position: 'absolute', display: 'block', 'z-index':999});
            $('#bg_header, #bg_content, #bg_footer').css({display: 'block', opacity: 0, 'z-index':998});
            showed_countries_block = true;
        });

        $('#bg_header, #bg_content, #bg_footer').click(function(event) {
            if(showed_countries_block){
                //alert("Блок показан")

                if($(event.target).closest("#countries_block, #countries_slider_link").length){
                    //alert("клик по ссылке или блоку")
                }
                else{
                    //alert('Hide');
                    $("#countries_block, #bg_header, #bg_content, #bg_footer").css("display", "none");

                    event.stopPropagation();
                    showed_countries_block = false;
                }
            }
            else{
                //alert("Блок НЕ показан")
            }
        });

    }
    else{
        // Показать слайдер со странами
        $("#countries_slider_link").mouseenter(function(){
            $("#countries_block").css("display", "block");
            showed_countries_block = true;
        });
        // Если мышь убирается с кнопки "СТРАНЫ" влево/вверх/вправо то убрать блок со странами
        $("#countries_slider_link").mouseleave(function(e){
            var x = e.pageX;
            var y = e.pageY;

            coord_link = $("#countries_slider_link").offset();
            link_x_start = coord_link.left;
            link_y_start = coord_link.top;
            link_w = $("#countries_slider_link").width();
            link_h = $("#countries_slider_link").height();

            if(x<link_x_start || y<link_y_start || x>(link_x_start+link_w)){
                $("#countries_block").css("display", "none");
            }
        });
    }






    $('#countries_block').mouseleave(function(){
        $("#countries_block").css("display", "none");
    });

    $("#form_search_field_name").focus(function(){
        event_search = true;
    });
    $("#form_search_field_name").blur(function(){
        event_search = false;
    });


    $(".main_search_input, #form_search_field_name").focus(function(){
        global_search = true;
        //console.log('GLOBAL SEARCH')
    }).blur(function(){
        global_search = false;
        //console.log('-= END =- GLOBAL SEARCH')
    });



    // отслеживание нажатия enter
    $(document).keydown(function(e){

        if((e.which == 13) && global_search){
            if($('#form_search_mini').length > 0){
                setTimeout("form_search_submit()", 300) ;
            }
            else{
                setTimeout("form_search_submit()", 300) ;
            }
        }
    });






    /*
    $(".main_menu_search_content_ext").click(function(){
        scrollToFormAndShow();
    });
    */

    $("#form_search_close").click(function(){
        $("#form_search").css("display", "none");
        $('#form_search').queue(function (next){
            /*
            top_fixed = $("#fest_content .fest_content_fixed_menu").offset();
            top_fixed_warning = $("#before_order_filter").offset();
            */
            next();
        });
    });






    $("#podrobnee").click(function(){

        $(".fest_inner_tabs_content").css("display","none");
        $("#polozh").css("display","block");

        $(".tab_item").removeClass("selected_tab");
        $("#tab_polozh").addClass("selected_tab");

        // поднять страницу чтобы было видно начало
        $('body,html').animate({scrollTop:top_fixed.top},200);
    });






    $('select[name="country[]"]').change(function(){
        return_town_find = false;
        name_country = $('select[name="country[]"] option:selected').text();
        id_country = $(this).val();
        country = id_country

        // Получить список городов и сделать селект
        if(parseInt(id_country) > -1){
            $("#quick_place").val('undefined');
            //alert(id_country);

            $('#select-country').selectize();
            var $select = $('#select-city').selectize();
            var control = $select[0].selectize;
            control.clearOptions();

            /*
            control.addOption({
                    value: 4,
                    text: 'Something New'
                });*/

            $.ajax({
                url: '/admin/modules/ajax/country.php',
                type: "POST",
                data: "mode=get_towns_by_country&country="+id_country,
                success: function(jsondata){
                    /*
                    $('select[name="town[]"] option').remove();
                    $('select[name="town[]"]').append('<option value="-1">(Не выбран)</option>');
                    */
                    for(var i=0; i<jsondata.id_dog_res.length; i++){
                        control.addOption({
                            value: i+'_'+jsondata.id_dog_res[i]['id'],
                            text: jsondata.id_dog_res[i]['name']
                        });
                        //alert(jsondata.id_dog_res[i]['name']);
                        //$('select[name="town[]"]').append('<option value="'+jsondata.id_dog_res[i]['id']+'">'+jsondata.id_dog_res[i]['name']+'</option>');
                    }
                },
                complete: function(){
                    //alert(jsondata.status);
                },
                async: false,
                dataType: 'json'
            });
        }
        else{
            $("#button_add_towns").attr('disabled',true);
            $("#button_new_town").attr('disabled',true);
            $("#new_town").attr('disabled',true);
        }
    });


    $('select[name="state22[]"]').change(function(){
        country_name_en = $(this).val();
        location.href="/events/"+country_name_en;
    });

    $(".help_more").hover(
    function(e){
        //$("#bg").css("display", "block");
        $(".tooltip").css("display","none");
        var href = this.id.split('_');
        //$("#tooltip_"+href[0]).css("display","block");

        var y_scroll = $(window).scrollTop();
        var x_mouse = e.pageX
        var y_mouse = e.pageY

        var y_start = y_mouse;// + y_scroll;
        var x_start = x_mouse;

        $("#tooltip_"+href[0]).css({"top":y_start+"px", "left":x_start+"px", "display":"block"})
    },
    function(){
        $(".tooltip").css("display","none");
    });

    $("#bg").click(function(e){
        $(".tooltip").css("display","none");
        $("#bg").css("display","none");

    });

    $('input[name=type]').change(function(){
        if ($('input:radio[name=type]:checked').val() === 'fiz')
        {
            $('.rekviz_for_jur').hide();
        }
        else{
            $('.rekviz_for_jur').show();
        };
    });

    $('input[name=rekv_bik]').change(function(){
        if ($('input:radio[name=type]:checked').val() === 'fiz')
        {
            $('.rekviz_for_jur').hide();
        }
        else{
            $('.rekviz_for_jur').show();
        };
    });

    //$('input[name=rekv_bik]').bind('focus blur keyup change', function() {
    $('input[name=rekv_bik]').bind('focus keyup change', function() {
        getBankByBIK();
    });


    // Список стран с листалкой
    var count_countriers = $(".countries_block_inner_item").length;
    //console.log(count_countriers);

    var t=v=0;
    var item_letters = '';
    $(".countries_block_inner_item").each(function(){
        //console.log(  $(this).children(".countries_block_inner_item_letter").html()     );

        if(item_letters == ''){
            item_letters += $(this).children(".countries_block_inner_item_letter").html() + ' - ';
        }
        else{
            if((t==4) || (v==(count_countriers-1))){
                item_letters += $(this).children(".countries_block_inner_item_letter").html()
                arr_letters.push(item_letters);
            }
        }

        t++; v++;
        //console.log(item_letters, t);

        if(t==5 || t==0){
            t=0;
            item_letters = '';
        }
    })

    for (q=0; q<arr_letters.length; q++) {
        //console.log(arr_letters[q]);
    }

    current_countries_item = 0;
    $("#countries_block_move_right").html(arr_letters[current_countries_item+1]);

    if($('#personal_data_link').length > 0){
        $('#personal_data_link').mousemove(function(e){
            $('#personal_data_content').css({
                'display': 'block',
                'position' : 'fixed',
                'z-index': 9999,
                'top': e.screenY-55,
                'left' : e.screenX+10
            });
        }).mouseleave(function(e){
            $('#personal_data_content').css({
                'display': 'none'
            });
        }).mouseenter(function(e){
            $('#personal_data_content').css({
                'display': 'block',
                'position' : 'fixed',
                'z-index': 9999,
                'top': e.screenY-55,
                'left' : e.screenX+10
            });
        });
    }


});

function scrollToFormAndShow(){
    // если форма скрыта - показать
    if($("input").is("#is_page_event_list") || $("input").is("#is_search_results")){
        if(!form_mini_show){
            show_hide_form_search_mini();
        }
    }
    else{
        if($("#form_search").is(":visible")){
        }
        else{
            $("#form_search").css({
                'display' : 'block'
            });
            showed_form_search = false;
        }

        if(!showed_form_search){
            showHideFormSearch();
        }
    }


    if($("#page_main").length>0 || $("#page_fest").length>0){
        top_form_search= $("#form_search").offset();
        $('body,html').animate({scrollTop:top_form_search.top-90},400);
    }
    else{

        $('body,html').animate({scrollTop:$("#form_search_mini_id").offset().top-190},400);
        //$('body,html').animate({scrollTop:top_fixed_header_search.top+50},400);
    }






    $('#form_search').queue(function (next){
        /*
        top_fixed = $("#fest_content .fest_content_fixed_menu").offset();
        top_fixed_warning = $("#before_order_filter").offset();
        */
        next();
    });
}

function move_countries_right(){
    current_countries_item--;

    countries_block_inner_x = $("#countries_block_inner").position();
    //console.log(current_countries_item, countries_block_inner_x.left);
    $("#countries_block_inner").animate({left: countries_block_inner_x.left+1000 + "px"}, 500);

    if(current_countries_item==(arr_letters.length-2)){
        $("#countries_block_move_right").css("visibility", "visible");
    }
    if(current_countries_item==0){
        $("#countries_block_move_left").css("visibility", "hidden");
    }
    $("#countries_block_move_right").html(arr_letters[current_countries_item+1]);
    $("#countries_block_move_left").html(arr_letters[current_countries_item-1]);

}

function move_countries_left(){
    current_countries_item++;
    countries_block_inner_x = $("#countries_block_inner").position();
    //console.log(countries_block_inner_x.left);
    $("#countries_block_inner").animate({left: countries_block_inner_x.left-1000 + "px"}, 500);
    //если 1-й элемент то показать кнопку влево
    if(current_countries_item==1){
        $("#countries_block_move_left").css("visibility", "visible");
    }
    // изменить текст кнопки влево
    $("#countries_block_move_left").html(arr_letters[current_countries_item-1]);
    // и если это не последний элемент то изменить текст кнопки вправо. если последжний - скрыть кнопку
    if(current_countries_item!=(arr_letters.length-1)){
        $("#countries_block_move_right").html(arr_letters[current_countries_item+1]);
    }
    else{
        $("#countries_block_move_right").css("visibility", "hidden");
    }
}


/*
function startList() {
    if (document.all && document.getElementById) {
        navRoot = document.getElementById("upmenu");
        for (i = 0; i < navRoot.childNodes.length; i++) {
            node = navRoot.childNodes[i];
            if (node.nodeName == "LI") {
                node.onmouseover = function () {
                    this.className = " over";
                }
                node.onmouseout = function () {
                    this.className = this.className.replace(" over", "");
                }
            }
        }
    }
}

window.onload = function () {
    startList();
}
*/
function diplomas(id,w,h){
 w=w+30; h=h+30;
 Photo=window.open(base_dir+"d/pict.php?i="+id.replace(/^.*\/\/[^\/]+/, '')+"&w="+w+"&h="+h,"Photo","width=" + w + ",height=" + h + ",resizable=no,scrollbars=yes");
 Photo.focus();
}



/*
function check_valid_rekv(){
    //console.log(123);
    $(".may_br_error").css("border", "1px solid #ccc");

    var error='';

    var email_user = $('#user_email').val();
    if(email_user.length == 0){
        error = error + '"E-mail" не может быть пустым\n';
        $('#user_email').css("border", "1px solid red");
    }
    else{
        reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if (!email_user.match(reg)) {
           error = error + 'Неверно заполнено поле "E-mail"\n';
           $('#user_email').css("border", "1px solid red");
        }
    }
    var f_user = $('#user_f').val();
    if(f_user.length == 0){
        error = error + 'Необходимо заполнить фамилию\n';
        $('[name=user_f]').css("border", "1px solid red");
    }
    var i_user = $('#user_i').val();
    if(i_user.length == 0){
        error = error + 'Необходимо заполнить имя\n';
        $('[name=user_i]').css("border", "1px solid red");
    }

    if ($('input:radio[name=type]:checked').val() === 'jur'){
        // Проверить ИНН
        var inn = $('[name="rekv_inn"]').val();
        var true_inn = is_valid_inn(inn);
        if(!true_inn){
            error = error + 'Неверно заполнено поле "ИНН"';
        }

        // проверить КПП
        var kpp = $('[name="rekv_kpp"]').val();
        if((kpp.length != 0) && true_inn){
            // Если ИНН верный то проверить код подразделения
            if(kpp.length == 9){
                true_kpp = false;
                if(inn.substr(0,4) == kpp.substr(0,4)){
                    true_kpp = true;
                }
            }
            else{
                true_kpp = false;
            }

            if(!true_kpp){
                error = error + 'Неверно заполнено поле "КПП"\n';
            }
        }


        // Р/С
        var rs = $('[name="rekv_rs"]').val();
        var bik = $('[name="rekv_bik"]').val();
        if(rs.length > 0){
            var true_rs = false;
            if(rs.length == 20){
                if(bik.length > 0){
                    var true_rs = fn_checkRS(rs,bik)
                }
                else{
                    error = error + 'невозможно проверить правильность заполнения р/с без указания "БИК"\n';
                }
            }
            if(!true_rs){
                error = error + 'Неверно заполнено поле "Расчетный счет"\n';
            }

        }

    }

    if(error.length > 0){
        document.forms["rekv_form"].submit()

        //alert(error);
        //return false;

    }
    else{
        //return true;
        document.forms["rekv_form"].submit()
    }

}
*/

function is_valid_inn(i){
    if ( i.match(/\D/) ) return false;

    var inn = i.match(/(\d)/g);

    if ( inn.length == 10 ){
        return inn[9] == String(((
            2*inn[0] + 4*inn[1] + 10*inn[2] +
            3*inn[3] + 5*inn[4] +  9*inn[5] +
            4*inn[6] + 6*inn[7] +  8*inn[8]
        ) % 11) % 10);
    }
    else if ( inn.length == 12 ){
        return inn[10] == String(((
             7*inn[0] + 2*inn[1] + 4*inn[2] +
            10*inn[3] + 3*inn[4] + 5*inn[5] +
             9*inn[6] + 4*inn[7] + 6*inn[8] +
             8*inn[9]
        ) % 11) % 10) && inn[11] == String(((
            3*inn[0] +  7*inn[1] + 2*inn[2] +
            4*inn[3] + 10*inn[4] + 3*inn[5] +
            5*inn[6] +  9*inn[7] + 4*inn[8] +
            6*inn[9] +  8*inn[10]
        ) % 11) % 10);
    }
    return false;
}

/*
Алгоритм проверки счёта с помощью БИКа банка:
1. Изменение счета с учетом БИКа(см ниже).
2. Вычисляется контрольная сумма со следующими весовыми коэффициентами: (7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1)
3. Вычисляется контрольное число как остаток от деления контрольной суммы на 10
4. Контрольное число сравнивается с нулём. В случае их равенства расчётного счёт считается правильным.
*/

//функция проверки правильности указания банковского счёта
function fn_bank_account(Str)
{
    var result = false;
    var Sum = 0;

    //весовые коэффициенты
    var v = [7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1,3,7,1];

    for (var i = 0; i <= 22; i++)
    {
        //вычисляем контрольную сумму
        Sum = Sum + ( Number(Str.charAt(i)) * v[i] ) % 10;
    }

    //сравниваем остаток от деления контрольной суммы на 10 с нулём
    if(Sum % 10 == 0)
    {
        result = true;
    }
    return result;
}

/*
Проверка правильности указания корреспондентского счёта:
1. Для проверки контрольной суммы перед корреспондентским счётом добавляются "0" и два знака БИКа банка, начиная с пятого знака.
*/
function fn_checkKS(Account,BIK)
{
    return fn_bank_account('0'+BIK.substr(4,2)+Account);
}

/*
Проверка правильности указания расчётного счёта:
1. Для проверки контрольной суммы перед расчётным счётом добавляются три последние цифры БИКа банка.
*/
function fn_checkRS(Account,BIK)
{
    return fn_bank_account(BIK.substr(6,3)+Account);
}



function getBankByBIK(){
    /*
    var bik = $('input[name=rekv_bik]').val();
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=getbank&bik="+bik,
        success: function(jsondata){

            if(jsondata.status == 1){
                $('input[name=rekv_bankname]').val(jsondata.name);
                $('input[name=rekv_ks]').val(jsondata.ks);
                $('input[name=rekv_city]').val(jsondata.city);
            }
            else{
                $('input[name=rekv_bankname]').val('');
                $('input[name=rekv_ks]').val('');
                $('input[name=rekv_city]').val('');
            }
            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
    */
}


/*
function startSearchExt(){
    $('#search_box_main').show();
    $('#search_box_map').hide();
}
*/
var searching_map;
function startSearchMap(){
    searching_map = true;
    $('#search_box_main').hide();
    $('#search_box_map').show();
    var myLatLng = new google.maps.LatLng(53.067627, 34.094696);
    //MYMAP.init('#map-canvas', myLatLng, 4);
    //MYMAP.placeMarkers();
}

function print_r( array, return_val ) {    // Prints human-readable information about a variable

    var output = "", pad_char = " ", pad_val = 4;

    var formatArray = function (obj, cur_depth, pad_val, pad_char) {
        if(cur_depth > 0)
            cur_depth++;

        var base_pad = repeat_char(pad_val*cur_depth, pad_char);
        var thick_pad = repeat_char(pad_val*(cur_depth+1), pad_char);
        var str = "";

        if(obj instanceof Array || obj instanceof Object) {
            str += "Array\n" + base_pad + "(\n";
            for(var key in obj) {
                if(obj[key] instanceof Array || obj[key] instanceof Object) {
                    str += thick_pad + "["+key+"] => "+formatArray(obj[key], cur_depth+1, pad_val, pad_char);
                } else {
                    str += thick_pad + "["+key+"] => " + obj[key] + "\n";
                }
            }
            str += base_pad + ")\n";
        } else {
            str = obj.toString();
        };

        return str;
    };

    var repeat_char = function (len, char) {
        var str = "";
        for(var i=0; i < len; i++) { str += char; };
        return str;
    };

    output = formatArray(array, 0, pad_val, pad_char);

    if(return_val !== true) {
        document.write("<pre>" + output + "</pre>");
        return true;
    } else {
        return output;
    }
}

function dateToTrue(date){
    return date.substr(8,2)+'.'+date.substr(5,2)+'.'+date.substr(0,4);
}
function dateToUnTrue(date){
    return date.substr(6,4)+'-'+date.substr(3,2)+'-'+date.substr(0,2);
}

function numToNameMounth(num){
    switch(num){
        case "01": {return "января";break;}
        case "02": {return "февраля";break;}
        case "03": {return "марта";break;}
        case "04": {return "апреля";break;}
        case "05": {return "мая";break;}
        case "06": {return "июня";break;}
        case "07": {return "июля";break;}
        case "08": {return "августа";break;}
        case "09": {return "сентября";break;}
        case "10": {return "октября";break;}
        case "11": {return "ноября";break;}
        case "12": {return "декабря";break;}
    }
}

function twoDatesToString(date1, date2){
    var y1 = date1.substr(0,4);
    var m1 = numToNameMounth(date1.substr(5,2));
    var d1 = date1.substr(8,2);
    var y2 = date2.substr(0,4);
    var m2 = numToNameMounth(date2.substr(5,2));
    var d2 = date2.substr(8,2);
    var res = parseInt(d1) + ( y1 == y2 ? (m1 == m2 ? ' ' : (' ' + m1)) : ' ' + m1 + ' ' + y1) + ' - ' + parseInt(d2) + ' ' + m2 + ' ' + y2
    return res;
}

// Generates a storable representation of a value
function serialize(mixed_val){
    switch (typeof(mixed_val)){
        case "number":
            if (isNaN(mixed_val) || !isFinite(mixed_val)){
                return false;
            } else{
                return (Math.floor(mixed_val) == mixed_val ? "i" : "d") + ":" + mixed_val + ";";
            }
        case "string":
            return "s:" + mixed_val.length + ":\"" + mixed_val + "\";";
        case "boolean":
            return "b:" + (mixed_val ? "1" : "0") + ";";
        case "object":
            if (mixed_val == null) {
                return "N;";
            }else if(mixed_val instanceof Array){
                var idxobj = { idx: -1 };
        var map = []
        for(var i=0; i<mixed_val.length;i++) {
            idxobj.idx++;
                        var ser = serialize(mixed_val[i]);

            if (ser) {
                            map.push(serialize(idxobj.idx) + ser)
            }
        }

                return "a:" + mixed_val.length + ":{" + map.join("") + "}"

            }
            else {
                var class_name = get_class(mixed_val);

                if (class_name == undefined){
                    return false;
                }

                var props = new Array();
                for (var prop in mixed_val) {
                    var ser = serialize(mixed_val[prop]);

                    if (ser) {
                        props.push(serialize(prop) + ser);
                    }
                }
                return "O:" + class_name.length + ":\"" + class_name + "\":" + props.length + ":{" + props.join("") + "}";
            }
        case "undefined":
            return "N;";
    }
    return false;
}

/*
var fixed;
var fixed_warning;
var fixed_header;


var top_fixed;
var top_fixed_warning;
var top_fixed_header_search;

var no_fixed_val = fixed_val = form_val = '';
*/



$(document).ready(function(){
    // fixed = fixed_warning = fixed_header = false;




    if($(".header_search").length>0){
        //top_fixed_header_search = $("#header .header_search").offset();
        //$(".header_search").clone().appendTo("#fixed_blocks");

        //$("#fixed_blocks .header_search div .main_search_input").removeAttr('id').attr('id', 'main_search_input_fixed');


        $(".main_search_input, #form_search_field_name").focus(function(){
            global_search = true;
            //console.log('GLOBAL SEARCH')
        }).blur(function(){
            global_search = false;
            //console.log('-= END =- GLOBAL SEARCH')
        });
    }

    /*
    if($("#before_order_filter").length>0){
        $(".order_filter").clone().appendTo("#fixed_blocks");
        $('#form_search_header_switch_form_map').remove();
    }
    */
    /*
    if($(".fest_content_fixed_menu").length>0){
        top_fixed = $("#fest_content .fest_content_fixed_menu").offset();
        $(".fest_content_fixed_menu").clone().appendTo("#fixed_blocks");
    }
    */

    $("a").each(function functionName() {
      if ($(this).data('title')) {
        $(this).tooltip({ html: true});
      }
    })
    $("a").each(function(){
        $(this).attr('isset', 'true' );
    })

    $(".tab_item").click(function(){
        var href = this.id.split('_');
        location.hash=href[1];
        $(".fest_inner_tabs_content").css("display","none");
        $("#"+href[1]).css("display","block");
        $(".tab_item").removeClass("selected_tab");
        $("."+this.id).addClass("selected_tab");
        // поднять страницу чтобы было видно начало
        top_fixed = $("#start_of_page").offset();
        //$('body,html').animate({scrollTop:top_fixed.top-$(".fest_content_fixed_menu").height()},200);
        //$('body,html').scrollTop(top_fixed.top-95);
        $("body").animate({"scrollTop":top_fixed.top-95},"slow");

        console.log(top_fixed.top);

    });

    // если это страница фестиваля
    if($("input").is("#page_fest")){
        var page_fest = true;


        // Если есть хэш - показать что надо, еслди нету - первая вкладка
        var myHash = location.hash.substr(1, location.hash.length);
        if(myHash.length>0){
            $("#tab_"+myHash).click();
        }
        else{
            // Если фестиваль существует
            if($("div").is(".tab_item")){
                // изначально показать первую вкладку
                var tab_id = $('.tab_item:first').attr('id');
                var tab_name = $('.tab_item:first').attr('id').substr(4, $('.tab_item:first').attr('id').length);
                //alert(tab_id+ ' '+tab_name);

                $("#"+tab_name).css("display","block");
                $("#"+tab_id).addClass("selected_tab");
            }
        }
    }

    $(".main_menu_search").click(function(){
        //var val = $("#fixed_blocks .header_search #header_search_line_input .main_search_input").val();
        var val = $(".main_search_input").val();

        //val = val.length<4?val2:val

        if(val.length>3 || val == 'уфа' || val == 'Уфа' || val == 'УФА' || val == 'ОАЭ' || val == 'оаэ' || val == 'сша' || val == 'США' || val == 'бах' || val == 'Бах' || val == 'СНГ' || val == 'снг' || val == 'Рим' || val == 'РИМ' || val == 'рим' || val == 'КИТ'  || val == 'Кит'  || val == 'кит'){
            if($('#form_search_mini').length > 0){
                setTimeout("$('#mini_form_search_submit').click()", 300);

            }
            else{
                setTimeout("$('#main_form_search_submit').click()", 300);
            }

        }
        else{
            alert('Поисковый запрос слишком мал. (минимум 4 символа)');
        }
    });





    /*
    no_fixed_val = fixed_val = form_val = '';

    $('#main_search_input_nofixed').bind('focus', function(){focus_main_search_input_nofixed = true; console.log('заполняется НЕ примагниченный поиск');})
    $('#main_search_input_fixed').bind('focus', function(){focus_main_search_input_fixed = true; console.log('заполняется примагниченный поиск');});
    $('#form_search_field_name').bind('focus', function(){focus_form_search_field_name = true; console.log('заполняется в форме');});

    $('#main_search_input_nofixed, #main_search_input_fixed, #form_search_field_name').focusout(function(){
        focus_main_search_input_nofixed = focus_main_search_input_fixed = focus_form_search_field_name = false; console.log('Фокус потерян');
    })


    setInterval('checkNoFixedVal()', 200);
    setInterval('checkFixedVal()', 200);
    setInterval('checkFormVal()', 200);
    */
});





/*
no_fixed_val = fixed_val = form_val = $('#main_search_input_nofixed').val();

var focus_main_search_input_nofixed,
    focus_main_search_input_fixed,
    focus_form_search_field_name = false;

function checkNoFixedVal(){
    var new_no_fixed_val = $('#main_search_input_nofixed').val();
    if(new_no_fixed_val != no_fixed_val && focus_main_search_input_nofixed){
        console.log("снхр из НЕпримагниченного")
        $('#main_search_input_fixed').val(new_no_fixed_val);
        $('#form_search_field_name').val(new_no_fixed_val);
        no_fixed_val = fixed_val = form_val = new_no_fixed_val;
    }
}

function checkFixedVal(){
    var new_fixed_val = $('#main_search_input_fixed').val();
    if(new_fixed_val != fixed_val && focus_main_search_input_fixed){
        console.log("снхр из примагниченного")
        $('#main_search_input_nofixed').val(new_fixed_val);
        $('#form_search_field_name').val(new_fixed_val);
        fixed_val = no_fixed_val = form_val = new_fixed_val;
    }
}

function checkFormVal(){
    var new_form_val = $('#form_search_field_name').val();
    if(new_form_val != form_val && focus_form_search_field_name){
        console.log("снхр из формы")
        $('#main_search_input_fixed').val(new_form_val);
        $('#main_search_input_nofixed').val(new_form_val);
        form_val = no_fixed_val =  fixed_val = new_form_val;
    }
}
*/





















$(function(){
    $(window).scroll(function(){
        // Появление кнопки "Вверх"
        if($(this).scrollTop() > 600){
            $('#toup').fadeIn();
        }else{
            $('#toup').fadeOut();
        }

        //top_fixed_warning = $("#before_order_filter").offset();

        //$('#fixed_blocks').css({top: $(document).scrollTop()});


        // Фиксация верхнего поиска
        /*
        if($(".header_search").length>0 && $("#page_order").length==0){
            if(top_fixed_header_search.top <= $(this).scrollTop()){

                if(!fixed_header){
                    if($("#is_search_results").length>0){
                        $(".main_menu_search_content_ext").css({"display": "none", "margin": 0});
                    }

                    fixed_header = true;

                    $("#fixed_blocks .header_search").css({"width": "1260px", "z-index": "9999", "display": "block"}).removeClass("fixed_header_search_nofixed").addClass("fixed_header_search_fixed");

                    $('#fixed_blocks').addClass('fixed_blocks_shadow');
                }
            }
            else{
                //console.log($(this).scrollTop(), top_fixed.top);
                if($(this).scrollTop() < top_fixed_header_search.top){
                    if(fixed_header){

                        if($("#is_search_results").length>0){
                            $(".main_menu_search_content_ext").css({"display": "block", "margin": "5px 0 0 38px"});
                        }

                        $("#fixed_blocks .header_search").css({"display": "none"}).removeClass("fixed_header_search_fixed").addClass("fixed_header_search_nofixed");
                        $('#fixed_blocks').removeClass('fixed_blocks_shadow');
                        fixed_header = false;
                    }
                }
            }
        }
        */

        // Фиксация фильтра
        /*
        if($("#before_order_filter").length>0){

            height_header_search = 72
            if($("#is_search_results").length>0){
                height_header_search = 60
            }


            if(top_fixed_warning.top <= $(this).scrollTop()+height_header_search){
                if_form_search = $('#form_search').is(':visible')?$('#form_search').height():0
                if(!fixed_warning){
                    fixed_warning = true;
                    $("#fixed_blocks .order_filter").css({"width": "1260px", "z-index": "9999", "display": "block"}).removeClass("fixed_header_search_nofixed").addClass("fixed_header_search_fixed");
                    // перенести переключатель список/карта
                    $('#fixed_blocks .order_filter').prepend($('#form_search_header_switch_form_map'));
                }
            }
            else{
                //console.log($(this).scrollTop(), top_fixed.top);
                if($(this).scrollTop() < top_fixed_warning.top){
                    if(fixed_warning){
                        $("#fixed_blocks .order_filter").css({"width": "1260px", "z-index": "9999", "display": "none"}).removeClass("fixed_header_search_nofixed").addClass("fixed_header_search_fixed");
                        fixed_warning = false;
                        $('.order_filter').eq(1).prepend($('#form_search_header_switch_form_map'));
                    }
                }
            }
        }
        */

        // Фиксация меню (если это страница фестиваля)
        /*
        if($(".fest_content_fixed_menu").length>0){
            //height_header_search = 72
            height_header_search = 50
            if($("#is_search_results").length>0){
                height_header_search = 55
            }

            if(top_fixed.top <= $(this).scrollTop()+height_header_search){

                if(!fixed){
                    fixed = true;
                    $("#fixed_blocks .fest_content_fixed_menu").css({"display": "block"}).addClass('fixed_menu_fixed').removeClass('fixed_menu_nofixed');
                    $(".link_to_order_on_fixed").css({"visibility": "visible"});
                }
            }
            else{
                //console.log($(this).scrollTop(), top_fixed.top);
                if($(this).scrollTop() < top_fixed.top){
                    if(fixed){
                        $("#fixed_blocks .fest_content_fixed_menu").css({"display": "none"}).addClass('fixed_menu_nofixed').removeClass('fixed_menu_fixed');
                        $(".link_to_order_on_fixed").css({"visibility": "hidden"});
                        fixed = false;
                    }
                }
            }
        }
        */
    });

    $('#toup').click(function(){
        $('body,html').animate({scrollTop:0},800);
    });
});





// Примагничивание
$(document).ready(function(){
    if($('.header_search').exists()){
        $('.header_search')
            .sticky({topSpacing:0})
            .on('sticky-start', function() {
                if($('#is_search_results').exists()){
                    $('.main_menu_search_content_ext').css({'display': 'none'});
                }
                $('.header_search').addClass('fixed_header_search_fixed');
                $('.header_search').addClass('with_shadow_bottom');

            })
            .on('sticky-end', function() {
                $('.main_menu_search_content_ext').css({'display': 'block'});
                $('.header_search').removeClass('fixed_header_search_fixed');
                $('.header_search').removeClass('with_shadow_bottom');
            });
    }


        $('.order_filter')
            .sticky({topSpacing:$('#is_search_results').exists()?65:90})
            .on('sticky-start', function() {
                $('.order_filter').addClass('order_filter_fixed');
                $('.order_filter').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $('.header_search').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
            })
            .on('sticky-end', function() {
                $('.order_filter').removeClass('order_filter_fixed')
                $('.header_search').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $('.order_filter').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
            });




    if($('.fest_content_fixed_menu').exists()){
        $('.fest_content_fixed_menu')
            .sticky({topSpacing:90})
            .on('sticky-start', function() {
                $('.header_search').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
                $(".link_to_order_on_fixed").css({"visibility": "visible"});
                $('.fest_content_fixed_menu').addClass('fixed_menu_fixed').css({"width": "1212"});
                $('.fest_content_fixed_menu').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $(window).resize(function(){
                    $('.fest_content_fixed_menu').css({"width": "1212"});
                });

            })
            .on('sticky-end', function() {
                $('.header_search').removeClass('without_shadow_bottom').addClass('with_shadow_bottom');
                $(".link_to_order_on_fixed").css({"visibility": "hidden"});
                $('.fest_content_fixed_menu').removeClass('with_shadow_bottom').addClass('without_shadow_bottom');
            });
    }

});



var blink_attention;
$(document).ready(function(){
    if($('.attention a').exists()){
        blink_attention = 1;
        setInterval("blink_attention_closing()", 1000);
    }
});

function blink_attention_closing(){

    if(blink_attention == 1){
        $('.attention a').css("border", "1px solid red");
        blink_attention = 0;
    }
    else{
        $('.attention a').css("border", "1px solid #2D910E");
        blink_attention = 1;
    }
}



// удаление position relative для нормального отображения при прокрутке

var stButton = setInterval(function() {
    if($(".stButton").length){
        $('.stButton').css('position', 'inherit');
        clearInterval(stButton);
    }
}, 100);











function showExtGenres(){
    $("#ext_genres").css("display", "block");
    $("#a_show_ext_genres").css("display", "none");
}

function reCallTooltip(){
    $("a").each(function(){
        if($(this).attr("data-title")){
          $(this).tooltip();
          console.log($(this).attr('class'));
        }
    })
}

/**
 * Функция склонения числительных окончаний
 * Пример использования:
 *    numberOf(1, 'комментар', ['ий','ия','иев']);// вернет значение "комментарий"
 *    numberOf(2, 'комментар', ['ий','ия','иев']);// вернет значение "комментария"
 *    numberOf(5, 'комментар', ['ий','ия','иев']);// вернет значение "комментариев"
 *
 */
function numberOf(numberof, value, suffix)
{
    var keys = [2, 0, 1, 1, 1, 2];
    var mod = numberof % 100;
    var suffix_key = mod > 4 && mod < 20 ? 2 : keys[Math.min(mod%10, 5)];

    return value + suffix[suffix_key];
}

function textintext(re, str){
    var patt = new RegExp(re);
    if (patt.test(str))
        return true;
    else
        return false;
}


function in_array(needle, haystack, strict) {    // Checks if a value exists in an array
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    var found = false, key, strict = !!strict;
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
    return found;
}




(function(){
    var helpVideoMessage = {
        setupListeners: function() {
            $('.help-video-close').on('click', $.proxy(this.close_alert, this));
            $('.help-video-min').on('click', $.proxy(this.show_alert, this));
        },

        close_alert: function(title) {
            $('.help-video').animate({
                width: 0,
                opacity: 0},
                300).queue(function (next){
                    $('.help-video').css('display', 'none');
                    next();
                });

            $('.help-video-min').animate({right: 0},300);
        },

        show_alert: function(title) {
            $('.help-video').css('display', 'block')
                .queue(function(next){
                    $('.help-video').animate({
                        width: "243px",
                        opacity: 1},
                        300);
                        next();
                    });

            $('.help-video-min').animate({right: -50},300);
        }

    }

    $(document).ready(function(){
        helpVideoMessage.setupListeners();


        if($.cookie('show_help_video') === null){
            console.log($.cookie('show_help_video'));
            setTimeout(function(){
                helpVideoMessage.show_alert();
            }, 10000);

            setTimeout(function(){
                helpVideoMessage.close_alert();
            }, 13000);
            $.cookie('show_help_video', 'true', {path: '/'});

        }
        else{
            $('.help-video-min').css("right", 0);
        }


    });
}());
