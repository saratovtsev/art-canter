var sendme_id, sendme_target

function start_sendme(id, target){
    sendme_id = id;
    sendme_target = target;
    
    //$('#sendme_target').html($('.selected_tab').html());
    
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);
    
    $(".popup_window_inner_field").css("display", "block");
    $("#message_sendme").css("display", "none");
    
    var height = $('#popup_sendme').height();
    var width = $('#popup_sendme').width();
    var width = 393;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_sendme").animate({opacity: "1"}, 300);
    $('#popup_sendme').css({display: "block"});
    $('#popup_sendme').css("margin", "-"+height+"px 0 0 -"+width+"px");
    return false;
}

$(document).ready(function(){
    //start_sendme(3288, 'polozh');
})

function stop_sendme(){
    //$("#message_new_question").html('');
        
    $('#popup_sendme_mail').val('');
    
    $("#popup_sendme").animate({opacity: "0"}, 300);
    $('#popup_sendme').css({display: "none"});
    hidePopupBg();
    return false;
}

function ProcessSendme(){
    var mail = $('#popup_sendme_mail').val();
    var error = '';

    if(mail=='')error += 'Не заполнен адрес электронной почты\n';

    if(error.length!=0){
        alert(error)
    }
    else{
        $("#message_recall").html('');
        $("#message_recall").css("display", "none");
        $(".popup_window_inner_field").css("display", "block");
        
        $("#message_sendme").html('Пожалуйста подождите..');
        $("#message_sendme").css("display", "block");
        
        $.ajax({
            url: '/modules/ajax/user-backend.php',
            type: "POST",
            data: "mode=sendme&target="+sendme_target+"&id="+sendme_id+"&email="+mail,
            success: function(jsondata){
                
                status = parseInt(jsondata.substr(-2,1));

                if(status == 1){
                    $(".popup_window_inner_field").css("display", "none");
                    $("#message_sendme").html('Информация успешно отправлена на указанный Вами адрес');
                    $("#message_sendme").css("display", "block");
                    
                    $('#popup_sendme_mail').val('');
                    
                    setTimeout('stop_sendme()', 20000);
                    
                    //yaCounter17972227.reachGoal('oksendrecall');
                }
                else{
                    $("#message_sendme").html('Произошла ошибка. Проверьте правильность введенных данных');
                    $("#message_sendme").css("display", "block");
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'text'
        });
    }    
}




function start_recall(){
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);
    
    $(".popup_window_inner_field").css("display", "block");
    $("#message_recall").css("display", "none");
    
    var height = $('#popup_recall').height();
    var width = $('#popup_recall').width();
    var width = 393;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_recall").animate({opacity: "1"}, 300);
    $('#popup_recall').css({display: "block"});
    $('#popup_recall').css("margin", "-"+height+"px 0 0 -"+width+"px");
    return false;
}

$(document).ready(function(){
    //start_recall();
})

function stop_recall(){
    //$("#message_new_question").html('');
        
    $('#popup_recall_phone').val('');
    $('#popup_recall_name').val('');
    
    $("#popup_recall").animate({opacity: "0"}, 300);
    $('#popup_recall').css({display: "none"});
    hidePopupBg();
    return false;
}

function ProcessRecall(){
    var phone = $('#popup_recall_phone').val();
    var viber = $('#popup_recall_viber').val();
    var whatsapp = $('#popup_recall_whatsapp').val();
    var name = $('#popup_recall_name').val();
    var error = '';

    if(phone=='' && viber == '' && whatsapp == '')error += 'Не заполнен номер телефона\n';
    if(name=='')error += 'Не заполнено имя\n';

    if(error.length!=0){
        alert(error)
    }
    else{
        $("#message_recall").html('');
        $("#message_recall").css("display", "none");
        $(".popup_window_inner_field").css("display", "block");
        
        $("#message_recall").html('Пожалуйста подождите..');
        $("#message_recall").css("display", "block");

        $.ajax({
            url: '/modules/ajax/user-backend.php',
            type: "POST",
            data: "mode=help_recall&phone="+phone+"&viber="+viber+"&whatsapp="+whatsapp+"&name="+name+"&href="+location.href,
            success: function(jsondata){
                if(jsondata.status == 1){
                    $(".popup_window_inner_field").css("display", "none");
                    $("#message_recall").html('Спасибо за заявку!<br>Наш менеджер перезвонит Вам в ближайшее время.<br>(Запросы, присланные после 18.00, будут обработаны на следующий день)');
                    $("#message_recall").css("display", "block");
                    
                    $('#popup_recall_phone').val('');
                    $('#popup_recall_name').val('');
                    
                    setTimeout('stop_recall()', 20000);
                    
                    yaCounter17972227.reachGoal('oksendrecall');
                }
                else{
                    $("#message_recall").html('Произошла ошибка. Проверьте правильность введенных данных');
                    $("#message_recall").css("display", "block");
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'json'
        });
    }    
}





function start_help(){
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);
    
    $(".popup_window_inner_field, .popup_window_inner_big_field, .popup_window_inner_field_note").css("display", "block");
    $("#message_help").css("display", "none");
    
    var height = $('#popup_help').height();
    var width = $('#popup_help').width();
    var width = 393;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_help").animate({opacity: "1"}, 300);
    $('#popup_help').css({display: "block"});
    $('#popup_help').css("margin", "-"+height+"px 0 0 -"+width+"px");
	if (history.pushState) {history.pushState(null, null, '#popup_help');}
    return false;
}

$(document).ready(function(){
    //start_help();
	if ($('#popup_help').length > 0) {
		var hash = window.location.hash.substring(1);
		if(hash && hash=='popup_help') {
			start_help();
		}
	}
})

function stop_help(){
    //$("#message_new_question").html('');
        
    $('#popup_help_email').val('');
    $('#popup_help_name').val('');
    $('#popup_help_text').val('');
    
    $("#popup_help").animate({opacity: "0"}, 300);
    $('#popup_help').css({display: "none"});
    hidePopupBg();
	if (history.pushState) {history.pushState(null, null, window.location.href.split('#')[0]);} else {document.location.replace('#');}
    return false;
}

function ProcessHelpPhone(){
    var phone = $('#popup_help_phone').val();
    var viber = $('#popup_help_viber').val();
    var whatsapp = $('#popup_help_whatsapp').val();
    var name = $('#popup_help_name').val();
    var agree = $('#popup_help_agree').prop('checked');
    var error = '';

    if(phone=='' && viber == '' && whatsapp == '')error += 'Не заполнен номер телефона\n';
    if(name=='')error += 'Не заполнено имя\n';
    if(!agree)error += 'Необходимо дать согласие на обработку персональных данных\n';

    if(error.length!=0){
        alert(error)
    }
    else{
        $("#message_help").html('');
        $("#message_help").css("display", "none");
        $(".popup_window_inner_field").css("display", "block");
        
        $.ajax({
            url: '/modules/ajax/user-backend.php',
            type: "POST",
            data: "mode=help_phone&phone="+phone+"&name="+name+"&viber="+viber+"&whatsapp="+whatsapp,
            success: function(jsondata){
                if(jsondata.status == 1){
                    $(".popup_window_inner_field, .popup_window_inner_big_field, .popup_window_inner_field_note").css("display", "none");
                    $("#message_help").html('Спасибо за заявку! Наш менеджер перезвонит Вам в ближайшее время.<br>(Запросы, присланные после 18.00, будут обработаны на следующий день)');
                    $("#message_help").css("display", "block");
                    
                    $('#popup_help_phone').val('');
                    $('#popup_help_name').val('');
                    
                    setTimeout('stop_help()', 2000);
                    
                    yaCounter17972227.reachGoal('oksendhelp');
                }
                else{
                    $("#message_email").html('Произошла ошибка. Проверьте правильность введенных данных');
                    $("#message_email").css("display", "block");
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'json'
        });
    }    
}

function ProcessHelpPhone2(){
    var phone = $('#popup_help_phone2').val();
    var name = $('#popup_help_name2').val();
    var agree = $('#popup_help_agree2').prop('checked');
    var error = '';

    if(phone=='')error += 'Не заполнен номер телефона\n';
    if(name=='')error += 'Не заполнено имя\n';
    if(!agree)error += 'Необходимо дать согласие на обработку персональных данных\n';

    if(error.length!=0){
        alert(error)
    }
    else{
        $("#message_help2").html('');
        $("#message_help2").css("display", "none");
        $(".popup_window_inner_field").css("display", "block");
        
        $.ajax({
            url: '/modules/ajax/user-backend.php',
            type: "POST",
            data: "mode=help_phone&phone="+phone+"&name="+name,
            success: function(jsondata){
                if(jsondata.status == 1){
                    $(".popup_window_inner_field, .popup_window_inner_big_field, .popup_window_inner_field_note, .popup_window_inner_warning").css("display", "none");
                    $("#message_help2").html('Спасибо за заявку! Наш менеджер перезвонит Вам в ближайшее время.<br>(Запросы, присланные после 18.00, будут обработаны на следующий день)');
                    $("#message_help2").css("display", "block");
                    
                    $('#popup_help_phone2').val('');
                    $('#popup_help_name2').val('');
                    
                    yaCounter17972227.reachGoal('oksendhelp');
                }
                else{
                    $("#message_email2").html('Произошла ошибка. Проверьте правильность введенных данных');
                    $("#message_email2").css("display", "block");
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'json'
        });
    }    
}

function ProcessHelp(){
    /* получаем данные из формы */
    
    var name = $('#popup_help_name').val();
    var email = $('#popup_help_email').val();
    var text = $('#popup_help_text').val();
    var error = '';
    if(name=='')error += 'Не заполнено имя\n';
    if(email=='')error += 'Не заполнен email\n';
    if(text=='')error += 'Не заполнен текст сообщения\n';
   
    if(error.length!=0){
        alert(error)
    }
    else{
        $("#message_help").html('');
        $("#message_help").css("display", "none");
        $(".popup_window_inner_field").css("display", "block");
        
        $.ajax({
            url: '/modules/ajax/user-backend.php',
            type: "POST",
            data: "mode=help&name="+name+"&email="+email+"&text="+text,
            success: function(jsondata){
                if(jsondata.status == 1){
                    
                    $(".popup_window_inner_field").css("display", "none");
                    $("#message_help").html('Ваше сообщение отправлено.<br>Наши менеджеры свяжутся с Вами в ближайшее время*.<br><br>* Запросы, присланные после 18.00, будут обработаны на следующий день.');
                    $("#message_help").css("display", "block");
                    
                    $('#popup_help_name').val('');
                    $('#popup_help_email').val('');
                    $('#popup_help_text').val('');

                    
                    setTimeout('stop_help()', 4000);
                }
                else{
                    $("#message_email").html('Произошла ошибка. Проверьте правильность введенных данных');
                    $("#message_email").css("display", "block");
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
            },
            dataType: 'json'
        });
    }    
}

function start_mail(){
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);
    
    $(".popup_window_inner_field").css("display", "block");
    $("#message_mail").css("display", "none");
    
    var height = $('#popup_mail').height();
    var width = $('#popup_mail').width();
    var width = 393;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_mail").animate({opacity: "1"}, 300);
    $('#popup_mail').css({display: "block"});
    $('#popup_mail').css("margin", "-"+height+"px 0 0 -"+width+"px");
    return false;
}

function stop_mail(){
    //$("#message_new_question").html('');
    $('#popup_mail_email').val('');
    $('#popup_mail_name').val('');
    $('#popup_mail_text').val('');
    
    $("#popup_mail").animate({opacity: "0"}, 300);
    $('#popup_mail').css({display: "none"});
    hidePopupBg();
    return false;
}

function ProcessMail(){
    /* получаем данные из формы */
    var now_send_mail = false;
    var name = $('#popup_mail_name').val();
    var email = $('#popup_mail_email').val();
    var text = $('#popup_mail_text').val();
    var agree = $('#popup_mail_agree').prop('checked');
    var error = '';
    if(name=='')error += 'Не заполнено имя\n';
    if(email=='')error += 'Не заполнен email\n';
    if(text=='')error += 'Не заполнен текст сообщения\n';
    if(!agree)error += 'Необходимо дать согласие на обработку персональных данных\n';
    
    
    if(error.length!=0){
        alert(error)
    }
    else if(!now_send_mail){
        // Скрыть кнопку, показать "подождите"
        $('#popup_mail_submit').hide();
        $('#please_wait_on_sending_mail_us').show();
        
        now_send_mail = true;
        $("#message_email").html('');
        $("#message_email").css("display", "none");
        $(".popup_window_inner_field").css("display", "block");
        
        $.ajax({
            url: '/modules/ajax/user-backend.php',
            type: "POST",
            data: "mode=mail&name="+name+"&email="+email+"&text="+text+"&href="+location.href,
            success: function(jsondata){
                if(jsondata.status == 1){
                    
                    $(".popup_window_inner_field").css("display", "none");
                    $("#message_email").html('Ваше сообщение отправлено.<br>Наши менеджеры свяжутся с Вами в ближайшее время*.<br><br>* Запросы, присланные после 18.00, будут обработаны на следующий день. ');
                    $("#message_email").css("display", "block");
                    
                    $('#popup_mail_name').val('');
                    $('#popup_mail_email').val('');
                    $('#popup_mail_text').val('');

                    setTimeout('stop_mail()', 5000);
                    
                    // Скрыть "подождите"
                    $('#please_wait_on_sending_mail_us').hide();
                    
                    
                    yaCounter17972227.reachGoal('oksendnapishite');
                    
                }
                else{
                    $("#message_email").html('Произошла ошибка. Проверьте правильность введенных данных');
                    $("#message_email").css("display", "block");
                    
                    // Скрыть "подождите"
                    $('#please_wait_on_sending_mail_us').hide();
                    // показать кнопку снова
                    $('#popup_mail_submit').show();
                }
                //alert(jsondata.status);
            },
            complete: function(){
                //alert(jsondata.status);
                now_send_mail = false;
            },
            dataType: 'json'
        });
    }    
}

function start_login(){
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);
    
    $(".popup_window_inner_field").css("display", "block");
    $("#message_login").css("display", "none");
    
    var height = $('#popup_login').height();
    var width = $('#popup_login').width();
    var width = 293;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_login").animate({opacity: "1"}, 300);
    $('#popup_login').css({display: "block"});
    $('#popup_login').css("margin", "-"+height+"px 0 0 -"+width+"px");
    return false;
}

function loginSocial(from, href){
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=session_from&href="+from,
        success: function(jsondata){
            location.href=href;
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}

function stop_login(){
    //$("#message_new_question").html('');
    $('#popup_login_email').val('');
    $('#popup_login_password').val('');
    
    $("#popup_login").animate({opacity: "0"}, 300);
    $('#popup_login').css({display: "none"});
    hidePopupBg();
    return false;
}

function ProcessLogin(){
    /* получаем данные из формы */
    
    var email = $('#popup_login_email').val();
    var pass = $('#popup_login_password').val();
    
    if($('#login_remember').attr("checked")){var remember = 1;}else{var remember = 0;}
    
    $("#message_login").html('');
    $("#message_login").css("display", "none");
    $(".popup_window_inner_field").css("display", "block");
    
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=login&email="+email+"&pass="+pass+"&remember="+remember,
        success: function(jsondata){
            if(jsondata.status == 1){
                // сделать видимыми ссылки Избранное и Профиль
                $("#header_up_login_fav_link_profile, #header_up_login_fav_link_fav, #header_up_login_fav_link_exit").removeClass("display_none").addClass("display_block");
                // скрыть ссылку Логин/рег
                $("#header_up_login_fav_link_login").removeClass("display_block").addClass("display_none");
                
                $(".popup_window_inner_field").css("display", "none");
                $("#message_login").html('Вы успешно вошли в систему');
                $("#message_login").css("display", "block");
                
                $('#header_up_login_fav_link_profile').html(jsondata.i);
                
                var email = $('#popup_login_email').val('');
                var pass = $('#popup_login_password').val('');
                
                setTimeout('stop_login()', 2000);
            }
            else{
                $("#message_login").html('Произошла ошибка. Проверьте правильность введенных данных');
                $("#message_login").css("display", "block");
            }
            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}

function start_logout(){
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=logout",
        success: function(jsondata){
            if(jsondata.status == 1){
                // сделать НЕвидимыми ссылки Избранное и Профиль
                $("#header_up_login_fav_link_profile, #header_up_login_fav_link_exit").removeClass("display_block").addClass("display_none");
                // скрыть ссылку Логин/рег
                $("#header_up_login_fav_link_login").removeClass("display_none").addClass("display_block");

                location.reload();
            }
            else{
                alert("Произошла непридвиденная ошибка");
            }
            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}


function start_reg(){
    // скрыть форму логина
    $("#popup_login").animate({opacity: "0"}, 300);
    $('#popup_login').css({display: "none"});
    
    $('#popup_bg').css({display: "block"});
    $("#popup_bg").animate({opacity: "0.4"}, 100);
    
    // показать форму регистрации 
    
    $(".popup_window_inner_field").css("display", "block");
    $("#message_reg").css("display", "none");
    
    var height = $('#popup_reg').height();
    var width = $('#popup_reg').width();
    var width = 293;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_reg").animate({opacity: "1"}, 300);
    $('#popup_reg').css({display: "block"});
    $('#popup_reg').css("margin", "-"+height+"px 0 0 -"+width+"px");

    return false;
}

function stop_reg(){
    //$("#message_new_question").html('');
    $("#popup_reg").animate({opacity: "0"}, 300);
    $('#popup_reg').css({display: "none"});
    hidePopupBg();
    return false;
}

function ProcessReg(){
    /* получаем данные из формы */
    $("#message_reg").css("display", "none");
    $("#message_reg").html('');
    
    var f = $("#popup_reg_surname").val();
    var i = $("#popup_reg_name").val();
    var o = $("#popup_reg_name2").val();
    
    var email = $("#popup_reg_email").val();
    var pass = $("#popup_reg_password").val();
    
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=reg&f="+f+"&i="+i+"&o="+o+"&email="+email+"&pass="+pass,
        success: function(jsondata){
            if(jsondata.status == 1){
                $("#message_reg").css("display", "block");
                $("#message_reg").html('Вы успешно зарегистрировались, инструкции по активации аккаунта отправлены вам на e-mail');
                
                $("#popup_reg_surname").val('');
                $("#popup_reg_name").val('');
                $("#popup_reg_name2").val('');
                
                $("#popup_reg_email").val('');
                $("#popup_reg_password").val('');
                
                setTimeout('stop_reg()', 4000);
                
            }
            else{
                var text_error = '';
                
                if(jsondata.status_f == 2){
                    text_error = text_error+'Поле "Фамилия" должно быть заполнено\n';
                }
                if(jsondata.status_i == 2){
                    text_error = text_error+'Поле "Имя" должно быть заполнено\n';
                }
                if(jsondata.status_pass == 2){
                    text_error = text_error+'Пароль не может быть пустым\n';
                }
                if(jsondata.status_email == 2){
                    text_error = text_error+'E-mail не заполнен\n';
                }
                if(jsondata.status_email == 3){
                    text_error = text_error+'E-mail введен некорректно\n';
                }
                if(jsondata.status_email == 4){
                    text_error = text_error+'E-mail уже зарегистрирован\n';
                }
                
                alert(text_error);
                
            }
            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}

function start_recover(){
    // скрыть форму логина
    $("#popup_login").animate({opacity: "0"}, 300);
    $('#popup_login').css({display: "none"});
    
    // показать форму регистрации 
    
    $(".popup_window_inner_field").css("display", "block");
    $("#message_rec").css("display", "none");
    
    var height = $('#popup_rec').height();
    var width = $('#popup_rec').width();
    var width = 320;
    
    // половина высоты и ширины
    height = height/2>>0;
    height += 40;
    width = width/2>>0;
    width += 40;
    
    
    $("#popup_rec").animate({opacity: "1"}, 300);
    $('#popup_rec').css({display: "block"});
    $('#popup_rec').css("margin", "-"+height+"px 0 0 -"+width+"px");

    return false;
}

function stop_rec(){
    $("#message_recovery").html('');
    $("#popup_rec").animate({opacity: "0"}, 300);
    $('#popup_rec').css({display: "none"});
    hidePopupBg();
    return false;
}




function FinishLoginRegQuestion(){
    $("#bg_shadow").animate({opacity: "0"}, 300);
    $("#bg_shadow").queue(function () {
        $(this).hide();
        $(this).dequeue();
    });
    
    $('#login_window').css({display: "none"});
    $("#login_window").animate({opacity: "0"}, 300);
    
    $('#reg_window').css({display: "none"});
    $("#reg_window").animate({opacity: "0"}, 300);
    
    $('#recover_window').css({display: "none"});
    $("#recover_window").animate({opacity: "0"}, 300);
    $('#recover_input_table').show();
    $("#message_recover").html('');
    
    $('#new_question').css({display: "none"});
    $("#new_question").animate({opacity: "0"}, 300);

    return false;    
}




function ProcessRecover(){
    var email = $("#popup_rec_email").val();
    $.ajax({
        url: '/modules/ajax/user-backend.php',
        type: "POST",
        data: "mode=recover&email="+email,
        success: function(jsondata){
            if(jsondata.status == 1){
                $("#message_recovery").html('Пароль был выслан на ваш адрес электронной почты');
                setTimeout('stop_rec()', 2000);
            }
            else if(jsondata.status == 2){
                $("#message_recovery").html('Произошла ошибка при отправке письма, повторите попытку позднее');
                setTimeout('stop_rec()', 5000);
            }
            else if(jsondata.status == 0){
                $("#message_recovery").html('Указанный вами адрес электронной почты не найден');
            }
            //alert(jsondata.status);
        },
        complete: function(){
            //alert(jsondata.status);
        },
        dataType: 'json'
    });
}


