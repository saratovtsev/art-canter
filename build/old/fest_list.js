var type_list;
var type_list_val = -1;
var order_by_date;
var filter_show;        // all, go, close
var count_all = -1;
var count_showed;
var array_events=[];
var the_end;

$(document).ready(function(){
    if($("#is_page_event_list").length>0){
        var myHash = location.hash;
        if(myHash.length>0 && myHash.substr(1,4)=='list' ){
            myHash=myHash.substr(5,myHash.length-1);
            if(myHash.length>0){
                myHash = myHash.split('&');
                // сортировка по полученный параметрам
                for(var i=0; i<myHash.length; i++){
                    hash_item = myHash[i].split('=');
                    if(hash_item[0]=='type_list') type_list = hash_item[1]
                    if(hash_item[0]=='type_list_val') type_list_val = hash_item[1]
                    if(hash_item[0]=='order_by_date') order_by_date = hash_item[1]
                    if(hash_item[0]=='filter_show') filter_show = hash_item[1]
                    if(hash_item[0]=='count_showed') count_showed = hash_item[1]
                }

                if(order_by_date == 'ASC'){
                    $(".order_filter_inner_date").removeClass('disable').removeClass('desc').addClass('asc');
                }
                else{
                    console.log(order_by_date);
                    $(".order_filter_inner_date").removeClass('disable').removeClass('asc').addClass('desc');
                }
                reviewListNewParams('hash')
            }

        }
        else{
            //console.log(myHash);
            type_list = $("#type_list").val();
            type_list_val = $("#type_list_val").val();
            order_by_date = 'ASC';
            if($('#type_list').val() == 'sel'){
                filter_show = 'all';
            }
            else{
                filter_show = 'go';
            }

            count_showed = parseInt($("#count_showed_events").val());

            // после загрузки страницы получить id всех фестивалей
            getListAll(0)
        }

        $('#select-country').selectize();
        $('#select-city').selectize();

        // слапйдер цен
        var max_price = parseInt($('#max_price_real').val());
        var slider_price = document.getElementById('slider_price_mini');
        noUiSlider.create(slider_price, {
            start: [0, max_price],
            step: 10,
            margin: 2,
            connect: true,
            direction: 'ltr',
            orientation: 'horizontal',
            range: {
                'min': 0,
                'max': max_price
            },
            format: wNumb({
                decimals: 0
            })
        });

        var price1_mini = document.getElementById('minCost'),
            price2_mini = document.getElementById('maxCost');


        slider_price.noUiSlider.on('update', function(values, handle){
            if (handle){
                price2_mini.value = values[handle];
            }else{
                price1_mini.value = values[handle];
            }
        });

        // When the input changes, set the slider value
        price1_mini.addEventListener('change', function(){
            slider_price.noUiSlider.set([null, this.value]);
        });
        price2_mini.addEventListener('change', function(){
            slider_price.noUiSlider.set([null, this.value]);
        });

        // слапйдер возраста
        var slider_age = document.getElementById('slider_age_mini');
        noUiSlider.create(slider_age, {
            start: [0, 100],
            step: 1,
            margin: 2,
            connect: true,
            direction: 'ltr',
            orientation: 'horizontal',
            range: {
                'min': 0,
                'max': 100
            },
            format: wNumb({
                decimals: 0
            })
        });

        var age1_mini = document.getElementById('minAge'),
            age2_mini = document.getElementById('maxAge');


        slider_age.noUiSlider.on('update', function(values, handle){
            if (handle){
                age2_mini.value = values[handle];
            }else{
                age1_mini.value = values[handle];
            }
        });

        // When the input changes, set the slider value
        age1_mini.addEventListener('change', function(){
            slider_age.noUiSlider.set([null, this.value]);
        });
        age2_mini.addEventListener('change', function(){
            slider_age.noUiSlider.set([null, this.value]);
        });



    }

    $(".form_filter_time").on("click", ".time_filter", function (event) {
        filter_show = event.target.id

        if(filter_show=='go'){
            $(".go").prop("checked", "checked");
        }
        if(filter_show=='close'){
            $(".close").prop("checked", "checked");
        }
        if(filter_show=='all'){
            $(".all").prop("checked", "checked");
        }

        reviewListNewParams('filter');
    });
});

function getListAll(count_showed){
    console.log('getListAll');
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: "mode=get_list_all&type_list="+type_list+"&type_list_val="+type_list_val+"&order_by_date="+order_by_date+"&filter_show="+filter_show+"&count_showed="+count_showed,
        success: function(jsondata){
            if(jsondata.status==1){
                for(var i=0; i<jsondata.res.length; i++){
                    array_events.push(jsondata.res[i])
                }
                //console.log(array_events);
                if(jsondata.res.length<11){
                    $("#show_more_results").hide();
                }
                else{
                    $("#show_more_results").show();
                }
            }
        },
        complete: function(){
            //alert(jsondata.status);
        },
        async: false,
        dataType: 'json'
    });
}

function reviewListNewParams(mode){
    //console.log('reviewListNewParams');
    if(mode=='date'){
        if(order_by_date=='ASC'){
            order_by_date = 'DESC';
            $(".order_filter_inner_date").removeClass('disable').removeClass('asc').addClass('desc');
            setHashValue('order_by_date', 'DESC');
        }
        else{
            order_by_date = 'ASC';
            $(".order_filter_inner_date").removeClass('disable').removeClass('desc').addClass('asc');
            setHashValue('order_by_date', 'ASC')
        }
    }
    else if(mode=='filter'){
        setHashValue('filter_show', filter_show);
    }
    else if(mode=='hash'){
        if(order_by_date=='ASC'){
            $(".order_filter_inner_date").removeClass('disable').removeClass('desc').addClass('asc');
        }
        else{
            $(".order_filter_inner_date").removeClass('disable').removeClass('asc').addClass('desc');
        }
        if(filter_show=='all'){
            $("input.all[name=time_filter]:radio").attr('checked', 'checked');
        }
        else if(filter_show=='close'){
            $("input.close[name=time_filter]:radio").attr('checked', 'checked');
        }
    }


    // Вставить в location.hash новые параметры
    location.hash = 'list&type_list='+type_list+'&type_list_val='+type_list_val+'&order_by_date='+order_by_date+'&filter_show='+filter_show+'&count_showed='+count_showed;

    count_showed = 0;
    array_events=[];


    //console.log("Показано "+count_showed)
    $("#show_more_results").css("display", "none");
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: "mode=get_new_10&type_list="+type_list+"&type_list_val="+type_list_val+"&order_by_date="+order_by_date+"&filter_show="+filter_show+"&count_showed="+count_showed,
        beforeSend: function(){
            startLoader();
        },
        success: function(jsondata){
            if(jsondata.status==1){
                //console.log(jsondata.res);
                if(jsondata.count > 0){
                    $('#list_fest').html(jsondata.res);
                    count_showed += parseInt(jsondata.count);


                    //console.log(showed_key);
                    if(parseInt(jsondata.count)<10){
                        $("#show_more_results").css("display", "none");
                    }
                }
                else{
                    $('#list_fest').html('<br><div style="width: 100%; text-align: center; font-size: 16px;">Результатов по данному запросу не найдено</div>');
                    $("#show_more_results").css("display", "none");
                }
            }
            else{
                alert("Ошибка сервера");
            }
        },
        complete: function(){
            //alert(jsondata.status);
            $("#show_more_results span").html('Показать еще 10 <img class="treug" src="/d/i/show_more_treug.png">');
            $("#show_more_results").show();
            reCallTooltip();
            stopLoader();
            getListAll(0);

            makeDotDotDotAndFav();
        },
        async: true,
        dataType: 'json'
    });
}


function showMoreEventsList(){
    //console.log('showMoreEventsList');
    console.log("Показано "+count_showed)
    $("#show_more_results").show();
    $("#show_more_results span").html('<img class="loader" src="/d/i/294.GIF">');

    var tmp_arr=[];
    for(var i=count_showed; i<count_showed+10; i++){
        //console.log(tmp_arr);
        if(typeof array_events[i] != "undefined"){
            tmp_arr.push(array_events[i])
        }
        else{
            the_end = true;
        }

    }
    //console.log(tmp_arr);
    $.ajax({
        url: '/modules/ajax/fest_list.php',
        type: "POST",
        data: "mode=get_arr_events&arr="+tmp_arr,
        success: function(jsondata){
            if(jsondata.status==1){
                //console.log(jsondata.res);
                $('#list_fest').append(jsondata.res);
                makeDotDotDotAndFav();
                count_showed += parseInt(jsondata.count);

                //console.log(showed_key);
                if(parseInt(jsondata.count)<10){
                    $("#show_more_results").css("display", "none");
                }
            }
            else{
                $('#list_fest').append('<br><div style="width: 100%; text-align: center; font-size: 16px;">Результатов по данному запросу больше не найдено</div>');
                $("#show_more_results").css("display", "none");
            }
        },
        complete: function(){
            //alert(jsondata.status);
            $("#show_more_results").show();
            if(tmp_arr.length < 10){
                $("#show_more_results").css("display", "none");
            }
            else{
                $("#show_more_results span").html('Показать еще 10 <img class="treug" src="/d/i/show_more_treug.png">');
            }

            reCallTooltip();
        },
        async: true,
        dataType: 'json'
    });

}
