<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Festivals find page</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.5,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,300italic,400,400italic,500,500italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/build/main.css?time=<?= date('Y-m-d\Th:i:s'); ?>">
    <link rel="stylesheet" href="/build/old/kalendae.css">
    <link rel="stylesheet" href="/build/old/toggles-iphone.css">
    <link rel="stylesheet" href="/build/old/toggles.css">
    <link rel="stylesheet" href="/build/old/nouislider.css">
    <link rel="stylesheet" href="/node_modules/selectize/dist/css/selectize.default.css">

    <script src="/node_modules/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="/node_modules/jquery-migrate/dist/jquery-migrate.min.js" charset="utf-8"></script>
    <script src="/node_modules/kalendae/build/kalendae.standalone.min.js" charset="utf-8"></script>
    <script src="/node_modules/selectize/dist/js/standalone/selectize.min.js" charset="utf-8"></script>
    <script src="/node_modules/selectize/dist/js/standalone/selectize.min.js" charset="utf-8"></script>

  </head>
  <body class="page">

    <?php require_once 'blocks/_begin.php'; ?>

    <div id="header" class="header">
        <?php require_once 'blocks/_header.php'; ?>
        <div class="clear"></div>

        <div class="header_search header-search">
            <div class="i_need_help header-search__help" onclick="start_help(); yaCounter17972227.reachGoal('presshelp');">Подобрать Вам фестиваль/конкурс?</div>
            <div id="header_search_line_input" class="header-search__form">
                    <input onclick="yaCounter17972227.reachGoal('search');" type="submit" class="main_menu_search header-search__submit" value="Найти">
                    <input type="text" class="main_search_input header-search__input" id="main_search_input_nofixed" name="text" placeholder="Введите ключевые слова"
                    <?php
                        if(!in_array($info['id'], array(5))){
                            echo 'ng-model="search_input_text"';
                        }
                        ?>
                    >
            </div>
            <div class="header-search__details" onclick="scrollToFormAndShow();"><span>Перейти к расширенному подбору по параметрам</span></div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="content">
      <div id="bg_content"></div>

      <div id="before_order_filter"></div>
      <div class="order_filter" class="fixed_warning_nofixed">
        <a id="order_filter_inner_date"></a>
        <div id="order_filter_inner">

            <div id="filtres_all">
              <div class="param_search_to_delete_block"></div>
              <div id="filter_time_block">
                <form class="form_filter_search_time">
                  <label><input type="radio" name="time_filter" class="time_filter_search go" id="go" value="1"> идет прием заявок</label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="time_filter" class="time_filter_search close" id="close" value="2"> прием заявок окончен</label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="time_filter" class="time_filter_search all" id="all" value="3" checked> все</label>
                </form>
              </div>
              <div class="param_search_to_delete_block_ext"></div>
            </div>

        </div>
        <div style="clear: both;"></div>

      </div>

      <div style="clear: both;"></div>

      <input id="is_search_results" type="hidden">
      <input id="curs_eur" value="70" type="hidden">
      <input id="curs_usd" value="62" type="hidden">
      <input id="map_bounds" value="" type="hidden">
      <input id="map_bounds_new" value="" type="hidden">
      <input id="map_bounds_for_search" value="" type="hidden">
      <input id="map_bounds_tmp" value="" type="hidden">

      <br><br>

      <div id="list_fest_header" class="list_fest_header_mini">
        <div class="list_fest_header_title">Фестивали</div>
        <div class="list_fest_header_count"></div>
        <div class="list_map_switch">Показать результаты <span id="list">списком<img src="/d/i/list2.png"></span><span id="map">на карте<img src="/d/i/map_icon_yellow.png" ></span></div>
      </div>

      <div id="map_search" style="display: none">
        <div id="map_search_event_info">
            <a id="map_search_event_info_back" onclick="backToResultsOnMapFromEventsInfo()">Назад к результатам поиска</a>
            <div id="map_search_event_info_content">
            </div>
        </div>

        <div id="map_search_map" style="display: block"></div>
        <div id="map_search_list" style="display: block"></div>
      </div>

      <?php require_once 'blocks/_form-mini.php'; ?>

      <div id="list_fest" class="list_fest_mini">
        <div class="fest_list_item_mini ">
          <a onclick="yaCounter17972227.reachGoal('details');" class="img_link_to_fest" target="_blank" title="Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур 2017»" href="/events/internet-belii-kit-2017-1/">
              <img src="//art-center.ru/upload/e/4524/main_fbf6a64edb6dc9d718a38f0cd472398b.jpg" alt="Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур 2017»">
          </a>
          <?php $helperFest = "
          <div class=&quot;event-manager&quot;>
            Возник вопрос? Напишите нам и наш менеджер ответит вам на email
            <br><br>
            <div class=&quot;event-manager__name&quot;>
              <b><i>Горбунова Наталья</i></b>
            </div>
            <div class=&quot;event-manager__question&quot;>
              <a style=&quot;text-decoration: none !important;&quot; class=&quot;event-manager__make_question&quot; onclick=&quot;start_question(4524)&quot;>Задать вопрос</a>
            </div>
            <div class=&quot;event-manager__phone&quot;>+7-925-642-36-56</div>
            <div class=&quot;event-manager__email&quot;>
              <a class=&quot;event-manager__link&quot; href=&quot;mailto:nataly_g@art-center.ru?subject=Вопрос с сайта: Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур 2017»&quot;>nataly_g@art-center.ru</a>
            </div>
            <div class=&quot;event-manager__skype&quot;>gorbunova-nataly-art</div>
            <div class=&quot;event-manager__vk&quot;>
              <a class=&quot;event-manager__link&quot; href=&quot;http://vk.com/id311817282&quot;>Страница менеджера Вконтакте</a>
            </div>
            <div class=&quot;event-manager__ok&quot;>
              <a class=&quot;event-manager__link&quot; href=&quot;http://ok.ru/profile/573436785089&quot;>Страница менеджера в Одноклассниках</a>
            </div>
            <div class=&quot;event-manager__fb&quot;>
              <a class=&quot;event-manager__link&quot; href=&quot;https://www.facebook.com/profile.php?id=100006377442694&amp;fref=ts&quot;>Страница менеджера в Facebook</a>
            </div>
          </div>
          " ?>
          <div class="fest_info">
              <div onclick="start_question(4524)" class="fest_list_item_content_manager fest-item__manager" style="background: url(//art-center.ru/upload/managers/13/mini/b00148e1c507d652e1e4a35bdcd442bb.jpg); background-size: cover;">
                  <a data-title="<?= $helperFest ?>" data-placement="bottom" data-html="true" data-container="#list_fest" style="cursor: pointer;"></a>
              </div>

              <div class="fest_list_item_content_name" style="">
                <div class="dotdotdot" style="height: auto; width: auto;">
                  <a onclick="yaCounter17972227.reachGoal('details');" target="_blank" title="Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур 2017»" href="/events/internet-belii-kit-2017-1/">Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур... </a>
                  <img title="Победитель конкурса &quot;Синергия Успеха&quot; на лучшего организатора культурного мероприятия 2015 года." class="cup_img" src="/d/i/cup.png">
                  <img title="Добавить в избранное" onclick="addfav(4834)" class="fav_btn fav_btn_4834" src="/d/i/fav.png" alt="">
                </div>
              </div>
              <div class="fest_list_item_content_name_normal" style="display: none;">
                  <a target="_blank" title="Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур 2017»" href="/events/internet-belii-kit-2017-1/" isset="true">Международный детско-юношеский многожанровый дистанционный интернет-конкурс «Белый КиТ. Отборочный Тур 2017»</a>
                  <img title="Добавить в избранное" onclick="addfav(4524)" class="fav_btn fav_btn_4524" src="/d/i/fav.png" alt="">
              </div>
              <div class="fest_list_item_content_locations">
                  Россия, <span class="fest_list_item_content_locations_town">Москва</span><img src="/d/i/map_icon_red.png" class="fest_list_item_content_locations_icon" onclick="showMapFest('55.838189, 37.705279');">
              </div>
              <div class="fest_list_item_content_categories fest-item__category">
                  <a data-title="Для участия приглашаются отдельные исполнители, ансамбли и оркестры. В рамках данной категории проводятся конкурсы для юных музыкантов, фестивали и конкурсы для духовых, симфонических и оркестров народных инструментов, конкурсы для профессиональных исполнителей и любителей." data-placement="right" style="cursor: pointer;">Инструментальные</a>,
                  <a data-title="Для участия приглашаются танцоры и хореографические коллективы различных направлений хореографии. В данной категории проводятся хореографические фестивали и конкурсы, конкурсы балета, конкурсы современной и народной хореографии, мастер-классы для танцоров и хореографов, хореографические лагеря и семинары для педагогов." data-placement="bottom" style="cursor: pointer;">Хореографические</a>,
                  <a data-title="Для участия приглашаются вокалисты и вокальные ансамбли эстрадного, академического и народного вокала. В рамках данной категории проводятся конкурсы эстрадного и академического вокала, конкурсы вокально-инструментальных ансамблей, конкурсы эстрадной песни, конкурсы народного пения и другие." data-placement="bottom" style="cursor: pointer;">Вокальные</a>,
                  <a data-title="Для участия приглашаются хоры всех категорий и вокальные ансамбли. В рамках данного жанра проводятся международные и всероссийские хоровые фестивали и конкурсы, конкурсы вокальных ансамблей, конкурсы хормейстеров и конкурсы для дирижеров хоров." data-placement="bottom" style="cursor: pointer;">Хоровые</a>,
                  <a data-title="Для участия приглашаются солисты и коллективы всех направлений творчества. В рамках данной категории проводятся хореографические и вокальные фестивали, фестивали для театров, конкурс театрального творчества, конкурс театров мод, конкурс чтецов и исполнителей авторской песни, конкурсы художников и дизайнеров, конкурсы красоты." data-placement="bottom" style="cursor: pointer;">Другие</a>
              </div>
              <div class="fest_list_item_content_dates_price">
                15 января — 01 ноября 2017 г.
                <span>
                  <a data-title="Солист - 1300 руб/чел.<br />
                    Дуэт - 2500 руб.<br />
                    Трио - 3300 руб.<br />
                    Для коллективов от 4 до 20 человек - 500 руб/чел.<br />
                    Для коллективов от 21 человека  - 10000 руб/заявка<br />
                    Номинации ИЗО, ДПИ, Фотография  – 350 руб/1 работа<br />
                    Номинации Кино и Мультипликация – 600 руб/1 фильм/мультфильм" data-placement="auto bottom" data-container="#list_fest" data-html="true" style="cursor: pointer;">350</a>
                  <img src="/d/i/rur_black.png">
                </span>
              </div>

              <div class="fest_list_item_content_buttons" st_processed="yes">
                  <a onclick="yaCounter17972227.reachGoal('details');" target="_blank" href="/events/internet-belii-kit-2017-1/" class="button_grey_mini_fest_list" isset="true"><div>Подробнее</div></a>
                  <a onclick="yaCounter17972227.reachGoal('send');" target="_blank" href="/order/?id=4524" class="button_green_mini_fest_list left" isset="true"><div>Оставить заявку</div></a>
              </div>

          </div>
          <div style="clear: both"></div>
      </div>
    </div>
    <div class="clear"></div>

    <div id="show_more_results" class="show_more_results_mini" onclick="showMoreEventsSearch()" style="display: block;">
      <span>Показать еще 10 <img class="treug" src="/d/i/show_more_treug.png"></span>
    </div>
    <div id="list_results" style="display: block;">
    	<h3>Найдено в статьях:</h3>
    	<br>
    	<div class="main_search_result_item_title">1.&nbsp;<a class="main_search_result_item_title_a" href="/about/37/alena_berezko/">Алена Берёзко</a></div>
    	<div class="main_search_result_item_text">... &nbsp; Алёна Берёзко&nbsp;(Ракша) Россия, г. Пермь&nbsp;&nbsp; &nbsp; -&nbsp;судья международной категории IDF, официальный судья Кубков Европы (г. <strong>Москва)</strong> и Чемпоната мира (г. Надьканиджа, Венгрия) по современным направлениям хореографии, The &nbsp; &nbsp; &nbsp;International dance festival in Seoul  ...</div>
    	<hr>
    </div>

    <div style="padding-left: 40px;">Поделиться</div>

    </div>

    <div class="clear"></div>
    <br>
    <div id="block_not_found">Результатов по данному запросу не найдено. <br>Попробуйте задать более широкие параметры поиска: увеличить интервал дат, стоимости, <br>расширить географический диапазон и т.д.<br><br>Возможно, фестиваля, который Вы ищете, нет среди активных. Попробуйте поискать среди прошедших фестивалей (вкладка "все" над результатами поиска)</div>

    <div style="clear:both"></div>
  </div>

    <?php require_once 'blocks/_footer.php'; ?>

    <script src="/build/bundle.js?time=<?= date('Y-m-d\Th:i:s'); ?>" charset="utf-8"></script>
    <script src="/build/old/fest.js?time=<?= date('Y-m-d h:i:s'); ?>" charset="utf-8"></script>
    <script src="/build/old/users.js" charset="utf-8"></script>
    <script src="/build/old/main.js" charset="utf-8" defer async></script>
    <script src="/build/old/main_page.js" charset="utf-8"></script>

  </body>
</html>
