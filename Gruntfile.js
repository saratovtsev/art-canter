module.exports = function(grunt) {
	grunt.config.init({
		// define
		pkg: grunt.file.readJSON('package.json'),
		path_frontend: 'less/',
		path_build_css: 'build/',
		// tasks
		less: {
			mainDev: {
				options: {
					compress: false,
				},
				files: {
					'<%= path_build_css %>main.css': [
						'<%= path_frontend %>main.less',
						'<%= path_frontend %>plugins/**/*.less',
					]
				},
			},
			mainProd: {
				options: {
					compress: true,
				},
        files: {
					'<%= path_build_css %>main.css': [
						'<%= path_frontend %>main.less',
						'<%= path_frontend %>plugins/**/*.less',
					]
				},
			},
		},
		autoprefixer: {
			mainDev: {
				src: '<%= path_build_css %>main.css',
				dest: '<%= path_build_css %>main.css',
			},
			mainProd: {
				src: '<%= path_build_css %>main.min.css',
				dest: '<%= path_build_css %>main.min.css',
			},
		},
		concat: {
			options: {
				separator: ';',
			},
			main: {
				src: [
					'node_modules/jquery-fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
			    'node_modules/jquery-fancybox/source/js/jquery.fancybox.pack.js',
			    'node_modules/jquery-fancybox/source/js/jquery.fancybox.js',
			    'node_modules/jquery.cookie/jquery.cookie.js',
					'node_modules/jquery-dotdotdot/src/jquery.dotdotdot.js',
					'node_modules/nouislider/distribute/nouislider.js',
					'node_modules/wnumb/wNumb.js',
					'node_modules/bootstrap/js/tooltip.js',
					'js/**/*.js',
					'node_modules/zeroclipboard/dist/ZeroClipboard.min.js',
				],
				dest: '<%= path_build_css %>bundle.js',
			},
		},
		watch: {
			options: {
				atBegin: true,
				livereload: true
			},
			less: {
				files: '<%= path_frontend %>**/*.less',
				tasks: ['less:mainDev', 'autoprefixer:mainDev'],
				options: {
					spawn: false,
				},
			},
			php: {
				files: '**/*.php',
				tasks: '',
				options: {
					spawn: false,
				},
			},
			js: {
				files: 'js/**/*.js',
				tasks: 'concat',
				options: {
					spawn: false,
				},
			}
		},
	});
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.registerTask('build', ['less:mainProd', 'autoprefixer:mainProd']);
};
