<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Main page</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.5,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,300italic,400,400italic,500,500italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/build/main.css?time=<?= date('Y-m-d h:i:s'); ?>">
    <link rel="stylesheet" href="/build/old/kalendae.css">
    <link rel="stylesheet" href="/build/old/toggles-iphone.css">
    <link rel="stylesheet" href="/build/old/toggles.css">
    <link rel="stylesheet" href="/build/old/nouislider.css">
    <link rel="stylesheet" href="/node_modules/selectize/dist/css/selectize.default.css">

    <script src="/node_modules/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="/node_modules/jquery-migrate/dist/jquery-migrate.min.js" charset="utf-8"></script>
    <script src="/node_modules/kalendae/build/kalendae.standalone.min.js" charset="utf-8"></script>
    <script src="/node_modules/selectize/dist/js/standalone/selectize.min.js" charset="utf-8"></script>
    <script src="/node_modules/selectize/dist/js/standalone/selectize.min.js" charset="utf-8"></script>

  </head>
  <body class="page">
    <div id="blurred_image_container">
        <div class="img-src blur_block_bottom"></div>
        <div class="img-src blur_block_top" style="opacity: 0"></div>
    </div>

    <div id="page_bg"></div>


    <div id="popup_bg"></div>
    <div id="loader"><img alt="" src="/d/i/482.GIF"></div>

    <div id="popup_vopros" class="popup_window ng-scope" ng-controller="festQuestion">
      <div class="popup_window_inner">
        <div id="popup_vopros_close" class="popup_window_inner_close"></div>
        <div id="popup_vopros_title" class="popup_window_inner_title">задать вопрос</div>
        <div class="popup_window_inner_annotation">
            Возник вопрос? Напишите нам, и наш менеджер ответит вам на email.
        </div>
        <div class="popup_window_inner_warning">
            Все поля обязательны для заполнения
        </div>
        <div class="popup_window_inner_warning">
            <label>
                <input type="checkbox" ng-model="agree" class="ng-pristine ng-untouched ng-valid">
                В соответствии с Федеральным законом от 27.07.2006 года № 152-ФЗ  «О персональных данных» , я даю свое письменное согласие на обработку  персональных данных  компанией ООО «МК «Арт-Центр Плюс»

                <br><br>
                Нажимая "Отправить", я соглашаюсь с <a href="/about/privacy-policy/" isset="true">политикой конфиденциальности</a> компании ООО «МК «Арт-Центр Плюс».
            </label>
        </div>

        <div id="message_new_question">Вопрос успешно отправлен, наши менеджеры свяжется с Вами в ближайшее время</div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">Как к вам обращаться</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text ng-pristine ng-untouched ng-valid" id="popup_vopros_name" ng-model="name">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">email</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text ng-pristine ng-untouched ng-valid" id="popup_vopros_email" ng-model="email_qstn">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">Телефон</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text ng-pristine ng-untouched ng-valid" id="popup_vopros_phone" ng-model="phone_qstn">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">Город</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text ng-pristine ng-untouched ng-valid" id="popup_vopros_city" ng-model="city_qstn">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">Номинация</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text ng-pristine ng-untouched ng-valid" id="popup_vopros_nomin" ng-model="nomin_qstn">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">задайте вопрос</div>
            <div class="popup_window_inner_field_inputtext">
                <textarea class="popup_input_textarea ng-pristine ng-untouched ng-valid" id="popup_vopros_text" ng-model="text_qstn"></textarea>
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title" style="text-transform: none">
                <label><input type="checkbox" name="want_reg" ng-init="want_reg_chkbx = true" ng-model="want_reg_chkbx" class="ng-pristine ng-untouched ng-valid"> Хочу зарегистрироваться на сайте</label>
            </div>
        </div>

        <div class="popup_window_inner_field">
            <a href="#" ng-click="sendData()" class="button_green_mini_popup" id="popup_vopros_submit" isset="true"><div>Отправить</div></a>
        </div>
    </div>
  </div>

  <div id="popup_mail" class="popup_window">
    <div class="popup_window_inner">
        <div onclick="stop_mail()" class="popup_window_inner_close"></div>
        <div class="popup_window_inner_title">напишите нам</div>
        <div class="popup_window_inner_warning">
            Все поля обязательны для заполнения
        </div>

        <div class="popup_window_inner_warning">
            <label>
                <input type="checkbox" checked="checked" id="popup_mail_agree">
                В соответствии с Федеральным законом от 27.07.2006 года № 152-ФЗ  «О персональных данных» , я даю свое письменное согласие на обработку  персональных данных  компанией ООО «МК «Арт-Центр Плюс»

                <br><br>
                Нажимая "Отправить", я соглашаюсь с <a href="/about/privacy-policy/" isset="true">политикой конфиденциальности</a> компании ООО «МК «Арт-Центр Плюс».
            </label>
        </div>

        <div id="message_email"></div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">имя</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_mail_name" value="">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">email</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_mail_email" value="">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">текст сообщения</div>
            <div class="popup_window_inner_field_inputtext">
                <textarea class="popup_input_textarea" id="popup_mail_text"></textarea>
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <a href="#" onclick="ProcessMail(); yaCounter17972227.reachGoal('sendnapishite'); return true;" class="button_green_mini_popup" id="popup_mail_submit" isset="true"><div>Отправить</div></a>
                <div id="please_wait_on_sending_mail_us" style="display: none;">Пожалуйста, подождите..</div>
            </div>
        </div>
    </div>
  </div>

  <div id="popup_help" class="popup_window">
    <div class="popup_window_inner">
        <div onclick="stop_help()" class="popup_window_inner_close"></div>
        <div class="popup_window_inner_title">Хотите, подберём<br>фестиваль для вас?</div>

        <div id="message_help"></div>

        <div class="popup_window_inner_warning">
            <label>
                <input type="checkbox" checked="checked" id="popup_help_agree">
                В соответствии с Федеральным законом от 27.07.2006 года № 152-ФЗ  «О персональных данных» , я даю свое письменное согласие на обработку  персональных данных  компанией ООО «МК «Арт-Центр Плюс»

                <br><br>
                Нажимая "Жду звонка", я соглашаюсь с <a href="/about/privacy-policy/" isset="true">политикой конфиденциальности</a> компании ООО «МК «Арт-Центр Плюс».
            </label>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_help_phone" value="" placeholder="Введите Ваш телефон">
            </div>
            <a href="#" onclick="ProcessHelpPhone(); return true;" class="button_green_mini_popup" id="popup_help_call_btn" isset="true"><div>Жду звонка!</div></a>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_help_viber" value="" placeholder="или Viber">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_help_whatsapp" value="" placeholder="или WhatsApp">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_help_name" value="" placeholder="Как к вам обращаться">
            </div>
        </div>

        <div style="clear: both"></div>

        <div class="popup_window_inner_big_field">
            <span style="color: #0ECB20; font-size: 16px">Или позвоните нам по телефонам</span><br>
            +7-926-777-32-47<br>
            +7-925-642-36-56 (дежурный менеджер)
        </div>

        <div class="popup_window_inner_field_note">
            * Запросы, присланные после 18.00, будут обработаны<br>на следующий день.
        </div>

    </div>
  </div>

  <div id="popup_login" class="popup_window">
    <div class="popup_window_inner">
        <div id="popup_login_close" class="popup_window_inner_close"></div>
        <div id="popup_login_title" class="popup_window_inner_title">вход</div>
        <div class="popup_window_inner_annotation">
            Если вы не являетесь пользователем Арт-Центр, вам необходимо <a href="#" onclick="start_reg();" isset="true">зарегистрироваться</a>
        </div>
        <div class="popup_window_inner_warning">
            Все поля обязательны для заполнения
        </div>

        <div id="message_login"></div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">email</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_login_email">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">пароль</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="password" class="popup_input_text" id="popup_login_password">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="checkbox" name="remember" id="login_remember" checked=""><label for="login_remember"><span></span>запомнить</label>
                <a href="#" id="popup_login_forget_password" onclick="start_recover();" isset="true">Забыли пароль?</a>
                <a href="#" onclick="ProcessLogin(); return false;" class="button_green_mini_popup" id="popup_login_submit" isset="true"><div>Вход</div></a>
                <br><br>
                Войти через
                <a href="/vk-auth.php" isset="true"></a>
                <a onclick="" isset="true"><img style="height: 25px; position: relative;top: 8px; cursor: pointer" src="/d/i/login_vk.png" alt="VK"></a>
                <a onclick="" isset="true"><img style="height: 25px; position: relative; top: 8px; cursor: pointer" src="/d/i/login_fb.png" alt="Facebook"></a>
            </div>
        </div>
    </div>
  </div>

  <div id="popup_reg" class="popup_window">
    <div class="popup_window_inner">
        <div id="popup_reg_close" class="popup_window_inner_close"></div>
        <div class="popup_window_inner_title">регистрация</div>

        <div class="popup_window_inner_warning">
            Все поля обязательны для заполнения
        </div>

        <div id="message_reg"></div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">фамилия</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_reg_surname" value="">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">имя</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_reg_name" value="">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">отчество</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_reg_name2" value="">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">email</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_reg_email" value="">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">пароль</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="password" class="popup_input_text" id="popup_reg_password" value="">
            </div>
        </div>

        <br><br>
        <div style="font-size: 13px; letter-spacing: -0.01em; color: #787878;">Нажимая "Регистрация", я соглашаюсь с <a href="#" isset="true">политикой конфиденциальности</a> компании ООО «МК «Арт-Центр Плюс».</div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <a href="#" onclick="ProcessReg(); return false;" class="button_green_mini_popup" id="popup_reg_submit" isset="true"><div>Регистрация</div></a>
            </div>
        </div>
    </div>
  </div>

  <div id="popup_rec" class="popup_window">
    <div class="popup_window_inner">
        <div id="popup_reс_close" class="popup_window_inner_close"></div>
        <div class="popup_window_inner_title">восстановление пароля</div>
        <div class="popup_window_inner_warning">
            Введите e-mail, который был указан при регистрации
        </div>

        <div id="message_recovery"></div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_title">email</div>
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_rec_email" value="">
            </div>
        </div>
        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <a href="#" onclick="ProcessRecover(); return false;" class="button_green_mini_popup" id="popup_rec_submit" isset="true"><div>Восстановить</div></a>
            </div>
        </div>
    </div>
  </div>

  <div id="popup_map" class="popup_window_map" style="width: 950px; height: 600px;">
    <div style="position: absolute; right: 0; top: -30px; height: 20px; width: 20px; border: 0px solid red; font-size: 24px; font-weight: bold; color: #fff; cursor: pointer;" onclick="hideMapFest()">X</div>
    <div class="popup_window_inner" id="map_fest">

    </div>
  </div>


  <div id="popup_order_error" class="popup_window">
    <div class="popup_window_inner">
        <div class="popup_window_inner_close" onclick="stop_order_error()"></div>
        <div class="popup_window_inner_title">ошибка при добавлении заявки</div>
        <div class="popup_window_inner_field" id="popup_order_error_text_block">
      </div>
    </div>
  </div>

  <div id="popup_is_parent_event" class="popup_window">
    <div class="popup_window_inner">
        <div class="popup_window_inner_close" onclick="stop_is_parent_event()"></div>
        <div id="popup_parent_title" class="popup_window_inner_title">Внимание!</div>
        <div class="popup_window_inner_field" id="popup_is_parent_text_block">
            Это страница прошлогоднего фестиваля.<br><br>Актуальная информация здесь:<br><span id="link_to_actual_event"></span>
        </div>
    </div>
  </div>

  <div id="popup_recall" class="popup_window">
    <div class="popup_window_inner">
        <div id="popup_reсall_close" onclick="stop_recall()" class="popup_window_inner_close"></div>
        <div id="popup_reсall_title" class="popup_window_inner_title">Заказ обратного звонка</div>

        <div id="message_recall"></div>

        <div class="popup_window_inner_warning">
            Нажимая "Жду звонка", я соглашаюсь с <a href="/about/privacy-policy/" isset="true">политикой конфиденциальности</a> компании ООО «МК «Арт-Центр Плюс».
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_recall_phone" value="" placeholder="Введите Ваш телефон">
            </div>
            <a href="#" onclick="ProcessRecall(); return true;" class="button_green_mini_popup" isset="true"><div>Жду звонка!</div></a>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_recall_viber" value="" placeholder="или Viber">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_recall_whatsapp" value="" placeholder="или WhatsApp">
            </div>
        </div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_recall_name" value="" placeholder="Введите Ваше имя">
            </div>
        </div>

        <div style="clear: both"></div>
    </div>
  </div>

  <div id="popup_sendme" class="popup_window">
    <div class="popup_window_inner">
        <div id="popup_sendme_close" onclick="stop_recall()" class="popup_window_inner_close"></div>
        <div id="popup_sendme_title" class="popup_window_inner_title">Отправить <span id="sendme_target"></span> на почту</div>

        <div id="message_sendme"></div>

        <div class="popup_window_inner_field">
            <div class="popup_window_inner_field_inputtext">
                <input type="text" class="popup_input_text" id="popup_sendme_mail" value="" placeholder="Введите Ваш E-mail">
            </div>
            <a href="#" onclick="ProcessSendme(); return true;" class="button_green_mini_popup" isset="true"><div>Отправить</div></a>
        </div>
        <div style="clear: both"></div>
    </div>
  </div>

  <div id="toup" class="hover_pointer" style="display: none;">Вверх</div>

        <div id="header" class="header">
            <?php require_once 'blocks/_header.php'; ?>
            <div class="clear"></div>

            <div class="header_search header-search">
                <div class="i_need_help header-search__help" onclick="start_help(); yaCounter17972227.reachGoal('presshelp');">Подобрать Вам фестиваль/конкурс?</div>
                <div id="header_search_line_input" class="header-search__form">
                        <input onclick="yaCounter17972227.reachGoal('search');" type="submit" class="main_menu_search header-search__submit" value="Найти">
                        <input type="text" class="main_search_input header-search__input" id="main_search_input_nofixed" name="text" placeholder="Введите ключевые слова"
                        <?php
                            if(!in_array($info['id'], array(5))){
                                echo 'ng-model="search_input_text"';
                            }
                            ?>
                        >
                </div>
                <div class="header-search__details" onclick="scrollToFormAndShow();"><span>Перейти к расширенному подбору по параметрам</span></div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="content">
            <div id="bg_content"></div>
            <div id="countries_block">
                <div class="form_search_content_item_country_town">
                    <div class="form_search_content_item_content">
                      <select id="select-location22" name="state22[]" multiple class="demo-default" placeholder="начните вводить название страны или города..">
                        <?php for ($i=0; $i < 46; $i++) {
                            echo "<option value=\"".$i."\">Страна-".$i."</option>";
                          } ?>
                      </select>

                      <script>
                      $('#select-location22').selectize({
                          maxItems: 5
                      });
                      </script>
                    </div>
                    <div id="searchforMapSubmit2" onclick="return 0;"></div>
                </div>
                <div id="countries_block_inner">

                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">А</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Abkhazia" isset="true">Абхазия</a>
                        <br>
                        <a href="/events/Austria" isset="true">Австрия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Б</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Belarus" isset="true">Беларусь</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">В</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/United Kingdom" isset="true">Великобритания</a>
                        <br>
                        <a href="/events/Hungary" isset="true">Венгрия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Г</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Germany" isset="true">Германия</a>
                        <br>
                        <a href="/events/Greece" isset="true">Греция</a>
                        <br>
                        <a href="/events/Georgia" isset="true">Грузия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">И</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Spain" isset="true">Испания</a>
                        <br>
                        <a href="/events/Italy" isset="true">Италия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">К</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Kazakhstan" isset="true">Казахстан</a>
                        <br>
                        <a href="/events/China" isset="true">Китай</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Л</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Latvia" isset="true">Латвия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Н</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Netherlands" isset="true">Нидерланды (Голландия)</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">О</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/UAE" isset="true">ОАЭ</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">П</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Poland" isset="true">Польша</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Р</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Russia" isset="true">Россия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">С</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/San Marino" isset="true">Сан-Марино</a>
                        <br>
                        <a href="/events/USA" isset="true">США</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">У</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Uzbekistan" isset="true">Узбекистан</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Ф</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Finland" isset="true">Финляндия</a>
                        <br>
                        <a href="/events/France" isset="true">Франция</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Ч</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Czech Republic" isset="true">Чехия</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Ш</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Switzerland" isset="true">Швейцария</a>
                        <br>
                        <a href="/events/Sweden" isset="true">Швеция</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Э</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Estonia" isset="true">Эстония</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Ю</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/South Korea" isset="true">Южная Корея</a>
                        <br>
                      </div>
                    </div>
                    <div class="countries_block_inner_item">
                      <div class="countries_block_inner_item_letter">Я</div>
                      <div class="countries_block_inner_item_list">
                        <a href="/events/Japan" isset="true">Япония</a>
                        <br>
                      </div>
                    </div>
                </div>
                <div style="clear: both"></div>
                <div id="countries_block_move">
                    <div id="countries_block_move_left" onclick="move_countries_right()"></div>
                    <div id="countries_block_move_right" onclick="move_countries_left()">К - П</div>
                </div>
            </div>

            <input type="hidden" id="page_main">
            <pre></pre>
            <div id="main_page_big_renab_block">
              <div class="main_page_big_renab" id="main_page_big_renab_0" style="background: url(&quot;https://www.art-center.ru/upload/renabs/3b0f1bd8dafa4ac72f4a9046779f9177.jpg&quot;); display: block;">
                <div class="main_page_big_renab_btn_left" onclick="rotationBBleft()"></div>
                <div class="main_page_big_renab_btn_right" onclick="rotationBBright()"></div>
                <div class="main_page_big_renab_info">
                  <span class="main_page_big_renab_info_name">Художественный конкурс «ArtCello» <br>в очной и заочной формах</span>
                  <br><span class="main_page_big_renab_info_place">&nbsp;Конкурс работ на роспись виолончели компании Yamaha <br> принять участие может любой желающий!&nbsp;</span>
                  <br><br><br>
                  <a href="events/artcello/" class="button_green2_transparent" style="" isset="true"><div>Ознакомиться с условиями конкурса и подать заявку</div></a>
                </div>
              </div>
              <div class="main_page_big_renab" id="main_page_big_renab_1" style="background: url(&quot;https://www.art-center.ru/upload/renabs/5bac81d1ff92a11df22ef96dfed21a51.jpg&quot;); display: none;">
                  <div class="main_page_big_renab_btn_left" onclick="rotationBBleft()"></div>
                  <div class="main_page_big_renab_btn_right" onclick="rotationBBright()"></div>
                  <div class="main_page_big_renab_info">
                      <span class="main_page_big_renab_info_name">Территория, где сбываются мечты!</span>
                      <br><span class="main_page_big_renab_info_place">&nbsp;Сделайте первый шаг на встречу мечте - <br> подайте заявку на один из фестивалей-конкурсов проекта «Салют Талантов»&nbsp;</span>
                      <br><br><br>
                      <a href="events/sel/sdelaite-pervii-shag-na-vstrechy-mechte/" class="button_green2_transparent" style="" isset="true"><div>Выбрать фестиваль</div></a>
                  </div>
              </div>
              <div class="main_page_big_renab" id="main_page_big_renab_2" style="background: url(&quot;https://www.art-center.ru/upload/renabs/6450688001087b22cc841e8d8cffc14f.jpg&quot;); display: none;">
                  <div class="main_page_big_renab_btn_left" onclick="rotationBBleft()"></div>
                  <div class="main_page_big_renab_btn_right" onclick="rotationBBright()"></div>
                  <div class="main_page_big_renab_info">
                      <span class="main_page_big_renab_info_name">Международный конкурс-фестиваль <br> имени Яна Сибелиуса в Финляндии <br> ноябрь 2017</span>
                      <br><span class="main_page_big_renab_info_place">&nbsp;Участники - вокалисты, хоры, оркестры, инструменталисты <br> Конкурс проводится при поддержке Комитета культуры города Турку<br>и Мэрии города Турку&nbsp;</span>

                      <br><br><br>
                      <a href="events/sel/konkyrs-imeni-yana-sibeliysa-v-finlyandii/" class="button_green2_transparent" style="" isset="true"><div>Ознакомиться с условиями конкурса и подать заявку</div></a>
                  </div>
              </div>
              <div class="main_page_big_renab" id="main_page_big_renab_3" style="background: url(&quot;https://www.art-center.ru/upload/renabs/29f322112439ca4579d23e9ade264f4b.jpg&quot;); display: none;">
                  <div class="main_page_big_renab_btn_left" onclick="rotationBBleft()"></div>
                  <div class="main_page_big_renab_btn_right" onclick="rotationBBright()"></div>
                  <div class="main_page_big_renab_info">
                      <span class="main_page_big_renab_info_name">Международный хореографический фестиваль-конкурс для детей и юношества  «Танцевальный Олимп» «Tanzolymp&#8203;» Отборочный тур</span>
                      <br><span class="main_page_big_renab_info_place">&nbsp;С 27 по 29 октября 2017 года в Москве <br> В программе также мастер-классы, совместные выступления конкурсантов с звёздами мировой сцены&nbsp;</span>

                      <br><br><br>
                      <a href="/events/tantsevalinii-olimp-otborochnii-tyr-2017/" class="button_green2_transparent" style="" isset="true"><div>Подробнее</div></a>
                  </div>
              </div>
              <div class="main_page_big_renab" id="main_page_big_renab_4" style="background: url(&quot;https://www.art-center.ru/upload/renabs/39c105af04be0c6a77b35e4476bf239a.jpg&quot;); display: none;">
                  <div class="main_page_big_renab_btn_left" onclick="rotationBBleft()"></div>
                  <div class="main_page_big_renab_btn_right" onclick="rotationBBright()"></div>
                  <div class="main_page_big_renab_info">
                      <span class="main_page_big_renab_info_name">Всероссийский хореографический конкурс «Танцевальное единство»<br>в Санкт-Петербурге с 1 по 5 декабря</span>
                      <br><span class="main_page_big_renab_info_place">&nbsp;Бесплатное участие и мастер-классы от ведущих хореографов<br>Призовой фонд 750 000 рублей<br>Насыщенная культурная программа&nbsp;</span>

                      <br><br><br>
                      <a href="events/tantsevalinoe-edinstvo/" class="button_green2_transparent" style="" isset="true"><div>ОЗНАКОМИТЬСЯ С ПОЛОЖЕНИЕМ</div></a>
                  </div>
              </div>
              <div id="main_page_big_renab_pages"></div>
            </div>
            <a name="start_search" isset="true"></a>
            <div id="main_page_form_search_header_up">
              <div id="main_page_form_search_header_bg"></div>
              <div id="main_page_form_search_header_title">ПОДОБРАТЬ ФЕСТИВАЛЬ</div>
            </div>
            <form action="/search/" method="post" id="form_search_id" class="ng-pristine ng-valid">
              <div id="form_search" class="form_search" style="display: block;">
                <div id="form_search_close" style="display: none;"></div>
                <div id="form_search_header"></div>
                <div id="form_search_content_id" class="form_search_content">

                <div class="form_search_content_type" id="form_search_content_type_form">
                  <div class="form_search_content_type_content">
                    <div class="form_search_content_item">
                      <div class="form_search_content_item_title"></div>
                      <div class="form_search_content_item_content">
                        <input type="text" id="form_search_field_name" name="text" value="" placeholder="Введите название или страну проведения" ng-model="search_input_text" class="ng-pristine ng-untouched ng-valid">
                      </div>
                    </div>

                <div class="form_search_content_item_country_city">

                  <div class="form_search_content_item_country">
                      <div class="form_search_content_item_title">страна</div>
                      <div class="form_search_content_item_content">

                          <select id="select-country" class="demo-default" placeholder="Начните вводить текст.." name="country[]">
                            <option value="" selected="selected"></option>
                            <?php for ($i=0; $i < 47; $i++) {
                                echo "<option value=\"".$i."\">Страна-".$i."</option>";
                              } ?>
                          </select>

                        </div>
                        <div class="form_search_content_item_country_quick">
                            <input type="hidden" name="quick_place" id="quick_place" value="undefined">
                            <a onclick="quickPlace(-7, '')" isset="true">Россия</a>
                            <a onclick="quickPlace(-2, '')" isset="true">Европа</a>
                            <a onclick="quickPlace(-3, '')" isset="true">Азия</a>
                            <a onclick="quickPlace(-4, '')" isset="true">Америка</a>
                        </div>

                    </div>
                    <br>
                    <div class="form_search_content_item_city">
                        <div class="form_search_content_item_title">город</div>
                        <div class="form_search_content_item_content">
                            <select id="select-city" class="demo-default" placeholder="Начните вводить текст.." name="town[]">
                                <?php
                                    echo '<option value selected="selected"></option>';

                                    echo '<option value="1">Москва</option>';
                                    echo '<option value="17">Санкт-Петербург</option>';

                                    for ($i=2; $i < 47; $i++) {
                                        if ($i !== 17) {
                                        echo "<option value=\"".$i."\">Город-".$i."</option>";
                                      }
                                      }
                                ?>
                            </select>
                        </div>
                        <br>
                        <input type="checkbox" id="chbox_zaoch" name="zaoch" class="search_zaoch_check"><label for="chbox_zaoch"><span></span>Заочный</label>
                    </div>

                </div>

                <div class="form_search_content_item_calend">
                  <div class="form_search_content_item_title">сроки проведения</div>
                  <div class="form_search_content_item_content form_search_content_item_content-grow">

                    <input type="hidden" name="date3" id="date3" value="0">
                    с <input id="date1" type="text" name="date1" size="12" value="29.08.2017">
                    по <input id="date2" type="text" name="date2" size="12" value="01.01.2019">

                    <input type="hidden" id="min_date_real" value="20.05.2007">
                    <input type="hidden" id="max_date_real" value="02.01.2019">

                    </div>
                    <div class="form_search_content_item_calend_quick">
                        <a onclick="quickCalend(-4)" isset="true">зима</a>
                        <a onclick="quickCalend(-3)" isset="true">весна</a>
                        <a onclick="quickCalend(-2)" isset="true">лето</a>
                        <a onclick="quickCalend(-1)" isset="true">осень</a>
                    </div>
                    <div style="clear: both;"></div>

                    <div class="form_search_content_item_calend_3th" id="kalendae_3th">

                        <script type="text/javascript">

                            var k = new Kalendae(document.getElementById('kalendae_3th') , {
                                months:3,
                                mode:'range',
                                weekStart: 1
                            });

                            k.subscribe('change', function (date) {
                                if(this.getSelected().length==10){
                                    date1_tmp = this.getSelected().split('-')
                                    $("#date1").val(date1_tmp[2]+'.'+date1_tmp[1]+'.'+date1_tmp[0])
                                }
                                else if(this.getSelected().length==23){
                                    date1_tmp = this.getSelected().substr(0,10).split('-')
                                    $("#date1").val(date1_tmp[2]+'.'+date1_tmp[1]+'.'+date1_tmp[0])
                                    date2_tmp = this.getSelected().substr(13,10).split('-')
                                    $("#date2").val(date2_tmp[2]+'.'+date2_tmp[1]+'.'+date2_tmp[0])
                                }

                            });

                            $(document).ready(function(){


                                var myHash_tmp = location.hash;
                                if(myHash_tmp.length>0 && $('#is_search_results').length>0){
                                    //взять переменные из location.hash
                                    myHash_tmp=myHash_tmp.substr(1,myHash_tmp.length-1);

                                    if(myHash_tmp.length>0){
                                        myHash_tmp = myHash_tmp.split('&');
                                        // сортировка по полученный параметрам
                                        for(var i=0; i<myHash_tmp.length; i++){
                                            hash_item = myHash_tmp[i].split('=');

                                            if(hash_item[0]=='date1'){
                                                date1 = hash_item[1];
                                                $("#date1").val(date1);
                                            }
                                            if(hash_item[0]=='date2'){
                                                date2 = hash_item[1];
                                                $("#date2").val(date2);
                                            }
                                        }

                                        date1_calendar = date1.split('.')
                                        date1_calendar = date1_calendar[2]+'-'+date1_calendar[1]+'-'+date1_calendar[0]
                                        date2_calendar = date2.split('.')
                                        date2_calendar = date2_calendar[2]+'-'+date2_calendar[1]+'-'+date2_calendar[0]

                                        //console.log(date1_calendar, ' - ', date2_calendar)
                                        /*
                                        var k = new Kalendae(document.getElementById('kalendae_3th') , {
                                            months:3,
                                            mode:'range',
                                            selected:[date1_calendar, date2_calendar],
                                            weekStart: 1
                                        });
                                        */
                                        k.setSelected(date1_calendar+','+date2_calendar)
                                        //k.setSelected(date1_calendar+','+date2_calendar)
                                    }

                                }
                                else{
                                                                        k.setSelected('2017-08-29, 2019-01-01')

                                    k.subscribe('change', function (date) {
                                        if(this.getSelected().length==10){
                                            date1_tmp = this.getSelected().split('-')
                                            $("#date1").val(date1_tmp[2]+'.'+date1_tmp[1]+'.'+date1_tmp[0])
                                        }
                                        else if(this.getSelected().length==23){
                                            date1_tmp = this.getSelected().substr(0,10).split('-')
                                            $("#date1").val(date1_tmp[2]+'.'+date1_tmp[1]+'.'+date1_tmp[0])
                                            date2_tmp = this.getSelected().substr(13,10).split('-')
                                            $("#date2").val(date2_tmp[2]+'.'+date2_tmp[1]+'.'+date2_tmp[0])
                                        }
                                        //console.log(this.getSelected());
                                    });
                                }
                            });
                        </script>

                    </div>
                </div>
                <div style="clear: both;"></div>

                <div class="form_search_content_item">
                  <div class="form_search_content_item_title">жанр мероприятия</div>
                  <div class="form_search_content_item_content">
                    <div class="form_search_content_item_content_field_checkbox_block checkbox-block">
                      <input type="checkbox" id="chbox_1" name="cat[]" class="search_category_check" value="1">
                      <label for="chbox_1">
                        <span></span>
                        <a data-title="Для участия приглашаются хоры всех категорий и вокальные ансамбли. В рамках данного жанра проводятся международные и всероссийские хоровые фестивали и конкурсы, конкурсы вокальных ансамблей, конкурсы хормейстеров и конкурсы для дирижеров хоров." data-placement="bottom" data-container="body" style="cursor: pointer;">Хоровые</a>
                      </label>
                      <input type="checkbox" id="chbox_2" name="cat[]" class="search_category_check" value="2">
                      <label for="chbox_2">
                        <span></span>
                        <a data-title="Для участия приглашаются вокалисты и вокальные ансамбли эстрадного, академического и народного вокала. В рамках данной категории проводятся конкурсы эстрадного и академического вокала, конкурсы вокально-инструментальных ансамблей, конкурсы эстрадной песни, конкурсы народного пения и другие." data-placement="bottom" data-container="body" style="cursor: pointer;">Вокальные</a>
                      </label>
                      <input type="checkbox" id="chbox_3" name="cat[]" class="search_category_check" value="3">
                      <label for="chbox_3">
                        <span></span>
                        <a data-title="Для участия приглашаются танцоры и хореографические коллективы различных направлений хореографии. В данной категории проводятся хореографические фестивали и конкурсы, конкурсы балета, конкурсы современной и народной хореографии, мастер-классы для танцоров и хореографов, хореографические лагеря и семинары для педагогов." data-placement="bottom" data-container="body" style="cursor: pointer;">Хореографические</a>
                      </label>
                      <input type="checkbox" id="chbox_4" name="cat[]" class="search_category_check" value="4">
                      <label for="chbox_4">
                        <span></span>
                        <a data-title="Для участия приглашаются отдельные исполнители, ансамбли и оркестры. В рамках данной категории проводятся конкурсы для юных музыкантов, фестивали и конкурсы для духовых, симфонических и оркестров народных инструментов, конкурсы для профессиональных исполнителей и любителей." data-placement="bottom" data-container="body" style="cursor: pointer;">Инструментальные</a>
                      </label>
                      <input type="checkbox" id="chbox_5" name="cat[]" class="search_category_check" value="5">
                      <label for="chbox_5">
                        <span></span>
                        <a data-title="Для участия приглашаются солисты, ансамбли и коллективы, выступающие в народном, фольклорном и этно направлении, включая стилизации и обрядовые постановки. В рамках данной категории проводятся фестивали и конкурсы народного вокала и фестивали народной хореографии, конкурсы театральных и обрядовых постановок, фестивали декоративно-прикладного искусства, конкурсы народного костюма." data-placement="bottom" data-container="body" style="cursor: pointer;">Фольклорные</a>
                      </label>
                      <input type="checkbox" id="chbox_6" name="cat[]" class="search_category_check" value="6">
                      <label for="chbox_6">
                        <span></span>
                        <a data-title="Для участия приглашаются солисты и коллективы всех направлений творчества. В рамках данной категории проводятся хореографические и вокальные фестивали, фестивали для театров, конкурс театрального творчества, конкурс театров мод, конкурс чтецов и исполнителей авторской песни, конкурсы художников и дизайнеров, конкурсы красоты." data-placement="right" data-container="body" style="cursor: pointer;">Другие</a>
                      </label>
                    </div>
                  </div>
                </div>
                <div style="clear: both;"></div>

                <div class="form_search_content_item_half ages">
                    <div class="form_search_content_item_title">возраст участников</div>
                    <div class="form_search_content_item_content">

                        <input id="minAge" type="text" name="age1" value="0" size="3" style="float: left;" />
                        <input id="maxAge" type="text" name="age2" value="100" size="3" style="float: right;" />
                        <div class="sliderCont">
                        <div id="slider_age"></div>
                        </div>
                    </div>
                </div>

                <div class="form_search_content_item_half price">
                    <div class="form_search_content_item_title">стоимость в рублях</div>
                    <div class="form_search_content_item_content">

                        <input type="hidden" id="min_price_real" value="1000">
                        <input type="hidden" id="max_price_real" value="50000">

                        <input id="minCost" type="text" name="price1" value="30" size="5" style="float: left;"/>
                        <input id="maxCost" type="text" name="price2" value="50000" size="5" style="float: right;"/>
                        <div class="sliderCont">
                            <div id="slider_cost"></div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="form-search__submit">
                  <a href="#" id="main_form_search_submit" onclick="#" class="button_green button_green2" isset="true">
                    <div>Подобрать&nbsp;мероприятие</div>
                  </a>
                </div>
            </div>

        </div>
        <div class="form_search_content_type hidden" id="form_search_content_type_map">
            <div class="form_search_content_type_content">

                <div class="form_search_content_item_country_town">
                    <div class="form_search_content_item_content">
                      <select id="select-location" name="state[]" multiple="multiple" class="demo-default selectized" placeholder="начните вводить название страны или города.." tabindex="-1" style="display: none;"></select>
                      <div class="selectize-control demo-default multi">
                        <div class="selectize-input items not-full has-options" style="">
                          <input type="text" autocomplete="off" tabindex="" placeholder="начните вводить название страны или города.." style="width: 294px;">
                        </div>
                        <div class="selectize-dropdown multi demo-default" style="display: none; width: 100px; top: 18px; left: 0px;">
                          <div class="selectize-dropdown-content"></div>
                        </div>
                      </div>

                      <script>
                      $('#select-location').selectize({
                          maxItems: 5
                      });
                      </script>
                    </div>
                    <div id="searchforMapSubmit" onclick="startSearchByCountryTown()"></div>
                </div>
                <div class="form_search_content_item">
                  <div class="form_search_content_item_title">жанр мероприятия</div>
                  <div class="form_search_content_item_content">
                    <div class="form_search_content_item_content_field_checkbox_block">

                      <input type="checkbox" id="chbox_2_1" name="cat2[]" value="1" checked=""><label for="chbox_2_1"><span></span>Хоровые</label>
                      <input type="checkbox" id="chbox_2_2" name="cat2[]" value="2" checked=""><label for="chbox_2_2"><span></span>Вокальные</label>
                      <input type="checkbox" id="chbox_2_3" name="cat2[]" value="3" checked=""><label for="chbox_2_3"><span></span>Хореографические</label>
                      <input type="checkbox" id="chbox_2_4" name="cat2[]" value="4" checked=""><label for="chbox_2_4"><span></span>Инструментальные</label>
                      <input type="checkbox" id="chbox_2_5" name="cat2[]" value="5" checked=""><label for="chbox_2_5"><span></span>Фольклорные</label>
                      <input type="checkbox" id="chbox_2_6" name="cat2[]" value="6" checked=""><label for="chbox_2_6"><span></span>Другие</label>

                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    <div id="main_page_big_renab_wrapper" class="ads">
        <div id="main_page_big_renab" class="ads__wrapper">
          <a target="_blank" href="/goto.php?id=68&amp;url=http://muzklondike.ru/" isset="true">
            <img alt="" src="https://www.art-center.ru/upload/renabs/f7938d4bd7da5d3ad1973e5de6f0ad1b.jpg">
          </a>
        </div>
    </div>

    <div id="main_page_popular_events">
      <div id="main_page_popular_events_title">Популярные мероприятия</div>
      <div id="main_page_popular_events_content">
        <div class="main_page_popular_events_content_nav popular_prev_forbidden" id="popular_btn_prev"></div>
        <input type="hidden" id="count_populars" value="2">
        <div class="main_page_popular_events_content_slide" id="main_page_popular_events_content_slide_0" style="display: block;">
          <div class="main_page_popular_events_content_slide_photo">
            <img src="https://www.art-center.ru/upload/e/4670/pop_49aa85b4c6fbcc292dce1ed09941ab78.jpg" alt="{FEST}{NAME_FEST_SHORT}" style="width: 313px; height: 293px;">
            <div class="main_page_popular_events_content_slide_type_over_photo">
              <div class="main_page_popular_events_content_slide_photo_types_event e_fest">ФЕСТИВАЛЬ</div>
              <br>
              <div class="main_page_popular_events_content_slide_photo_types_event e_konk">КОНКУРС</div>
              <br>
            </div>
          </div>
          <div class="main_page_popular_events_content_slide_info">
            <div class="main_page_popular_events_content_slide_info_inner">
              <div class="main_page_popular_events_content_slide_info_inner_head">
                <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_day">
                  <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_day_bg"></div>
                  <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_day_val">16</div>
                </div>
                <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_month">мая</div>
                <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_place">Москва, Россия</div>
            </div>
            <div class="main_page_popular_events_content_slide_info_inner_other_info">
              <div class="main_page_popular_events_content_slide_info_inner_other_info_name"><a href="/events/deti-v-mire-starinnoi-myziki-zaochny-2017/" isset="true">II Международный заочный музыкальный конкурс «ДЕТИ В МИРЕ СТАРИННОЙ МУЗЫКИ» Москва, Россия</a></div>
              <div class="main_page_popular_events_content_slide_info_inner_other_info_priem">Прием заявок до: <span class="span_date_priem">13 октября 2017</span></div>
            </div>
          </div>
        </div>
        <a href="/events/deti-v-mire-starinnoi-myziki-zaochny-2017" class="main_page_popular_events_content_slide_over_photo_link" isset="true"></a>
      </div>
      <div class="main_page_popular_events_content_slide" id="main_page_popular_events_content_slide_1">
        <div class="main_page_popular_events_content_slide_photo">
          <img src="https://www.art-center.ru/upload/e/4768/pop_a44ef258f19af820899d78b592d3efb3.jpg" alt="{FEST}{NAME_FEST_SHORT}" style="width: 313px; height: 293px;">
          <div class="main_page_popular_events_content_slide_type_over_photo">
            <div class="main_page_popular_events_content_slide_photo_types_event e_fest">ФЕСТИВАЛЬ</div><br><div class="main_page_popular_events_content_slide_photo_types_event e_konk">КОНКУРС</div><br>
          </div>
        </div>
        <div class="main_page_popular_events_content_slide_info">
          <div class="main_page_popular_events_content_slide_info_inner">
            <div class="main_page_popular_events_content_slide_info_inner_head">
              <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_day">
                  <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_day_bg"></div>
                  <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_day_val">2</div>
              </div>
              <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_month">ноября</div>
              <div class="main_page_popular_events_content_slide_info_inner_head_date_begin_place">Прага, Чехия</div>
            </div>
            <div class="main_page_popular_events_content_slide_info_inner_other_info">
              <div class="main_page_popular_events_content_slide_info_inner_other_info_name">
                <a href="/events/horeograficheskii-v-prage/" isset="true">Международный Хореографический Конкурс в Праге</a>
              </div>
              <div class="main_page_popular_events_content_slide_info_inner_other_info_priem">Прием заявок до: <span class="span_date_priem">17 октября 2017</span></div>
            </div>
          </div>
        </div>
        <a href="/events/horeograficheskii-v-prage" class="main_page_popular_events_content_slide_over_photo_link" isset="true"></a>
      </div>

      <div class="main_page_popular_events_content_nav_next popular_next" id="popular_btn_next"></div>
    </div>
  </div>
  <div class="ads">
      <div class="ads__wrapper">
        <a target="_blank" href="http://www.culture.ru/" isset="true">
          <img alt="Портал Культура.рф" src="d/i/1000x90.jpg">
        </a>
      </div>
  </div>
  <div id="main_page_ending" class="ending">
    <div id="main_page_ending_title_bg" class="ending__background">
        <div id="main_page_ending_title_bg_1" class="ending__row ending__row_1"></div>
        <div id="main_page_ending_title_bg_2" class="ending__row ending__row_2"></div>
        <div id="main_page_ending_title_bg_3" class="ending__row ending__row_3"></div>
    </div>
    <div id="main_page_ending_title" class="ending__title">ЗАКАНЧИВАЕТСЯ ПРИЕМ</div>
    <div id="closing_events_block" class="ending__content">
      <div class="div_closing_item">
        <div class="closing_item_photo">
          <img src="https://www.art-center.ru/upload/e/4861/pop_84485aae03cc456e60c0ac1a9947ba1d.jpg" alt="">
        </div>
        <div class="closing_item_info">
          <div class="closing_item_info_priem">
              <span class="day">01</span>
              <span class="month">октября</span>
          </div>
          <a href="/events/mi-vmeste-novosibirsk-2017/" isset="true">Международный конкурс-фестиваль детского и юношеского творчества «МЫ ВМЕСТЕ» Новосибирск</a>
          <div class="closing_item_info_begin">
            Начало фестиваля:
            <span>
                10
                ноября
                2017
            </span>
          </div>
        </div>
      </div>
      <div class="div_closing_item">
        <div class="closing_item_photo">
            <img src="https://www.art-center.ru/upload/e/4943/pop_92137b54afe2cefeb7986a4f05ccab6c.jpg" alt="">
        </div>
        <div class="closing_item_info">
          <div class="closing_item_info_priem">
              <span class="day">15</span>
              <span class="month">декабря</span>
          </div>
          <a href="/events/artcello/" isset="true">Художественный конкурс ArtCello 2018 в очной и заочной формах</a>
          <div class="closing_item_info_begin">
            Начало фестиваля:
            <span>
                01
                сентября
                2017
            </span>
          </div>
        </div>
      </div>
      <div class="div_closing_item">
        <div class="closing_item_photo">
            <img src="https://www.art-center.ru/upload/e/4768/pop_a44ef258f19af820899d78b592d3efb3.jpg" alt="">
        </div>
        <div class="closing_item_info">
          <div class="closing_item_info_priem">
              <span class="day">17</span>
              <span class="month">октября</span>
          </div>
          <a href="/events/horeograficheskii-v-prage/" isset="true">Международный Хореографический Конкурс в Праге</a>
          <div class="closing_item_info_begin">
            Начало фестиваля:
            <span>
                02
                ноября
                2017
            </span>
          </div>
        </div>
      </div>
      <div class="div_closing_item">
        <div class="closing_item_photo">
            <img src="https://www.art-center.ru/upload/e/4745/pop_c1204655548f3fc91f86c0ff807c986c.jpg" alt="">
        </div>
        <div class="closing_item_info">
          <div class="closing_item_info_priem">
              <span class="day">15</span>
              <span class="month">марта</span>
          </div>
          <a href="/events/zvezdnii-dozhdik/" isset="true">XVII Международный фестиваль-конкурс детско-юношеского творчества «Звездный Дождик» «Die kleinen Sternschnuppen»</a>
          <div class="closing_item_info_begin">
            Начало фестиваля:
            <span>
                02
                мая
                2018
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="ending__more">
        <a href="/events/closing" isset="true" class="ending__btn">Показать остальные</a>
    </div>
    <div class="clear"></div>
  </div>

  <div id="main_page_news">
    <div id="main_page_news_title">НОВОСТИ</div>

    <div class="div_news_item">
      28 августа 2017
      <br>
      <a href="/news/1852/" isset="true">Акция! II Всероссийский ежегодный фестиваль-конкурс «Новые имена» предоставляет скидки на участие!</a>
    </div>
    <div class="div_news_item">
      24 августа 2017
      <br>
      <a href="/news/1851/" isset="true">Лада Мишина проложила «Мосты» на RU.TV</a>
    </div>
    <div class="div_news_item">
      23 августа 2017
      <br>
      <a href="/news/1850/" isset="true">«Суперфинал» подводит конкурсные итоги</a>
    </div>
    <div class="div_news_item">
      23 августа 2017
      <br>
      <a href="/news/1849/" isset="true">«Развитие»: форум для педагогов</a>
    </div>
    <div class="div_news_item">
      14 августа 2017
      <br>
      <a href="/news/1847/" isset="true">Всем актерам приготовиться. Сентябрь начинается - даем ТРЕТИЙ ЗВОНОК!</a>
    </div>
    <a href="/news/" class="button_news" isset="true">
      <div>Остальные новости</div>
    </a>
  </div>
  <div id="main_page_about">
    <div id="main_page_about_title">О КОМПАНИИ</div>
    <div id="main_page_about_inner">
      <p>«Арт-Центр» - информационный сервисный холдинг.</p>

      <p>Включает в себя группу компаний: "Арт-Центр Плюс" и Информационное агентство «Музыкальный Клондайк»</p>

      <p>Компания основана в 1995 году.</p>

      <p>«Арт-Центр» сотрудничает только с проверенными организаторами и является гарантом качества для участников.</p>

      <p>Наш единый сервисный центр предлагает:</p>

      <ul>
      	<li>фестивали и конкурсы по всему миру</li>
      	<li>образовательные программы, мастер-классы</li>
      	<li>формирование положений и программ</li>
      	<li>консультации по вопросам участия и выезда</li>
      </ul>

      <p>&nbsp;</p>

      <p>Наши специалисты имеют богатый опыт в разработке моделей, подготовке и проведении фестивалей и конкурсов, организации участия российских коллективов и солистов в международных фестивалях и конкурсах, организации практических семинаров и мастер-классов.</p>

      <p>&nbsp;</p>
    </div>
  </div>
  <div style="clear: both"></div>
  <div id="main_page_ads_bottom">
    <div class="main_page_ads_bottom_item" id="main_page_ads_bottom_item_left">
      <a target="_blank" href="/goto.php?id=90&amp;url=http://www.intermedia.ru/" isset="true">
        <img alt="" src="https://www.art-center.ru/upload/renabs/f4e7223796c34718bf9f0efa4691c408.jpg">
      </a>
    </div>
    <div class="main_page_ads_bottom_item"></div>
      <a target="_blank" href="http://market.zakupki.mos.ru/Supplier/Supplier?supplierId=15092703&amp;from=sp_api_1_iregistered" isset="true">
        <img style="height: 100px;" src="d/i/PortalUser_06.png" alt="Я зарегистрирован на Портале Поставщиков">
      </a>
  </div>
  <div style="clear:both"></div>
</div>
<?php require_once 'blocks/_footer.php'; ?>
<script src="/build/bundle.js?time=<?= date('Y-m-d\Th:i:s'); ?>" charset="utf-8"></script>
<script src="/build/old/fest.js?time=<?= date('Y-m-d h:i:s'); ?>" charset="utf-8"></script>
<script src="/build/old/users.js" charset="utf-8"></script>
<script src="/build/old/main.js" charset="utf-8" defer async></script>
<script src="/build/old/main_page.js" charset="utf-8"></script>

</body>
</html>
